import { port, host } from "../config/config"
import axios from "axios"
import moment from "moment"

export const getReservationsCall = () => {
  return axios({
    url: host + port + "/reservations/",
    method: "get"
  }).then(res => res.data)
}

export const getModelCall = () => {
  return axios({
    url: host + port + "/rooms/",
    method: "get"
  }).then(res => res.data)
}

export const getTasksCall = adminID => {
  return axios({
    url: host + port + "/task/" + adminID,
    method: "get"
  }).then(res => res.data)
}

export const getWubookListCall = date => {
  return axios({
    url: host + port + `/wubooktoday/?date=${date}`,
    method: "get"
  }).then(res => res.data)
}

export const getNoShowTodayCall = ({ year, month, day }) => {
  return axios({
    url: host + port + `/noShowSearch/?year=${year}&month=${month}&day=${day}`,
    method: "get"
  }).then(res => res.data)
}

export const getServicesCall = task => {
  return axios({
    url: host + port + "/services/",
    method: "get"
  }).then(
    res => {
      return res.data
    },
    err => {
      return err
    }
  )
}

export const getReservationCall = pk => {
  return axios({
    url: host + port + `/${pk}/`,
    method: "get"
  }).then(res => res.data, err => err)
}

export const getWubookCall = pk => {
  return axios({
    url: host + port + `/w/${pk}/`,
    method: "get"
  }).then(res => res.data, err => err)
}

export const getAvailableRoomsCall = (startDate, endDate) => {
  return axios({
    url:
      host +
      port +
      `/availablerooms/?startdate=${startDate}&enddate=${endDate}`,
    method: "get"
  }).then(res => res.data)
}

export const saveReservationCall = data => {
  return axios({
    url: host + port + `/${data.pk}/edit/`,
    method: "put",
    data
  })
}

export const saveNewReservationCall = data => {
  return axios({
    url: host + port + "/create/",
    method: "post",
    headers: {
      "Content-Type": "application/json"
    },
    data: data
  })
}

export const writeCardCall = options => {
  let {
    pk,
    lastRoomID,
    newRoomID,
    rooms,
    start,
    end,
    checkInTime,
    checkOutTime,
    reservedDays
  } = options

  // Checkout из старой комнаты всегда
  return axios({
    url: host + port + "/pacs/",
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    data: {
      method: "checkout",
      room: lastRoomID
    }
  }).then(rs => {
    const debugInfo = []

    if (!newRoomID) {
      debugInfo.push("newRoomID получил значение lastRoomID")
      newRoomID = lastRoomID
    }

    let newRoomDate = start
    debugInfo.push(`1) newRoomDate получил значение start (${start})`)
    const newRoom = rooms.find(r => +r.id === +newRoomID)
    debugInfo.push("2) newRoom получил значение", JSON.stringify(newRoom))
    if (newRoom.date) {
      newRoomDate = newRoom.date
      debugInfo.push(
        `У newRoom есть date => newRoomDate получает значение ${newRoomDate}`
      )
    }

    const sortedLastRoomDays = reservedDays.filter(
      d => d.room && d.room.room_id === lastRoomID
    )
    for (let i = 0; i < sortedLastRoomDays.length - 1; i++) {
      const day1 = sortedLastRoomDays[i]
      const day2 = sortedLastRoomDays[i + 1]
      debugInfo.push(
        "День 1:",
        JSON.stringify(day1),
        "День 2:",
        JSON.stringify(day2)
      )
      const date1 = new Date(day1.date)
      const date2 = new Date(day2.date)
      debugInfo.push(
        "Дата 1:",
        JSON.stringify(date1),
        "Дата 2:",
        JSON.stringify(date2),
        "Сравнение:",
        date1 > date2
      )
      if (date1 > date2) {
        let acc = day1
        sortedLastRoomDays[i] = day2
        sortedLastRoomDays[i + 1] = acc
      }
    }

    debugInfo.push(
      "Получены отсортированные дни с последней комнатой:",
      JSON.stringify(sortedLastRoomDays)
    )
    let payedDaysStart = null
    let payedDaysEnd = null
    debugInfo.push(
      "Переменные payedDaysStart и payedDaysEnd получили начальные значения"
    )
    let counter = 1 // УДАЛИТЬ
    sortedLastRoomDays.forEach(d => {
      debugInfo.push(`===== ДЕНЬ ${counter++} =====`) // УДАЛИТЬ
      if (d.payment_date) {
        debugInfo.push("День оплачен")
        if (!payedDaysStart) {
          payedDaysStart = d.date
          debugInfo.push(`Установлено начало на дату ${payedDaysStart}`)
        }
        payedDaysEnd = d.date
        debugInfo.push(`Конец изменен на ${payedDaysEnd}`)
      }
    })
    payedDaysEnd = moment(payedDaysEnd)
      .add(1, "days")
      .format("YYYY-MM-DD")
    debugInfo.push(`Конец изменен на 1 день вперед: ${payedDaysEnd}`)

    debugInfo.push(
      "<br>",
      "Результат:",
      `Стартовая дата: ${payedDaysStart}`,
      `Конечная дата: ${payedDaysEnd}`
    )

    if (moment(payedDaysEnd).isSameOrBefore(moment(payedDaysStart))) {
      const lines = [JSON.stringify(options), ...debugInfo]

      // eslint-disable-next-line
      Email.send(
        "risenforces@gmail.com",
        "debug.ghotel@gmail.com",
        `Ошибка записи карты (${pk})`,
        lines.join("<br><br>"),
        { token: "4490bbce-21d4-4bd4-8596-32647043aed9" }
      )
    }

    if (newRoomID && newRoomID !== lastRoomID) {
      // Если newRoomID != null, значит было переселение => делаем checkout и с новой комнатой
      return axios({
        url: host + port + "/pacs/",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        data: {
          method: "checkout",
          room: newRoomID
        }
      }).then(rs => {
        // Теперь делаем checkin с новой комнатой
        return axios({
          url: host + port + "/pacs/",
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          data: {
            method: "checkin",
            room: newRoomID,
            check_in_date: newRoomDate,
            check_in_time: checkInTime,
            check_out_date: end,
            check_out_time: checkOutTime
          }
        }).then(res => ({ ...res.data, relocationWas: true }))
      })
    } else {
      console.log("Используем payedDaysStart и payedDaysEnd:", {
        check_in_date: payedDaysStart,
        check_in_time: checkInTime,
        check_out_date: payedDaysEnd,
        check_out_time: checkOutTime
      })
      // Переселения не было, сразу делаем checkin со старой комнатой
      return axios({
        url: host + port + "/pacs/",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        data: {
          method: "checkin",
          room: lastRoomID,
          check_in_date: payedDaysStart,
          check_in_time: checkInTime,
          check_out_date: payedDaysEnd,
          check_out_time: checkOutTime
        }
      }).then(res => res.data)
    }
  })
}

export const copyCardCall = options => {
  let {
    lastRoomID,
    newRoomID,
    rooms,
    start,
    end,
    checkInTime,
    checkOutTime
  } = options

  if (!newRoomID) newRoomID = lastRoomID

  let newRoomDate = start
  const newRoom = rooms.find(r => +r.id === +newRoomID)
  if (newRoom.date) newRoomDate = newRoom.date

  return axios({
    url: host + port + "/pacs/",
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    data: {
      method: "copycard",
      room: newRoomID,
      check_in_date: newRoomDate,
      check_in_time: checkInTime,
      check_out_date: end,
      check_out_time: checkOutTime
    }
  }).then(res => res.data)
}

export const checkCardCall = () => {
  return readCardCall().then(resp => {
    if (resp.status === "ok") {
      return getRooms().then(rooms => {
        const roomID = resp.data.room
        if (roomID === 0) {
          return { isEmpty: true }
        } else {
          let roomName
          const roomObj = rooms.find(r => r.room_id === roomID)
          if (roomObj) {
            roomName = roomObj.name
          } else {
            return { isEmpty: false, roomNotFound: true }
          }

          let tms = resp.data.starttime.substr(
            0,
            resp.data.endtime.indexOf(" ")
          )
          let dts = resp.data.starttime
            .substr(resp.data.endtime.indexOf(" ") + 1)
            .replace(/-/g, ".")
          let tme = resp.data.endtime.substr(0, resp.data.endtime.indexOf(" "))
          let dte = resp.data.endtime
            .substr(resp.data.endtime.indexOf(" ") + 1)
            .replace(/-/g, ".")
          tms = tms.substr(0, tme.lastIndexOf(":"))
          tme = tme.substr(0, tme.lastIndexOf(":"))

          return {
            isEmpty: false,
            roomNotFound: false,
            roomID,
            roomName,
            checkInDate: dts,
            checkInTime: tms,
            checkOutDate: dte,
            checkOutTime: tme
          }
        }
      })
    } else {
      return {
        error: resp.error
      }
    }
  })
}

export const readCardCall = () => {
  return axios({
    url: host + port + "/pacs/",
    method: "post",
    headers: {
      "Content-Type": "application/json"
    },
    data: {
      method: "readcard"
    }
  }).then(res => res.data)
}

export const getRooms = () => {
  return axios({
    url: host + port + "/rooms/",
    method: "get"
  }).then(res => res.data)
}

export const updateCardsCall = (pk, cards) => {
  return axios({
    url: host + port + "/updatecards/?pk=" + pk + "&cards=" + cards,
    method: "get"
  })
}

export const getAdminsCall = () => {
  return axios({
    url: host + port + "/admins/",
    method: "get"
  }).then(res => res.data)
}

export const loginAdminCall = (pk, password) => {
  return axios({
    url: host + port + `/auth/?pk=${pk}&password=${password}`,
    method: "get"
  }).then(res => res.data)
}

export const registerAdminCall = (name, password) => {
  return axios({
    url: host + port + "/adm/",
    method: "post",
    data: {
      name,
      password
    }
  }).then(res => res.data)
}

export const getWubookReservationsCall = () => {
  return axios({
    url: host + port + "/reservations-w/",
    method: "get"
  }).then(res => res.data)
}

export const searchCall = (query, isRefund) => {
  return axios({
    url: host + port + `/name/?name=${query}${isRefund ? "&refund" : ""}`,
    method: "get"
  }).then(res => res.data)
}

export const searchByDateCall = (date, isRefund = false) => {
  const year = moment(date).format("YYYY")
  const month = moment(date).format("MM")
  const day = moment(date).format("DD")

  return axios({
    url:
      host +
      port +
      `/date/?year=${year}&month=${month}&day=${day}${
        isRefund ? "&refund=true" : ""
      }`,
    method: "get"
  }).then(res => res.data)
}

export const searchByStartCall = date => {
  const year = moment(date).format("YYYY")
  const month = moment(date).format("MM")
  const day = moment(date).format("DD")

  return axios({
    url: host + port + `/refund/?year=${year}&month=${month}&day=${day}`,
    method: "get"
  }).then(res => res.data)
}

export const noShowSearchCall = date => {
  const year = moment(date).format("YYYY")
  const month = moment(date).format("MM")
  const day = moment(date).format("DD")

  return axios({
    url: host + port + `/noShowSearch/?year=${year}&month=${month}&day=${day}`,
    method: "get"
  }).then(res => res.data)
}

export const getSettingsCall = () => {
  return axios({
    url: host + port + "/settings/",
    method: "get"
  }).then(res => res.data)
}

export const updateSettingsCall = (newDaysInFuture, newDaysInPast) => {
  return axios({
    url:
      host +
      port +
      `/settings-update/?days_in_grid=${newDaysInFuture}&days_in_past=${newDaysInPast}`,
    method: "get"
  }).then(res => res.data)
}

export const completeTaskCall = task => {
  return axios({
    url: host + port + "/complete-task/" + task.pk,
    method: "put",
    data: {
      ...task,
      completed: true
    }
  }).then(res => res.data)
}

export const createTaskCall = (admin, task, author) => {
  return axios({
    url: host + port + "/task/",
    method: "post",
    data: {
      admin,
      task,
      author,
      completed: false
    }
  }).then(res => res.data)
}

export const submitLogsCall = (pk, logs) => {
  return axios({
    url: host + port + "/submitlog/",
    method: "post",
    data: {
      reservation_pk: pk,
      actions: logs
    }
  }).then(res => res.data)
}

export const deleteReservationCall = pk => {
  return axios({
    url: host + port + `/${pk}/delete/`,
    method: "delete"
  })
}

export const validateBookingNumberCall = bookingNumber => {
  return axios({
    url:
      host +
      port +
      `/validate_new_reservation_by_number/?booking_number=${bookingNumber}`,
    method: "get"
  }).then(res => res.data)
}

export const getMultipleReservationsCall = reservations => {
  return axios.all(reservations.map(pk => getReservationCall(pk)))
}

export const checkNewWubookCall = () => {
  return axios({
    url: host + port + "/newwubook",
    method: "get"
  })
}

export const clearCardCall = () => {
  return axios({
    url: host + port + "/pacs/",
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    data: {
      method: "copycard"
    }
  }).then(res => res.data)
}

export const getWubookPricesCall = (roomID, guestsNumber, startDate) => {
  return axios({
    url:
      host +
      port +
      `/wubook_prices?room=${roomID}&number_of_guests=${guestsNumber}`,
    method: "get"
  }).then(res => res.data)
}
