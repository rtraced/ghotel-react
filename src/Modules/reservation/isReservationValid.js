import moment from "moment"
import paymentMethods from "../helpers/paymentMethods"

export default reservation => {
  let validStatus = true
  const check = condition => {
    if (!condition) validStatus = false
  }

  const messages = []

  const isDateValid = date => moment(date, "YYYY-MM-DD", true).isValid()
  const isTimeValid = time => moment(time, "HH:mm:ss", true).isValid()
  const isNumber = num => !isNaN(parseFloat(num)) && isFinite(num)

  switch (reservation.mode) {
    case "create":
    case "edit":
      // guests_number should be integer number
      reservation.guests_number = +reservation.guests_number
      check(
        isNumber(reservation.guests_number) &&
          parseInt(reservation.guests_number, 10) === reservation.guests_number
      )

      // start and end should be correct YYYY-MM-DD dates
      check(isDateValid(reservation.start))
      check(isDateValid(reservation.end))

      // checkin and checkout should be correct HH:mm:ss times
      check(isTimeValid(reservation.check_in_time))
      check(isTimeValid(reservation.check_out_time))

      messages.push(`Is base data valid: ${validStatus}`)

      // Days validation
      check(
        reservation.reserved_days.every(day => {
          let dayValidStatus = true
          const dayCheck = condition => {
            if (!condition) dayValidStatus = false
          }

          // Days objects should always have correct date, price and room values
          dayCheck(isDateValid(day.date))
          dayCheck(day.room && day.room.room_id && day.room.name)
          dayCheck(isNumber(day.price))

          if (day.payment_date) {
            // payment_date should be correct YYYY-MM-DD date
            dayCheck(isDateValid(day.payment_date))

            // Payed days should have correct payment_type and admin fields
            dayCheck(paymentMethods.includes(day.payment_type))
            dayCheck(day.admin)
          }

          messages.push(`Is day valid: ${dayValidStatus}`)
          return dayValidStatus
        })
      )

      // Services validation
      check(
        reservation.additional_services.every(service => {
          let serviceValidStatus = true
          const serviceCheck = condition => {
            if (!condition) serviceValidStatus = false
          }

          // Services objects should have correct
          // date, payment_date, price, quantity, payment_type, service and admin
          // fields
          serviceCheck(isDateValid(service.date))
          serviceCheck(isDateValid(service.payment_date))
          serviceCheck(isNumber(service.price))
          serviceCheck(isNumber(service.quantity))
          serviceCheck(paymentMethods.includes(service.payment_type))
          serviceCheck(
            reservation.services
              .map(s => s.name)
              .concat("Улучшение", "Поздний выезд", "Ранний заезд")
              .includes(service.service)
          )
          serviceCheck(service.admin)

          messages.push(`Is service valid: ${serviceValidStatus}`)
          return serviceValidStatus
        })
      )

      break

    default:
      // reservation.mode isn't correct so we can't process data and validStatus is unknown
      validStatus = false
      break
  }

  console.log("Служебная информация:")
  console.log(reservation)
  messages.forEach(m => console.log(m))

  return validStatus
}
