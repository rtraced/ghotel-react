import Cookies from "js-cookie"
import reservationDuration from "../helpers/reservationDuration"
import getArrayWithUpdatedItem from "../helpers/getArrayWithUpdatedItem"
import moment from "moment"
import Logs from "../Logs"
import {
  getServicesCall,
  getReservationCall,
  saveReservationCall,
  saveNewReservationCall,
  writeCardCall,
  copyCardCall,
  updateCardsCall,
  getWubookCall,
  getAvailableRoomsCall,
  validateBookingNumberCall,
  getMultipleReservationsCall,
  getWubookPricesCall,
  deleteReservationCall
} from "../api/index"
import CheckPrintController from "../CheckPrintController"
import getSellCheckData from "../CheckPrintController/sellCheck/getSellCheckData"
import getRefundCheckData from "../CheckPrintController/refundCheck/getRefundCheckData"
import { SERVICE_CLEAR } from "../service/ServiceModule"
import { UPGRADE_CLEAR } from "../upgrade/UpgradeModule"
import omit from "object.omit"
import {
  CHECK_IN_TIME_CHANGED,
  CHECK_IN_SET,
  CHECK_IN_CLEAR,
  onCheckInDateChange
} from "../checkin/CheckInModule"
import {
  CHECK_OUT_TIME_CHANGED,
  CHECK_OUT_SET,
  CHECK_OUT_CLEAR,
  onCheckOutDateChange
} from "../checkout/CheckOutModule"
import {
  earlyCheckInService,
  lateCheckOutService,
  upgradeService
} from "../helpers/servicesNames"
import {
  defaultCheckInTime,
  defaultCheckOutTime
} from "../helpers/defaultTimes"
import isReservationValid from "./isReservationValid"
import doNeedToPrint from "../CheckPrintController/doNeedToPrint"
import history from "../helpers/history"
import isServerRunning from "../CheckPrintController/isServerRunning"
import getErrorFields from "./getErrorFields"

if (!Object.assign) {
  Object.defineProperty(Object, "assign", {
    enumerable: false,
    configurable: true,
    writable: true,
    value: function(target, firstSource) {
      if (target === undefined || target === null) {
        throw new TypeError("Cannot convert first argument to object")
      }

      var to = Object(target)
      for (var i = 1; i < arguments.length; i++) {
        var nextSource = arguments[i]
        if (nextSource === undefined || nextSource === null) {
          continue
        }

        var keysArray = Object.keys(Object(nextSource))
        for (
          var nextIndex = 0, len = keysArray.length;
          nextIndex < len;
          nextIndex++
        ) {
          var nextKey = keysArray[nextIndex]
          var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey)
          if (desc !== undefined && desc.enumerable) {
            to[nextKey] = nextSource[nextKey]
          }
        }
      }
      return to
    }
  })
}

const START_CHANGED = "ghotel/reservation/START_CHANGED"
const END_CHANGED = "ghotel/reservation/END_CHANGED"
const DAY_PRICE_CHANGED = "ghotel/reservation/DAY_PRICE_CHANGED"
const DAY_PAYED_CHANGED = "ghotel/reservation/DAY_PAYED_CHANGED"
const DAY_METHOD_CHANGED = "ghotel/reservation/DAY_METHOD_CHANGED"
const RESERVATION_PAYED_CHANGED = "ghotel/reservation/RESERVATION_PAYED_CHANGED"
const SERVICES_SET = "ghotel/reservation/SERVICES_SET"
const SERVICE_CREATE = "ghotel/reservation/SERVICE_CREATE"
const SERVICE_QTY_CHANGED = "ghotel/reservation/SERVICE_QTY_CHANGED"
const SERVICE_PRICE_CHANGED = "ghotel/reservation/SERVICE_PRICE_CHANGED"
const SERVICE_PAYMENT_CHANGED = "ghotel/reservation/SERVICE_PAYMENT_CHANGED"
const SERVICE_DATE_CHANGED = "ghotel/reservation/SERVICE_DATE_CHANGED"
const SERVICE_DELETE = "ghotel/reservation/SERVICE_DELETE"
const SERVICE_REFUND = "ghotel/reservation/SERVICE_REFUND"
const SERVICE_COPY = "ghotel/reservation/SERVICE_COPY"
const SERVICE_ERROR_SET = "ghotel/reservation/SERVICE_ERROR_SET"
const DAY_ADD = "ghotel/reservation/DAY_ADD"
const DAY_REMOVE = "ghotel/reservation/DAY_REMOVE"
const DAY_SUMM_COPY = "ghotel/reservation/DAY_SUMM_COPY"
const DAY_METHOD_COPY = "ghotel/reservation/DAY_METHOD_COPY"
const NAME_CHANGED = "ghotel/reservation/NAME_CHANGED"
const BOOKING_NUMBER_CHANGED = "ghotel/reservation/BOOKING_NUMBER_CHANGED"
const PHONE_CHANGED = "ghotel/reservation/PHONE_CHANGED"
const GUESTS_NUMBER_CHANGED = "ghotel/reservation/GUESTS_NUMBER_CHANGED"
const MAIL_CHANGED = "ghotel/reservation/MAIL_CHANGED"
const NOTE_CHANGED = "ghotel/reservation/NOTE_CHANGED"
const CLIENT_NOTE_CHANGED = "ghotel/reservation/CLIENT_NOTE_CHANGED"
const WUBOOK_NOTE_CHANGED = "ghotel/reservation/WUBOOK_NOTE_CHANGED"
const REFUND_CHANGED = "ghotel/reservation/REFUND_CHANGED"
const UPGRADE_CREATE = "ghotel/reservation/UPGRADE_CREATE"
const UPGRADE_DELETE = "ghotel/reservation/UPGRADE_DELETE"
const UPGRADE_ERROR_SET = "ghotel/reservation/UPGRADE_ERROR_SET"
const UPGRADE_ERROR_CLEAR = "ghotel/reservation/UPGRADE_ERROR_CLEAR"
export const CHECK_IN_CHANGED = "ghotel/reservation/CHECK_IN_CHANGED"
export const CHECK_OUT_CHANGED = "ghotel/reservation/CHECK_OUT_CHANGED"
const RESERVATION_SET = "ghotel/reservation/RESERVATION_SET"
const AVAILABLE_ROOMS_SET = "ghotel/reservation/AVAILABLE_ROOMS_SET"
const RESERVATION_CLEAR = "ghotel/reservation/RESERVATION_CLEAR"
const CREATE_NEW_RESERVATION = "ghotel/reservation/CREATE_NEW_RESERVATION"
const TOGGLE_ENTRY_MODAL = "ghotel/reservation/TOGGLE_ENTRY_MODAL"
const SET_SALE = "ghotel/reservation/SET_SALE"
const SET_REFUND = "ghotel/reservation/SET_REFUND"
const SALE_END_CHANGED = "ghotel/reservation/SALE_END_CHANGED"
const NOT_PAYED_PRICE_CHANGED = "ghotel/reservation/NOT_PAYED_PRICE_CHANGED"
const NOT_PAYED_METHOD_CHANGED = "ghotel/reservation/NOT_PAYED_METHOD_CHANGED"
const NOT_PAYED_PAYED_CHANGED = "ghotel/reservation/NOT_PAYED_PAYED_CHANGED"
const REFUND_DAY = "ghotel/reservation/REFUND_DAY"
const SET_ERROR_FIELDS = "ghotel/reservation/SET_ERROR_FIELDS"
const CLEAR_ERROR_FIELDS = "ghotel/reservation/CLEAR_ERROR_FIELDS"
const CLOSE_MODAL_WITH_ALL_ERRORS =
  "ghotel/reservation/CLOSE_MODAL_WITH_ALL_ERRORS"
const MIGRATION_DONE = "ghotel/reservation/MIGRATION_DONE"
const REFUND_CHECK_IN = "ghotel/reservation/REFUND_CHECK_IN"
const REFUND_CHECK_OUT = "ghotel/reservation/REFUND_CHECK_OUT"
const SUCCESS_MODAL_SHOW = "ghotel/reservation/SUCCESS_MODAL_SHOW"
const ERROR_MODAL_SHOW = "ghotel/reservation/ERROR_MODAL_SHOW"
const ERROR_MODAL_HID = "ghotel/reservation/ERROR_MODAL_HID"
const SUCCESS_MODAL_HID = "ghotel/reservation/SUCCESS_MODAL_HID"
const LOADING_SHOW = "ghotel/reservation/LOADING_SHOW"
const LOADING_HID = "ghotel/reservation/LOADING_HID"
const INCREASE_CARDS = "ghotel/reservation/INCREASE_CARDS"
const SET_WRITECARD_ERROR = "ghotel/reservation/SET_WRITECARD_ERROR"
const CLEAR_WRITECARD_ERROR = "ghotel/reservation/CLEAR_WRITECARD_ERROR"
const SET_PK = "ghotel/reservation/SET_PK"
const DONT_SETTLE_CHANGED = "ghotel/reservation/DONT_SETTLE_CHANGED"
const SET_DUPLICATES = "ghotel/reservation/SET_DUPLICATES"
const CLEAR_DUPLICATES = "ghotel/reservation/CLEAR_DUPLICATES"
const CONFIRM_DUPLICATE = "ghotel/reservation/CONFIRM_DUPLICATE"
const SET_DAY_PRICES = "ghotel/reservation/SET_DAY_PRICES"
const SET_ROOMS = "ghotel/reservation/SET_ROOMS"
const RESET_CARDS = "ghotel/reservation/RESET_CARDS"
const SHOW_PRINT_SERVER_ERROR = "ghotel/reservation/SHOW_PRINT_SERVER_ERROR"
const HIDE_PRINT_SERVER_ERROR = "ghotel/reservation/HIDE_PRINT_SERVER_ERROR"

const CheckPrintControl = new CheckPrintController()

const initial_state = {
  pk: "",
  start: null,
  end: null,
  initial_end: null,
  check_in_time: "14:00:00",
  check_out_time: "12:00:00",
  initial_reserved_days_count: 0,
  reserved_days: [],
  new_reserved_days: [],
  not_payed_reserved_days: [], // неоплаченные уже существующие дни в редактируемых бронях (те, которые были созданы вместе с бронью и не оплачены. либо были созданы и не оплачены при редактировании)
  payed_reserved_days: [],
  additional_services: [],
  services_error: "",
  upgrades_error: "",
  has_refund: false,
  payed: false,
  guest_name: "",
  booking_number: "",
  admin: "",
  note: "",
  hasCard: 0,
  wubook_note: "",
  guest_phone: "",
  guest_mail: "",
  client_note: "",
  isReady: true,
  guests_number: 1,
  room_description: null,
  duration: 0,
  days: [],
  services: [],
  isShowingEntryModal: false,
  isRefund: false,
  isSale: false,
  refundSumm: 0,
  rooms: [],
  errorFields: [],
  showModalWithAllErrors: false,
  refundedItems: [],
  showSuccessModal: false,
  showErrorModal: false,
  errorPK: null,
  isLoading: false,
  isFetching: true,
  lastRoomID: null,
  newRoomID: null,
  writecardError: null,
  availableRooms: null,
  selectedRoom: null,
  dontSettle: false,
  isDuplicatesFound: false,
  duplicatesData: {
    duplicates: [],
    message: ""
  },
  isDuplicateConfirmed: false,
  dayPrices: null,
  mode: null,
  isPrintServerErrorModalActive: false,
  initialData: {
    reservedDays: [],
    isRefund: false,
    isReady: false
  }
}

export default function reducer(state = initial_state, action) {
  let { payload } = action

  let getUpdatedDays

  const getFirstDay = () =>
    Object.assign(
      {},
      state.new_reserved_days[0]
        ? state.new_reserved_days[0]
        : state.reserved_days[0]
          ? state.reserved_days[0]
          : {
            id: 0,
            price: "",
            date: state.start,
            payment_type: "",
            payment_date: "",
            room: null,
            admin: Cookies.get("admin")
          }
    )
  const getLastDay = () =>
    Object.assign(
      {},
      state.new_reserved_days[state.new_reserved_days.length - 1]
        ? state.new_reserved_days[state.new_reserved_days.length - 1]
        : state.reserved_days[state.reserved_days.length - 1]
          ? state.reserved_days[state.reserved_days.length - 1]
          : {
            id: 0,
            price: "",
            date: state.end,
            payment_type: "",
            payment_date: "",
            room: null,
            admin: Cookies.get("admin")
          }
    )

  switch (action.type) {
    case SHOW_PRINT_SERVER_ERROR:
      return {
        ...state,
        isPrintServerErrorModalActive: true
      }

    case HIDE_PRINT_SERVER_ERROR:
      return {
        ...state,
        isPrintServerErrorModalActive: false
      }

    case RESET_CARDS:
      return {
        ...state,
        hasCard: 0
      }

    case SET_DAY_PRICES:
      return {
        ...state,
        dayPrices: payload.dayPrices,
        reserved_days: payload.reservedDays,
        new_reserved_days: payload.newReservedDays
      }

    case CONFIRM_DUPLICATE:
      return {
        ...state,
        isDuplicateConfirmed: true
      }

    case SET_DUPLICATES:
      return {
        ...state,
        isDuplicatesFound: true,
        duplicatesData: payload
      }

    case CLEAR_DUPLICATES:
      return {
        ...state,
        isDuplicatesFound: false,
        duplicatesData: {
          duplicates: [],
          message: ""
        }
      }

    case DONT_SETTLE_CHANGED:
      return {
        ...state,
        dontSettle: payload
      }

    case SET_PK:
      return {
        ...state,
        pk: payload
      }

    case INCREASE_CARDS:
      return {
        ...state,
        hasCard: state.hasCard + 1,
        writecardError: null
      }

    case SET_WRITECARD_ERROR:
      return {
        ...state,
        writecardError: payload
      }

    case CLEAR_WRITECARD_ERROR:
      return {
        ...state,
        writecardError: null
      }

    case SUCCESS_MODAL_SHOW:
      return {
        ...state,
        showSuccessModal: true
      }

    case SUCCESS_MODAL_HID:
      return {
        ...state,
        showSuccessModal: false
      }

    case ERROR_MODAL_SHOW:
      return {
        ...state,
        showErrorModal: true,
        errorPK: payload
      }

    case ERROR_MODAL_HID:
      return {
        ...state,
        showErrorModal: false,
        errorPK: null
      }

    case LOADING_SHOW:
      return {
        ...state,
        isLoading: true
      }

    case LOADING_HID:
      return {
        ...state,
        isLoading: false
      }

    case REFUND_CHECK_IN:
      return {
        ...state,
        check_in_time: defaultCheckInTime,
        additional_services: state.additional_services.filter(
          s => s.service_type !== "checkin"
        ),
        refundSumm:
          state.refundSumm +
          state.additional_services.find(s => s.service_type === "checkin")
            .price,
        refundedItems: state.refundedItems.concat({
          type: "service",
          name: earlyCheckInService,
          quantity: 1,
          price: state.additional_services.find(
            s => s.service_type === "checkin"
          ).price,
          payment_type: state.additional_services.find(
            s => s.service_type === "checkin"
          ).payment_type,
          date: state.reserved_days[0].date,
          payment_date: state.additional_services.find(
            s => s.service_type === "checkin"
          ).payment_date
        })
      }

    case REFUND_CHECK_OUT:
      return {
        ...state,
        check_out_time: defaultCheckOutTime,
        additional_services: state.additional_services.filter(
          s => s.service_type !== "checkout"
        ),
        refundSumm:
          state.refundSumm +
          state.additional_services.find(s => s.service_type === "checkout")
            .price,
        refundedItems: state.refundedItems.concat({
          type: "service",
          name: lateCheckOutService,
          quantity: 1,
          price: state.additional_services.find(
            s => s.service_type === "checkout"
          ).price,
          payment_type: state.additional_services.find(
            s => s.service_type === "checkout"
          ).payment_type,
          date: state.reserved_days[state.reserved_days.length - 1].date,
          payment_date: state.additional_services.find(
            s => s.service_type === "checkout"
          ).payment_date
        })
      }

    case SET_ROOMS:
      getUpdatedDays = days =>
        days.map(d => ({
          ...d,
          room: payload
        }))

      return {
        ...state,
        reserved_days: getUpdatedDays(state.reserved_days),
        new_reserved_days: getUpdatedDays(state.new_reserved_days),
        payed_reserved_days: getUpdatedDays(state.payed_reserved_days),
        not_payed_reserved_days: getUpdatedDays(state.not_payed_reserved_days),
        newRoomID: payload.room_id,
        lastRoomID: payload.room_id,
        rooms: [{ id: payload.room_id }]
      }

    case MIGRATION_DONE:
      const firstMigrationDayID = state.reserved_days.find(
        d => d.date === payload.date
      ).id
      const getDaysAfterMigration = array =>
        array.map(d => {
          if (+d.id >= +firstMigrationDayID)
            return {
              ...d,
              room: payload.room
            }

          return d
        })

      const nRD = getDaysAfterMigration(state.reserved_days)

      const roomsAfterMigration = []
      if (state.isReady) {
        let lastRoomID = null
        nRD.forEach(d => {
          const room = d.room
          const roomID = room.room_id
          if (roomID !== lastRoomID) {
            roomsAfterMigration.push({
              id: roomID,
              date: roomsAfterMigration.length ? d.date : undefined
            })
            lastRoomID = roomID
          }
        })
      }

      return {
        ...state,
        reserved_days: nRD,
        new_reserved_days: getDaysAfterMigration(state.new_reserved_days),
        payed_reserved_days: getDaysAfterMigration(state.payed_reserved_days),
        not_payed_reserved_days: getDaysAfterMigration(
          state.not_payed_reserved_days
        ),
        rooms: roomsAfterMigration,
        newRoomID: payload.room.room_id,
        hasCard: 0
      }

    case SET_ERROR_FIELDS:
      return {
        ...state,
        errorFields: payload,
        showModalWithAllErrors: true
      }

    case CLEAR_ERROR_FIELDS:
      return {
        ...state,
        errorFields: []
      }

    case CLOSE_MODAL_WITH_ALL_ERRORS:
      return {
        ...state,
        showModalWithAllErrors: false
      }

    case START_CHANGED:
      const __oldDuration = reservationDuration(state.start, state.end)
      const __newDuration = reservationDuration(payload, state.end)
      const newStart = payload
      let _firstDay = getFirstDay()

      _firstDay.pk = 0

      if (__newDuration > __oldDuration) {
        const addedDays = []
        let id = _firstDay.id,
          date = _firstDay.date

        for (let i = __oldDuration; i < __newDuration; i++) {
          date = moment(date)
            .subtract(1, "days")
            .format("YYYY-MM-DD")

          addedDays.unshift({
            ..._firstDay,
            type: "newDay",
            id: --id,
            admin: Cookies.get("admin"),
            date
          })
        }

        return {
          ...state,
          start: newStart,
          duration: __newDuration,
          days: addedDays.concat(state.reserved_days).map(d => d.date),
          reserved_days: addedDays.concat(state.reserved_days),
          new_reserved_days: addedDays.concat(state.new_reserved_days)
        }
      } else {
        const _rDays = state.reserved_days.slice(0)
        const _nRDays = state.new_reserved_days.slice(0)
        for (let i = __newDuration; i < __oldDuration; i++) {
          _rDays.shift()
          _nRDays.shift()
        }

        return {
          ...state,
          start: newStart,
          duration: __newDuration,
          days: _rDays.map(d => d.date),
          reserved_days: _rDays,
          new_reserved_days: _nRDays
        }
      }

    case END_CHANGED:
      const _oldDuration = reservationDuration(state.start, state.end)
      const _newDuration = reservationDuration(state.start, payload)
      const newEnd = payload
      let _lastDay = getLastDay()

      _lastDay.pk = 0

      if (_newDuration > _oldDuration) {
        const addedDays = []
        let id = _lastDay.id,
          date = _lastDay.date

        for (let i = _oldDuration; i < _newDuration; i++) {
          id += 1
          date = moment(date)
            .add(1, "days")
            .format("YYYY-MM-DD")

          addedDays.push({
            ..._lastDay,
            type: "newDay",
            id,
            admin: Cookies.get("admin"),
            date
          })
        }

        return {
          ...state,
          end: newEnd,
          duration: _newDuration,
          days: state.reserved_days.concat(addedDays).map(d => d.date),
          reserved_days: state.reserved_days.concat(addedDays),
          new_reserved_days: state.new_reserved_days.concat(addedDays)
        }
      } else {
        const rDays = state.reserved_days.slice(0)
        const nRDays = state.new_reserved_days.slice(0)
        for (let i = _newDuration; i < _oldDuration; i++) {
          rDays.pop()
          nRDays.pop()
        }

        return {
          ...state,
          end: newEnd,
          duration: _newDuration,
          days: rDays.map(d => d.date),
          reserved_days: rDays,
          new_reserved_days: nRDays
        }
      }

    case SALE_END_CHANGED:
      const saleDuration = reservationDuration(state.end, payload)
      let saleDaysData = []
      let sale_reserved_days_to_sell = []
      let saleDays = []

      for (let index = 0; index < saleDuration; index++) {
        saleDaysData.push({
          date: moment(state.end, "YYYY-MM-DD").add(index + 1, "days"),
          price: "",
          payment_type: ""
        })
      }

      const wholeDuration = reservationDuration(state.start, payload)

      for (let index = 0; index < wholeDuration - 1; index++) {
        saleDays.push(moment(state.start, "YYYY-MM-DD").add(index + 1, "days"))
      }

      saleDaysData.forEach((day, index) => {
        sale_reserved_days_to_sell.push({
          price: saleDaysData[index].price,
          date: saleDaysData[index].date,
          payment_type: saleDaysData[index].payment_type,
          payment_date:
            state.payed === true ? moment().format("YYYY-MM-DD") : "",
          admin: ""
        })
      })

      return {
        ...state,
        end: payload,
        duration: wholeDuration,
        new_reserved_days: sale_reserved_days_to_sell,
        daysData: saleDaysData,
        days: saleDays
      }

    case DAY_PRICE_CHANGED:
      const newDayPrice = payload.price ? +payload.price : ""

      return {
        ...state,
        reserved_days: getArrayWithUpdatedItem(
          state.reserved_days,
          payload.id,
          "price",
          newDayPrice
        ),
        new_reserved_days: getArrayWithUpdatedItem(
          state.new_reserved_days,
          payload.id,
          "price",
          newDayPrice
        )
      }

    case DAY_METHOD_CHANGED:
      return {
        ...state,
        reserved_days: getArrayWithUpdatedItem(
          state.reserved_days,
          payload.id,
          "payment_type",
          payload.payment_type
        ),
        new_reserved_days: getArrayWithUpdatedItem(
          state.new_reserved_days,
          payload.id,
          "payment_type",
          payload.payment_type
        )
      }

    case DAY_PAYED_CHANGED:
      const newDayPayed = payload.payed ? moment().format("YYYY-MM-DD") : ""
      const newRD = getArrayWithUpdatedItem(
        state.reserved_days,
        payload.id,
        "payment_date",
        newDayPayed
      )
      const newNRD = getArrayWithUpdatedItem(
        state.new_reserved_days,
        payload.id,
        "payment_date",
        newDayPayed
      )

      return {
        ...state,
        payed: newRD.every(d => d.payment_date !== ""),
        reserved_days: newRD,
        new_reserved_days: newNRD
      }

    case NOT_PAYED_PRICE_CHANGED:
      const newNPDPrice = payload.price ? +payload.price : ""

      return {
        ...state,
        reserved_days: getArrayWithUpdatedItem(
          state.reserved_days,
          payload.id,
          "price",
          newNPDPrice
        ),
        not_payed_reserved_days: getArrayWithUpdatedItem(
          state.not_payed_reserved_days,
          payload.id,
          "price",
          newNPDPrice
        )
      }

    case NOT_PAYED_METHOD_CHANGED:
      const newNPDMethod = payload.payment_type

      return {
        ...state,
        reserved_days: getArrayWithUpdatedItem(
          state.reserved_days,
          payload.id,
          "payment_type",
          newNPDMethod
        ),
        not_payed_reserved_days: getArrayWithUpdatedItem(
          state.not_payed_reserved_days,
          payload.id,
          "payment_type",
          newNPDMethod
        )
      }

    case NOT_PAYED_PAYED_CHANGED:
      const newNPDPaymentDate = payload.payed
        ? moment().format("YYYY-MM-DD")
        : ""
      const _newRD = getArrayWithUpdatedItem(
        state.reserved_days,
        payload.id,
        "payment_date",
        newNPDPaymentDate
      )

      return {
        ...state,
        payed: _newRD.every(d => d.payment_date !== ""),
        reserved_days: _newRD,
        not_payed_reserved_days: getArrayWithUpdatedItem(
          state.not_payed_reserved_days,
          payload.id,
          "payment_date",
          newNPDPaymentDate
        )
      }

    case DAY_REMOVE:
      return {
        ...state,
        payed: state.reserved_days
          .slice(0, -1)
          .every(d => d.payment_date !== ""),
        end: moment(state.end)
          .subtract(1, "days")
          .format("YYYY-MM-DD"),
        duration: state.duration - 1,
        days: state.days.slice(0, -1),
        reserved_days: state.reserved_days.slice(0, -1),
        new_reserved_days: state.new_reserved_days.slice(0, -1),
        additional_services: state.additional_services.filter(
          s =>
            s.service === earlyCheckInService ||
            s.service === lateCheckOutService ||
            s.date !==
              state.new_reserved_days[state.new_reserved_days.length - 1].date
        )
      }

    case RESERVATION_PAYED_CHANGED:
      const newPD = state.payed ? "" : moment().format("YYYY-MM-DD")
      getUpdatedDays = days =>
        days.map(d => {
          if (d.type !== "payedDay") {
            d.payment_date = newPD
          }
          return d
        })

      return {
        ...state,
        payed: !getUpdatedDays(state.reserved_days).every(
          d => d.type === "payedDay"
        )
          ? !state.payed
          : state.payed,
        reserved_days: getUpdatedDays(state.reserved_days),
        new_reserved_days: getUpdatedDays(state.new_reserved_days),
        not_payed_reserved_days: getUpdatedDays(state.not_payed_reserved_days)
      }

    case SERVICES_SET:
      return { ...state, services: payload }

    case SERVICE_CREATE:
      const service = Object.assign({}, payload)
      service.id = state.additional_services.length
      service.payment_date = moment().format("YYYY-MM-DD")
      service.price = state.services.filter(
        s => s.name === service.service
      )[0].price
      service.admin = Cookies.get("admin")

      return {
        ...state,
        additional_services: state.additional_services.concat(service),
        services_error: ""
      }

    case SERVICE_ERROR_SET:
      return {
        ...state,
        services_error: payload
      }

    case SERVICE_QTY_CHANGED:
      return {
        ...state,
        additional_services: state.additional_services.map(s =>
          s.id === payload.id ? { ...s, quantity: payload.value } : s
        )
      }

    case SERVICE_PRICE_CHANGED:
      return {
        ...state,
        additional_services: state.additional_services.map(s =>
          s.id === payload.id ? { ...s, price: payload.value } : s
        )
      }

    case SERVICE_PAYMENT_CHANGED:
      return {
        ...state,
        additional_services: state.additional_services.map(s =>
          s.id === payload.id ? { ...s, payment_type: payload.value } : s
        )
      }

    case SERVICE_DATE_CHANGED:
      return {
        ...state,
        additional_services: state.additional_services.map(s =>
          s.id === payload.id ? { ...s, date: payload.value } : s
        )
      }

    case SERVICE_DELETE:
      return {
        ...state,
        additional_services: state.additional_services.filter(
          s => s.id !== payload
        )
      }

    case SERVICE_REFUND:
      const refundedService = state.additional_services.find(
        s => s.id === payload
      )

      return {
        ...state,
        additional_services: state.additional_services.filter(
          s => s.id !== payload
        ),
        refundSumm:
          state.refundSumm +
          state.additional_services.find(s => s.id === payload).price *
            state.additional_services.find(s => s.id === payload).quantity,
        refundedItems: state.refundedItems.concat({
          type: "service",
          name: refundedService.service,
          quantity: refundedService.quantity,
          price: refundedService.price,
          payment_type: refundedService.payment_type,
          date: refundedService.date,
          payment_date: refundedService.payment_date
        })
      }

    case SERVICE_COPY:
      let customFields
      switch (
        state.additional_services.find(s => s.id === payload).service_type
      ) {
        case "service":
          customFields = {
            payed: undefined,
            pk: undefined
          }
          break

        case "upgrade":
          customFields = {
            payed: undefined,
            pk: undefined,
            date: ""
          }
          break

        default:
          customFields = {}
      }

      return {
        ...state,
        additional_services: [
          ...state.additional_services,
          {
            ...state.additional_services.find(s => s.id === payload),
            id: state.additional_services.length,
            payment_date: moment().format("YYYY-MM-DD"),
            admin: Cookies.get("admin"),
            ...customFields
          }
        ]
      }

    case DAY_ADD:
      let lastDay = getLastDay()
      let newDayID = state.reserved_days.length
      let newDayDate = moment(lastDay.date)
        .add(1, "days")
        .format("YYYY-MM-DD")
      let newPrice = state.dayPrices[newDayDate]

      const newDay = {
        ...lastDay,
        pk: 0,
        id: newDayID,
        date: newDayDate,
        payment_date: lastDay.payment_date ? moment().format("YYYY-MM-DD") : "",
        type: "newDay",
        price: newPrice || "",
        admin: Cookies.get("admin")
      }

      return {
        ...state,
        end: moment(state.end)
          .add(1, "days")
          .format("YYYY-MM-DD"),
        duration: state.duration + 1,
        days: state.days.concat(newDayDate),
        reserved_days: state.reserved_days.concat(newDay),
        new_reserved_days: state.new_reserved_days.concat(newDay)
      }

    case REFUND_DAY:
      const deletedDay = state.reserved_days.find(d => d.id === payload)

      const refundedSumm = deletedDay.payment_date ? deletedDay.price : 0

      return {
        ...state,
        payed: state.reserved_days
          .filter(d => d.id !== payload)
          .every(d => d.payment_date !== ""),
        end: moment(state.end)
          .subtract(1, "days")
          .format("YYYY-MM-DD"),
        duration: state.duration - 1,
        days: state.days.slice(0, -1),
        reserved_days: state.reserved_days.filter(d => d.id !== payload),
        new_reserved_days: state.new_reserved_days.filter(
          d => d.id !== payload
        ),
        payed_reserved_days: state.payed_reserved_days.filter(
          d => d.id !== payload
        ),
        not_payed_reserved_days: state.not_payed_reserved_days.filter(
          d => d.id !== payload
        ),
        refundSumm: state.refundSumm + refundedSumm,
        refundedItems: state.refundedItems.concat({
          type: "day",
          price: refundedSumm,
          payment_type: deletedDay.payment_type,
          payment_date: deletedDay.payment_date,
          date: deletedDay.date
        })
      }

    case DAY_SUMM_COPY:
      return {
        ...state,
        reserved_days: [
          ...state.reserved_days.filter(
            d => d.type !== "newDay" && d.type !== "notPayedDay"
          ),
          ...state.reserved_days
            .filter(d => d.type === "newDay" || d.type === "notPayedDay")
            .map(d => ({
              ...d,
              price: state.reserved_days[0].price
            }))
        ],
        new_reserved_days: state.new_reserved_days.map(d => ({
          ...d,
          price: state.reserved_days[0].price
        })),
        not_payed_reserved_days: state.not_payed_reserved_days.map(d => ({
          ...d,
          price: state.reserved_days[0].price
        }))
      }

    case DAY_METHOD_COPY:
      return {
        ...state,
        reserved_days: [
          ...state.reserved_days.filter(
            d => d.type !== "newDay" && d.type !== "notPayedDay"
          ),
          ...state.reserved_days
            .filter(d => d.type === "newDay" || d.type === "notPayedDay")
            .map(d => ({
              ...d,
              payment_type: state.reserved_days[0].payment_type
            }))
        ],
        new_reserved_days: state.new_reserved_days.map(d => ({
          ...d,
          payment_type: state.reserved_days[0].payment_type
        })),
        not_payed_reserved_days: state.not_payed_reserved_days.map(d => ({
          ...d,
          payment_type: state.reserved_days[0].payment_type
        }))
      }

    case NAME_CHANGED:
      return { ...state, guest_name: payload }

    case PHONE_CHANGED:
      return { ...state, guest_phone: payload }

    case GUESTS_NUMBER_CHANGED:
      return { ...state, guests_number: payload }

    case MAIL_CHANGED:
      return { ...state, guest_mail: payload }

    case BOOKING_NUMBER_CHANGED:
      return { ...state, booking_number: payload }

    case NOTE_CHANGED:
      return { ...state, note: payload }

    case CLIENT_NOTE_CHANGED:
      return { ...state, client_note: payload }

    case WUBOOK_NOTE_CHANGED:
      return { ...state, wubook_note: payload }

    case REFUND_CHANGED:
      return { ...state, has_refund: payload }

    case UPGRADE_CREATE:
      const upgrade = Object.assign({}, payload)
      upgrade.id = state.additional_services.length
      upgrade.admin = Cookies.get("admin")

      return {
        ...state,
        additional_services: state.additional_services.concat(upgrade),
        upgrades_error: ""
      }

    case UPGRADE_ERROR_SET:
      return {
        ...state,
        upgrades_error: payload
      }

    case UPGRADE_ERROR_CLEAR:
      return {
        ...state,
        upgrades_error: ""
      }

    case UPGRADE_DELETE:
      return {
        ...state,
        additional_services: state.additional_services.filter(
          s => s.id !== payload
        ),
        refundSumm:
          state.refundSumm +
          state.additional_services.find(s => s.id === payload).price
      }

    case CHECK_IN_CHANGED:
      const newCheckInTime = payload.time
        ? payload.time.substr(0, 5) + ":00"
        : state.check_in_time
      const servicesWithoutCheckIn = state.additional_services.filter(
        s => s.service_type !== "checkin"
      )
      const checkInService = state.additional_services.find(
        s => s.service_type === "checkin"
      )
      const checkInPayload = omit(payload, "time")
      checkInPayload.admin = Cookies.get("admin")

      return {
        ...state,
        check_in_time: newCheckInTime,
        additional_services: checkInService
          ? servicesWithoutCheckIn.concat({
            ...checkInService,
            ...checkInPayload
          })
          : state.additional_services.concat(checkInPayload)
      }

    case CHECK_OUT_CHANGED:
      const newCheckOutTime = payload.time
        ? payload.time.substr(0, 5) + ":00"
        : state.check_out_time
      const servicesWithoutCheckOut = state.additional_services.filter(
        s => s.service_type !== "checkout"
      )
      const checkOutService = state.additional_services.find(
        s => s.service_type === "checkout"
      )
      const checkOutPayload = omit(payload, "time")
      checkOutPayload.admin = Cookies.get("admin")

      return {
        ...state,
        check_out_time: newCheckOutTime,
        additional_services: checkOutService
          ? servicesWithoutCheckOut.concat({
            ...checkOutService,
            ...checkOutPayload
          })
          : state.additional_services.concat(checkOutPayload)
      }

    case RESERVATION_SET:
      const newDuration = reservationDuration(payload.start, payload.end)
      let updatedDays = []

      for (let index = 0; index < newDuration; index++) {
        updatedDays.push(
          moment(payload.start)
            .add(index, "days")
            .format("YYYY-MM-DD")
        )
      }

      const updatedReservedDays = payload.reserved_days.map((item, id) => {
        item.id = id
        if (item.payment_date !== "") {
          item.type = "payedDay"
        } else {
          item.type = "notPayedDay"
        }
        return item
      })

      let rooms = []
      let roomsID = []
      if (payload.isReady) {
        updatedReservedDays.forEach(d => {
          const room = d.room
          const roomID = room.room_id
          if (!roomsID.includes(roomID)) {
            rooms.push({
              id: roomID,
              date: rooms.length ? d.date : undefined
            })
            roomsID.push(roomID)
          }
        })
      }

      payload.additional_services = payload.additional_services.map((s, id) => {
        let sType = ""
        if (state.services.some(sv => s.service === sv.name)) {
          sType = "service"
        }
        if (s.service === upgradeService) {
          sType = "upgrade"
        }
        if (s.service === earlyCheckInService) {
          sType = "checkin"
        }
        if (s.service === lateCheckOutService) {
          sType = "checkout"
        }
        return {
          ...s,
          service_type: sType,
          id: id,
          payed: true
        }
      })

      return {
        ...state,
        pk: payload.pk,
        guest_name: payload.guest_name,
        room_description: payload.room_names,
        booking_number: payload.booking_number,
        guest_phone: payload.guest_phone,
        guest_mail: payload.guest_mail,
        note: payload.note,
        wubook_note: payload.wubook_note,
        isReady: payload.isReady,
        start: payload.start,
        end: payload.end,
        initial_end: payload.end,
        check_in_time: payload.check_in_time,
        check_out_time: payload.check_out_time,
        admin: payload.admin,
        additional_services: payload.additional_services,
        initial_reserved_days_count: updatedReservedDays.length,
        reserved_days: updatedReservedDays,
        new_reserved_days: [],
        not_payed_reserved_days: updatedReservedDays.filter(
          s => s.payment_date === ""
        ),
        payed_reserved_days: updatedReservedDays.filter(
          s => s.payment_date !== ""
        ),
        has_refund: payload.has_refund,
        payed: payload.payed,
        hasCard: payload.hasCard,
        client_note: payload.client_note,
        guests_number: payload.guests_number,
        duration: newDuration,
        days: updatedDays,
        lastRoomID: roomsID[roomsID.length - 1] || null,
        rooms,
        isFetching: false,
        mode: "edit",
        initialData: {
          reservedDays: updatedReservedDays,
          hasRefund: payload.has_refund,
          isReady: payload.isReady
        }
      }

    case AVAILABLE_ROOMS_SET:
      return {
        ...state,
        availableRooms: payload
      }

    case CREATE_NEW_RESERVATION:
      const createdDays = []
      const daysDates = []
      let cDate = payload.start
      let id = 0
      while (cDate !== payload.end) {
        createdDays.push({
          date: cDate,
          price: "",
          payment_type: "",
          payment_date: moment().format("YYYY-MM-DD"),
          room: {
            room_id: payload.room,
            name: payload.roomType
          },
          admin: Cookies.get("admin"),
          id: id++,
          type: "newDay"
        })
        daysDates.push(cDate)
        cDate = moment(cDate)
          .add(1, "days")
          .format("YYYY-MM-DD")
      }

      return {
        ...state,
        payed: true,
        start: payload.start,
        end: payload.end,
        reserved_days: createdDays,
        new_reserved_days: createdDays,
        days: daysDates,
        duration: id,
        rooms: [
          {
            id: payload.room
          }
        ],
        lastRoomID: payload.room,
        admin: Cookies.get("admin"),
        isFetching: false,
        mode: "create"
      }

    case RESERVATION_CLEAR:
      return initial_state

    case TOGGLE_ENTRY_MODAL:
      return { ...state, isShowingEntryModal: payload }

    case SET_REFUND:
      return { ...state, isRefund: true, isShowingEntryModal: false }

    case SET_SALE:
      return { ...state, isSale: true, isShowingEntryModal: false }

    default:
      return state
  }
}

export const updateCheckTimes = (isFirstDayChanged, isLastDayChanged) => (
  dispatch,
  getState
) => {
  const r = getState().reservation
  const isEarlyCheckInExists = r.additional_services.some(
    s => s.service_type === "checkin"
  )
  const isLateCheckOutExists = r.additional_services.some(
    s => s.service_type === "checkout"
  )
  if (isFirstDayChanged && isEarlyCheckInExists) {
    const newFirstDay = r.reserved_days[0]
    dispatch(onCheckInDateChange(newFirstDay.date))
    dispatch({
      type: CHECK_IN_TIME_CHANGED,
      payload: {
        payedDays: r.payed_reserved_days,
        notPayedDays: r.not_payed_reserved_days,
        newDays: r.new_reserved_days,
        currentTime: r.check_in_time,
        newTime: r.check_in_time,
        newPrice: newFirstDay.price
      }
    })
    const checkin = getState().checkin

    Logs.logHiddenAction(
      `[Ранний заезд] date перед экшном CHECK_IN_CHANGED: ${checkin.date}`
    )
    Logs.logHiddenAction(
      `[Ранний заезд] payment_date перед экшном CHECK_IN_CHANGED: ${
        checkin.payment_date
      }`
    )

    Logs.logEvent("Пересчитана стоимость раннего заезда")
    dispatch({
      type: CHECK_IN_CHANGED,
      payload: {
        ...checkin,
        time: r.check_in_time
      }
    })

    const newCheckin = getState().checkin
    Logs.logHiddenAction(
      `[Ранний заезд] date после изменений в днях: ${newCheckin.date}`
    )
    Logs.logHiddenAction(
      `[Ранний заезд] payment_date после изменений в днях: ${
        newCheckin.payment_date
      }`
    )
  }
  if (isLastDayChanged && isLateCheckOutExists) {
    const newLastDay = r.reserved_days[r.reserved_days.length - 1]
    dispatch(onCheckOutDateChange(newLastDay.date))
    dispatch({
      type: CHECK_OUT_TIME_CHANGED,
      payload: {
        payedDays: r.payed_reserved_days,
        notPayedDays: r.not_payed_reserved_days,
        newDays: r.new_reserved_days,
        currentTime: r.check_out_time,
        newTime: r.check_out_time,
        newPrice: newLastDay.price
      }
    })
    const checkout = getState().checkout

    Logs.logHiddenAction(
      `[Поздний выезд] date перед экшном CHECK_OUT_CHANGED: ${checkout.date}`
    )
    Logs.logHiddenAction(
      `[Поздний выезд] payment_date перед экшном CHECK_OUT_CHANGED: ${
        checkout.payment_date
      }`
    )

    Logs.logEvent("Пересчитана стоимость позднего выезда")
    dispatch({
      type: CHECK_OUT_CHANGED,
      payload: {
        ...checkout,
        time: r.check_out_time
      }
    })

    const newCheckout = getState().checkout
    Logs.logHiddenAction(
      `[Поздний выезд] date после изменений в днях: ${newCheckout.date}`
    )
    Logs.logHiddenAction(
      `[Поздний выезд] payment_date после изменений в днях: ${
        newCheckout.payment_date
      }`
    )
  }
}

const getDayData = day => ({
  price: day.price || 0,
  method: day.payment_type || "Не выбрано",
  payed: day.payment_date ? "Оплачено" : "Не оплачено"
})

export const onDayPriceChange = (id, newPrice) => (dispatch, getState) => {
  dispatch({ type: DAY_PRICE_CHANGED, payload: { id, price: newPrice } })
  const days = getState().reservation.reserved_days
  const day = days.find(d => d.id === id)
  const { price, method, payed } = getDayData(day)
  Logs.logEditAction(`${price} | ${method} | ${payed}`, `День ${day.date}`)
  dispatch(updateCheckTimes(days[0].id === id, days[days.length - 1].id === id))
}

export const onDayMethodChange = (id, newPaymentType) => (
  dispatch,
  getState
) => {
  dispatch({
    type: DAY_METHOD_CHANGED,
    payload: { id, payment_type: newPaymentType }
  })
  const days = getState().reservation.reserved_days
  const day = days.find(d => d.id === id)
  const { price, method, payed } = getDayData(day)
  Logs.logEditAction(`${price} | ${method} | ${payed}`, `День ${day.date}`)
}

export const onDayPayedChange = (id, newPayed) => (dispatch, getState) => {
  dispatch({ type: DAY_PAYED_CHANGED, payload: { id, payed: newPayed } })
  const days = getState().reservation.reserved_days
  const day = days.find(d => d.id === id)
  const { price, method, payed } = getDayData(day)
  Logs.logEditAction(`${price} | ${method} | ${payed}`, `День ${day.date}`)
}

export const onDayRefund = id => (dispatch, getState) => {
  const reservedDays = getState().reservation.reserved_days
  if (reservedDays.length === 1) {
    if (
      !window.confirm(
        "Вы уверены, что хотите возвратить последний день брони? \nБронь без дней будет удалена после сохранения."
      )
    )
      return
  }
  const day = getState().reservation.reserved_days.find(d => d.id === id)
  dispatch({ type: REFUND_DAY, payload: id })
  Logs.logPressAction(`Возврат денег за проживание ${day.date}`)
}

export const onReservationPayedChange = isPayed => dispatch => {
  dispatch({ type: RESERVATION_PAYED_CHANGED, payload: isPayed })
  Logs.logPressAction(
    `Выбрать все дни оплаченными => ${isPayed ? "Да" : "Нет"}`
  )
}

export const getServices = () => dispatch => {
  return getServicesCall().then(payload => {
    dispatch({ type: SERVICES_SET, payload })
  })
}

export const getReservation = pk => async dispatch => {
  const reservation = Object.assign({}, await getReservationCall(pk))

  let selectedReservation
  if (reservation.isReady) {
    selectedReservation = reservation
    dispatch({ type: RESERVATION_SET, payload: reservation })
  } else {
    const wubook = Object.assign({}, await getWubookCall(pk))
    selectedReservation = wubook
    const availableRooms = await getAvailableRoomsCall(
      moment(wubook.start).format("YYYY-MM-DD"),
      moment(wubook.end).format("YYYY-MM-DD")
    )
    dispatch({ type: AVAILABLE_ROOMS_SET, payload: availableRooms })
    dispatch({ type: RESERVATION_SET, payload: wubook })
  }
  dispatch(updateDayPrices(true))

  const checkInService = selectedReservation.additional_services.find(
    s => s.service === earlyCheckInService
  )
  const checkOutService = selectedReservation.additional_services.find(
    s => s.service === lateCheckOutService
  )
  if (checkInService) {
    dispatch({
      type: CHECK_IN_SET,
      payload: {
        ...checkInService,
        time: selectedReservation.check_in_time,
        pk: undefined
      }
    })
  }
  if (checkOutService) {
    dispatch({
      type: CHECK_OUT_SET,
      payload: {
        ...checkOutService,
        time: selectedReservation.check_out_time,
        pk: undefined
      }
    })
  }
}

export const onServiceCreate = service => dispatch => {
  Logs.logPressAction("Создать услугу")
  const serviceRequiredFields = ["date", "service", "quantity", "payment_type"]

  for (let key of serviceRequiredFields) {
    if (!service[key]) {
      dispatch({
        type: SERVICE_ERROR_SET,
        payload: "Все поля должны быть заполнены!"
      })
      Logs.logError("Не все поля новой услуги заполнены")
      return
    }
  }

  Logs.logHiddenAction(`[Услуга] date при создании: ${service.date}`)
  Logs.logHiddenAction(
    `[Услуга] payment_date при создании: ${service.payment_date}`
  )

  dispatch({ type: SERVICE_CREATE, payload: service })

  Logs.logEvent(
    `Создана услуга ${service.service} (${service.quantity} шт) за ${
      service.date
    } общей стоимостью ${service.price * service.quantity} ₽ (${
      service.payment_type
    })`
  )
  dispatch({ type: SERVICE_CLEAR })
}

export const onSaleEndDateChange = date => {
  return { type: SALE_END_CHANGED, payload: date }
}

export const onCreatedServiceQtyChange = (value, id) => (
  dispatch,
  getState
) => {
  const service = getState().reservation.additional_services.find(
    s => s.id === id
  )
  dispatch({ type: SERVICE_QTY_CHANGED, payload: { value, id } })
  Logs.logEditAction(
    value,
    `Количество услуги ${service.service} за ${service.date}`
  )
}

export const onCreatedServicePriceChange = (value, id) => (
  dispatch,
  getState
) => {
  const service = getState().reservation.additional_services.find(
    s => s.id === id
  )
  dispatch({ type: SERVICE_PRICE_CHANGED, payload: { value, id } })
  Logs.logEditAction(value, `Цена услуги ${service.service} за ${service.date}`)
}

export const onCreatedServicePaymentChange = (value, id) => (
  dispatch,
  getState
) => {
  const service = getState().reservation.additional_services.find(
    s => s.id === id
  )
  dispatch({ type: SERVICE_PAYMENT_CHANGED, payload: { value, id } })
  Logs.logEditAction(
    value,
    `Метод оплаты услуги ${service.service} за ${service.date}`
  )
}

export const onServiceCopyClick = id => (dispatch, getState) => {
  const s = getState().reservation.additional_services.find(sr => sr.id === id)
  dispatch({ type: SERVICE_COPY, payload: id })
  Logs.logPressAction(`Копировать ${s.service} за ${s.date}`)
}

export const onServiceDeleteClick = id => (dispatch, getState) => {
  const s = getState().reservation.additional_services.find(sr => sr.id === id)
  dispatch({ type: SERVICE_DELETE, payload: id })
  Logs.logPressAction(`Удалить ${s.service} за ${s.date}`)
}

export const onServiceRefundClick = id => (dispatch, getState) => {
  const s = getState().reservation.additional_services.find(sr => sr.id === id)
  dispatch({ type: SERVICE_REFUND, payload: id })
  Logs.logPressAction(`Возврат ${s.service} за ${s.date}`)
}

export const onCreatedServiceDateChange = (value, id) => (
  dispatch,
  getState
) => {
  const s = getState().reservation.additional_services.find(sr => sr.id === id)
  dispatch({ type: SERVICE_DATE_CHANGED, payload: { value, id } })
  Logs.logEditAction(value, `Дата услуги ${s.service}`)
}

export const onReservationDayAdd = () => dispatch => {
  dispatch({ type: DAY_ADD })
  Logs.logPressAction("Добавить день")
  dispatch(updateCheckTimes(false, true))
}

export const onReservationDayRemove = () => dispatch => {
  dispatch({ type: DAY_REMOVE })
  Logs.logPressAction("Удалить день")
  dispatch(updateCheckTimes(false, true))
}

export const onStartChange = payload => dispatch => {
  dispatch({ type: START_CHANGED, payload })
  Logs.logEditAction(payload, "Начало брони")
  dispatch(updateDayPrices())
  dispatch(updateCheckTimes(true, false))
}

export const onEndChange = payload => dispatch => {
  dispatch({ type: END_CHANGED, payload })
  Logs.logEditAction(payload, "Конец брони")
  dispatch(updateDayPrices())
  dispatch(updateCheckTimes(false, true))
}

export const onDaySummCopy = () => dispatch => {
  dispatch({ type: DAY_SUMM_COPY })
  Logs.logPressAction("Копировать цену с первого дня")
  dispatch(updateCheckTimes(false, true))
}

export const onDayMethodCopy = () => dispatch => {
  dispatch({ type: DAY_METHOD_COPY })
  Logs.logPressAction("Копировать метод оплаты с первого дня")
}

export const onNameChange = payload => dispatch => {
  dispatch({ type: NAME_CHANGED, payload })
  Logs.logEditAction(payload, "Имя гостя")
}

export const onPhoneChange = payload => dispatch => {
  dispatch({ type: PHONE_CHANGED, payload })
  Logs.logEditAction(payload, "Телефон гостя")
}

export const onGuestsNumberChange = payload => dispatch => {
  dispatch({ type: GUESTS_NUMBER_CHANGED, payload })
  Logs.logEditAction(payload, "Количество гостей")
  dispatch(updateDayPrices(true))
}

export const onMailChange = payload => dispatch => {
  dispatch({ type: MAIL_CHANGED, payload })
  Logs.logEditAction(payload, "Почта гостя")
}

export const onBookingNumberChange = payload => dispatch => {
  if (!/^$|[а-яА-ЯёЁa-zA-Z0-9.]+$/.test(payload)) return
  dispatch({ type: BOOKING_NUMBER_CHANGED, payload })
  Logs.logEditAction(payload, "Номер брони")
}

export const onNoteChange = payload => dispatch => {
  dispatch({ type: NOTE_CHANGED, payload })
  Logs.logEditAction(payload, "Примечание")
}

export const onClientNoteChange = payload => dispatch => {
  dispatch({ type: CLIENT_NOTE_CHANGED, payload })
  Logs.logEditAction(payload, "Примечание клиента")
}

export const onWubookNoteChange = payload => dispatch => {
  dispatch({ type: WUBOOK_NOTE_CHANGED, payload })
  Logs.logEditAction(payload, "Примечание Wubook")
}

export const onRefundChange = payload => dispatch => {
  dispatch({ type: REFUND_CHANGED, payload })
  Logs.logPressAction(`Бронь невозвратная? => ${payload ? "Да" : "Нет"}`)
}

export const onUpgradeCreate = upgrade => (dispatch, getState) => {
  Logs.logPressAction("Создать улучшение")
  const upgradeRequiredFields = ["date", "price", "payment_type"]
  const state = getState().reservation

  for (let key of upgradeRequiredFields) {
    if (!upgrade[key]) {
      dispatch({
        type: UPGRADE_ERROR_SET,
        payload: "Все поля должны быть заполнены"
      })
      Logs.logError("При создании улучшения не заполнены все поля")
      return
    }
  }

  if (
    state.additional_services.some(
      sv => sv.service_type === "upgrade" && sv.date === upgrade.date
    )
  ) {
    dispatch({
      type: UPGRADE_ERROR_SET,
      payload: "Нельзя создать 2 улучшения в один день"
    })
    Logs.logError("Нельзя создать 2 улучшения в один день")
    return
  }

  Logs.logHiddenAction(`[Улучшение] date при создании: ${upgrade.date}`)
  Logs.logHiddenAction(
    `[Улучшение] payment_date при создании: ${upgrade.payment_date}`
  )

  dispatch({ type: UPGRADE_CREATE, payload: upgrade })
  Logs.logEvent(
    `Создано улучшение за дату ${upgrade.date} стоимостью ${upgrade.price} ₽ (${
      upgrade.payment_type
    })`
  )
  dispatch({ type: UPGRADE_CLEAR })
}

export const setUpgradeError = error => ({
  type: UPGRADE_ERROR_SET,
  payload: error
})

export const clearUpgradeError = error => ({
  type: UPGRADE_ERROR_CLEAR
})

export const onUpgradeDelete = id => (dispatch, getState) => {
  Logs.logPressAction("Удалить улучшение")
  const s = getState().reservation.additional_services.find(sr => sr.id === id)
  dispatch({ type: UPGRADE_DELETE, payload: id })
  Logs.logEvent(`Удалено улучшение за ${s.date}`)
}

export const onDontSettleChange = payload => dispatch => {
  dispatch({ type: DONT_SETTLE_CHANGED, payload })
  Logs.logPressAction(`Не заселять => ${payload ? "Да" : "Нет"}`)
}

export const onCheckInChange = payload => {
  return { type: CHECK_IN_CHANGED, payload }
}

export const onCheckOutChange = payload => {
  return { type: CHECK_OUT_CHANGED, payload }
}

export const toggleEntryModal = isShowing => {
  return { type: TOGGLE_ENTRY_MODAL, payload: isShowing }
}

export const onSaleClick = () => {
  return { type: SET_SALE }
}

export const onRefundClick = () => {
  return { type: SET_REFUND }
}

export const onNotPayedDayPriceChange = (id, newPrice) => (
  dispatch,
  getState
) => {
  dispatch({ type: NOT_PAYED_PRICE_CHANGED, payload: { id, price: newPrice } })
  const days = getState().reservation.reserved_days
  const day = days.find(d => d.id === id)
  const { price, method, payed } = getDayData(day)
  Logs.logEditAction(`${price} | ${method} | ${payed}`, `День ${day.date}`)
  dispatch(updateCheckTimes(id === 0, id === days.length - 1))
}

export const onNotPayedDayMethodChange = (id, newPaymentType) => (
  dispatch,
  getState
) => {
  dispatch({
    type: NOT_PAYED_METHOD_CHANGED,
    payload: { id, payment_type: newPaymentType }
  })
  const days = getState().reservation.reserved_days
  const day = days.find(d => d.id === id)
  const { price, method, payed } = getDayData(day)
  Logs.logEditAction(`${price} | ${method} | ${payed}`, `День ${day.date}`)
}

export const onNotPayedDayPayedChange = (id, newPayed) => (
  dispatch,
  getState
) => {
  dispatch({ type: NOT_PAYED_PAYED_CHANGED, payload: { id, payed: newPayed } })
  const days = getState().reservation.reserved_days
  const day = days.find(d => d.id === id)
  const { price, method, payed } = getDayData(day)
  Logs.logEditAction(`${price} | ${method} | ${payed}`, `День ${day.date}`)
}

export const closeModalWithAllErrors = () => ({
  type: CLOSE_MODAL_WITH_ALL_ERRORS
})

export const saveReservation = ({ modes, showWritecardModal }) => async (
  dispatch,
  getState
) => {
  Logs.logPressAction("Сохранить бронь")
  let r = Object.assign({}, getState().reservation)

  // Remove early check-in or late check-out services
  // if they have an empty price (the payment method was chosen, but the check-time is default)
  const filteredServices = []
  Object.assign({}, r).additional_services.forEach(s => {
    if (
      s.service === earlyCheckInService ||
      s.service === lateCheckOutService
    ) {
      if (!s.price || !s.payment_type) return
    }
    filteredServices.push(s)
  })
  r = {
    ...r,
    additional_services: filteredServices
  }

  /*
   *  Logging
   */

  const earlyCheckIn = r.additional_services.find(
    s => s.service === earlyCheckInService
  )
  if (earlyCheckIn) {
    Logs.logHiddenAction(
      `[Сохранение] дата раннего заезда: ${earlyCheckIn.date}`
    )
  }

  const lateCheckOut = r.additional_services.find(
    s => s.service === lateCheckOutService
  )
  if (lateCheckOut) {
    Logs.logHiddenAction(
      `[Сохранение] дата позднего выезда: ${lateCheckOut.date}`
    )
  }

  const services = r.additional_services.filter(s =>
    r.services.map(sl => sl.name).includes(s.service)
  )
  const servicesDates = services.map(s => s.date)
  if (services.length) {
    Logs.logHiddenAction(`[Сохранение] даты услуг: ${servicesDates.join(", ")}`)
  }

  const upgrades = r.additional_services.filter(
    s => s.service === upgradeService
  )
  const upgradesDates = upgrades.map(s => s.date)
  if (upgrades.length) {
    Logs.logHiddenAction(
      `[Сохранение] даты улучшений: ${upgradesDates.join(", ")}`
    )
  }

  if (r.additional_services.find(s => !s.payed)) {
    Logs.sendToEMail(r.pk)
  }

  const errorFields = getErrorFields(Object.assign({}, r), modes)

  if (errorFields.length) {
    dispatch({ type: SET_ERROR_FIELDS, payload: errorFields })
    return
  } else {
    dispatch({ type: CLEAR_ERROR_FIELDS })
  }

  // If reservation is wubook, we should set admin fields to current admin name
  if (!r.isReady) {
    r.admin = Cookies.get("admin")
    r.reserved_days = r.reserved_days.map(d => {
      d.admin = Cookies.get("admin")
      return d
    })
    r.additional_services = r.additional_services.map(s => {
      s.admin = Cookies.get("admin")
      return s
    })
  }

  // If this is wubook reservation, set isReady to true
  if (!r.isReady) {
    r.isReady = true
  }

  if (!isReservationValid(Object.assign({}, r))) {
    alert(
      "Бронь некорректна. Служебная информация об ошибке записана в консоль."
    )
    return
  }

  if (modes.refund && r.initial_reserved_days_count > r.reserved_days.length) {
    window.alert(
      "Дни проживания изменены. Уведомите бухгалтерию об изменении дат проживания согласно инструкциям."
    )
  }

  dispatch({ type: LOADING_SHOW })

  // Do need to print check on this editing
  const needToPrint = await doNeedToPrint(Object.assign({}, r), modes)

  // Is server running
  const isServerOK = await isServerRunning()

  // If we need to print check but server is off,
  // show error modal and exit this function
  if (!isServerOK && needToPrint) {
    dispatch({ type: LOADING_HID })
    dispatch(showPrintServerError())
    return
  }

  let isPrintingFinished = false
  if (needToPrint) {
    let dataFunc, printFunc
    if (modes.refund) {
      dataFunc = getRefundCheckData
      printFunc = CheckPrintControl.printRefundCheck
    } else {
      dataFunc = getSellCheckData
      printFunc = CheckPrintControl.printSellCheck
    }
    while (!isPrintingFinished) {
      const checkData = dataFunc(Object.assign({}, r))
      const res = await printFunc(checkData)

      // If an error occurred
      // Ask the user: try to print check again or not
      if (res && res.Error) {
        const { escapeError } = res

        let confirmation

        // In some cases isn't necessary to show confirm modal
        // Because some actions lead to another modal that is shown before this modal
        // We don't need to ask user 2 times to end a check print process
        // Variable escapeError is used for it
        if (escapeError) {
          confirmation = false
        } else {
          confirmation = window.confirm(
            `При печати чека произошла ошибка: \n\n${
              res.Error
            } \n\nПопробовать снова?`
          )
        }

        // Do new iteration if confirm returned true
        // Or exit from while cycle if it returned false (in this case isPrintingFinished will be false)
        if (confirmation) {
          continue
        } else {
          break
        }
      } else if (typeof res !== "object") {
        // Res isn't an object, so we haven't printed a check
        break
      }

      // If there is no error
      isPrintingFinished = true
    }
  }

  // Save only when we don't need to print,
  // or when we need to do it and printing is already finished
  if (!needToPrint || isPrintingFinished) {
    saveReservationCall(Object.assign({}, r))
      .then(
        async res => {
          Logs.logEvent("Бронь успешно сохранена")

          if (r.reserved_days.length === 0) {
            deleteReservationCall(r.pk)
            history.push("/")
          }

          const diff = r.reserved_days.length - r.initial_reserved_days_count
          let needToWritecard = false
          if (
            r.hasCard &&
            diff > 0 &&
            r.reserved_days.slice(-diff).some(d => !!d.payment_date)
          ) {
            needToWritecard = true
            dispatch(resetCardsSync())
            await updateCardsCall(r.pk, 0)
          }

          dispatch({ type: SUCCESS_MODAL_SHOW })
          if (needToWritecard) {
            setTimeout(() => {
              window.alert(
                "Карты были сброшены, так как в бронь добавлены новые оплаченные дни. Перезапишите карты."
              )
              showWritecardModal()
            }, 500)
          }
        },
        err => {
          console.log(Object.assign({}, err))
          console.log(err.message)
          Logs.logEvent(
            "Один или несколько новых дней невозможно добавить, так как они принадлежат другой брони"
          )
          dispatch({
            type: ERROR_MODAL_SHOW,
            payload:
              (err &&
                err.response &&
                err.response.data &&
                err.response.data[0]) ||
              "[Неизвестный номер]"
          })
        }
      )
      .then(() => {
        dispatch({ type: LOADING_HID })
      })
      .then(() => {
        Logs.send(r.pk)
      })
  } else {
    dispatch({ type: LOADING_HID })
  }
}

export const hideSuccessModal = () => ({
  type: SUCCESS_MODAL_HID
})

export const hideErrorModal = () => ({
  type: ERROR_MODAL_HID
})

const showPrintServerError = () => ({
  type: SHOW_PRINT_SERVER_ERROR
})

export const hidePrintServerError = () => ({
  type: HIDE_PRINT_SERVER_ERROR
})

export const saveNewReservation = () => async (dispatch, getState) => {
  Logs.logPressAction("Создать новую бронь")
  let r = Object.assign({}, {
    ...getState().reservation,
    pk: undefined
  })

  const modes = {
    create: true
  }

  // Remove early check-in or late check-out services
  // if they have an empty price (the payment method was chosen, but the check-time is default)
  const filteredServices = []
  Object.assign({}, r).additional_services.forEach(s => {
    if (
      s.service === earlyCheckInService ||
      s.service === lateCheckOutService
    ) {
      if (!s.price || !s.payment_type) return
    }
    filteredServices.push(s)
  })
  r = {
    ...r,
    additional_services: filteredServices
  }

  /*
   *  Logging
   */

  const earlyCheckIn = r.additional_services.find(
    s => s.service === earlyCheckInService
  )
  if (earlyCheckIn) {
    Logs.logHiddenAction(
      `[Сохранение] дата раннего заезда: ${earlyCheckIn.date}`
    )
  }

  const lateCheckOut = r.additional_services.find(
    s => s.service === lateCheckOutService
  )
  if (lateCheckOut) {
    Logs.logHiddenAction(
      `[Сохранение] дата позднего выезда: ${lateCheckOut.date}`
    )
  }

  const services = r.additional_services.filter(s =>
    r.services.map(sl => sl.name).includes(s.service)
  )
  const servicesDates = services.map(s => s.date)
  if (services.length) {
    Logs.logHiddenAction(`[Сохранение] даты услуг: ${servicesDates.join(", ")}`)
  }

  const upgrades = r.additional_services.filter(
    s => s.service === upgradeService
  )
  const upgradesDates = upgrades.map(s => s.date)
  if (upgrades.length) {
    Logs.logHiddenAction(
      `[Сохранение] даты улучшений: ${upgradesDates.join(", ")}`
    )
  }

  if (r.additional_services.find(s => !s.payed)) {
    Logs.sendToEMail("Новая бронь")
  }

  const errorFields = getErrorFields(Object.assign({}, r), modes)

  if (errorFields.length) {
    dispatch({ type: SET_ERROR_FIELDS, payload: errorFields })
    return
  } else {
    dispatch({ type: CLEAR_ERROR_FIELDS })
  }

  if (r.has_refund && r.reserved_days.some(d => d.payment_type !== "Карта")) {
    if (
      !window.confirm(
        "Невозвратные брони обычно с оплатой картой. Вы уверены, что выбрали правильный способ оплаты?"
      )
    ) {
      return
    }
  }

  if (r.isDuplicateConfirmed) {
    Logs.logEvent(
      "Создание дубликата подтверждено. Продолжаем без проверки номера."
    )
  } else if (r.booking_number.match(/^[0-9]+$/)) {
    Logs.logEvent("Проверяем номер брони на существование")
    const validationResponse = await validateBookingNumberCall(r.booking_number)
    if (validationResponse.result === "ok") {
      Logs.logEvent("Дубликатов брони не найдено, сохраняем")
    } else {
      Logs.logEvent(
        `Найдены дубликаты: ${validationResponse.reservations.join(", ")}`
      )

      const reservationsResponse = await getMultipleReservationsCall(
        validationResponse.reservations
      )

      const foundReservations = []
      reservationsResponse.forEach(r =>
        foundReservations.push({
          start: r.start,
          room: r.reserved_days[0].room && r.reserved_days[0].room.room_id
        })
      )

      let detailsMessage
      switch (validationResponse.details) {
        case "wubook-today":
          Logs.logEvent("Найден дубликат Wubook с заселением сегодня")
          detailsMessage =
            "Данная бронь уже импортирована из Wubook. Её можно заселить, нажав на неё в списке слева от таблицы на главной странице."
          break

        case "wubook-in-future":
          Logs.logEvent("Найден дубликат Wubook с заселением в будущем")
          detailsMessage =
            "Данная бронь уже импортирована из Wubook. Её можно заселить, нажав на неё в разделе «Новые брони в Wubook»."
          break

        case "existing-reseravaion":
          Logs.logEvent("Найден уже заселенный дубликат")
          detailsMessage = "Бронь с данным номером уже была заселена"
          break

        default:
          Logs.logEvent("Не удалось определить тип дубликата")
          detailsMessage = "Тип брони-дубликата не удалось определить."
      }

      dispatch({
        type: SET_DUPLICATES,
        payload: { duplicates: foundReservations, message: detailsMessage }
      })

      return
    }
  } else {
    Logs.logEvent("Номер брони невалидный, выполняется сохранение новой брони")
  }

  if (!isReservationValid(Object.assign({}, r))) {
    alert(
      "Бронь некорректна. Служебная информация об ошибке записана в консоль."
    )
    return
  }

  dispatch({ type: LOADING_SHOW })

  // Do need to print check on this editing
  const needToPrint = await doNeedToPrint(Object.assign({}, r), modes)

  // Is server running
  const isServerOK = await isServerRunning()

  // If we need to print check but server is off,
  // show error modal and exit this function
  if (!isServerOK && needToPrint) {
    dispatch({ type: LOADING_HID })
    dispatch(showPrintServerError())
    return
  }

  let isPrintingFinished = false
  if (needToPrint) {
    while (!isPrintingFinished) {
      const checkData = getSellCheckData(Object.assign({}, r))
      const res = await CheckPrintControl.printSellCheck(checkData)

      console.log(res)

      // If an error occurred
      // Ask the user: try to print check again or not
      if (res.Error) {
        const { escapeError } = res

        let confirmation

        // In some cases isn't necessary to show confirm modal
        // Because some actions lead to another modal that is shown before this modal
        // We don't need to ask user 2 times to end a check print process
        // Variable escapeError is used for it
        if (escapeError) {
          confirmation = false
        } else {
          confirmation = window.confirm(
            `При печати чека произошла ошибка: \n\n${
              res.Error
            } \n\nПопробовать снова?`
          )
        }

        // Do new iteration if confirm returned true
        // Or exit from while cycle if it returned false (in this case isPrintingFinished will be false)
        if (confirmation) {
          continue
        } else {
          break
        }
      }

      // If there is no error
      isPrintingFinished = true
    }
  }

  // Save only when we don't need to print,
  // or when we need to do it and printing is already finished
  if (!needToPrint || isPrintingFinished) {
    saveNewReservationCall(Object.assign({}, r))
      .then(
        async res => {
          Logs.logEvent("Бронь успешно сохранена")

          dispatch({ type: SET_PK, payload: res.data.pk })
          dispatch({ type: SUCCESS_MODAL_SHOW })

          Logs.send(res.data.pk)
        },
        err => {
          Logs.logEvent(
            "Один или несколько новых дней невозможно добавить, так как они принадлежат другой брони"
          )

          dispatch({
            type: ERROR_MODAL_SHOW,
            payload:
              (err &&
                err.response &&
                err.response.data &&
                err.response.data[0]) ||
              "[Неизвестный номер]"
          })
        }
      )
      .then(() => {
        dispatch({ type: LOADING_HID })
      })
  } else {
    dispatch({ type: LOADING_HID })
  }
}

export const clearReservation = () => dispatch => {
  dispatch({ type: RESERVATION_CLEAR })
  dispatch({ type: SERVICE_CLEAR })
  dispatch({ type: UPGRADE_CLEAR })
  dispatch({ type: CHECK_IN_CLEAR })
  dispatch({ type: CHECK_OUT_CLEAR })
}

export const createNewReservation = (
  room,
  roomType,
  start,
  end
) => dispatch => {
  const payload = {
    room,
    roomType,
    start,
    end
  }

  dispatch({ type: CREATE_NEW_RESERVATION, payload })
  dispatch(updateDayPrices(true))
}

export const doMigration = (date, room) => dispatch => {
  dispatch({
    type: MIGRATION_DONE,
    payload: {
      date,
      room
    }
  })
}

export const setRooms = room => ({
  type: SET_ROOMS,
  payload: room
})

export const refundCheckIn = () => dispatch => {
  dispatch({ type: CHECK_IN_CLEAR })
  dispatch({ type: REFUND_CHECK_IN })
}

export const refundCheckOut = () => dispatch => {
  dispatch({ type: CHECK_OUT_CLEAR })
  dispatch({ type: REFUND_CHECK_OUT })
}

export const writeCard = () => (dispatch, getState) => {
  const r = getState().reservation
  const pk = r.pk
  const cardsAmount = r.hasCard
  const lastRoomID = r.lastRoomID
  const newRoomID = r.newRoomID
  const rooms = r.rooms
  const start = r.start
  const end = r.end
  const checkInTime = r.check_in_time
  const checkOutTime = r.check_out_time
  const reservedDays = r.reserved_days

  if (r.reserved_days.every(d => d.payment_date === ""))
    return new Promise((resolve, reject) => {
      dispatch({
        type: SET_WRITECARD_ERROR,
        payload: "Бронь не оплачена, карта не может быть записана"
      })
      reject("The reservation is not paid")
    })

  let cardAction
  console.log(`Текущее количество карт: ${cardsAmount}`)
  if (!cardsAmount) {
    console.log("Выбран writeCard")
    cardAction = writeCardCall
  } else {
    console.log("Выбран copyCard")
    cardAction = copyCardCall
  }
  console.log("CardAction reservation data: ")
  console.log(r)
  console.log(`ReservationModule CardAction end date: ${end}`)
  return cardAction({
    pk,
    lastRoomID,
    newRoomID,
    rooms,
    start,
    end,
    checkInTime,
    checkOutTime,
    reservedDays
  }).then(res => {
    return new Promise((resolve, reject) => {
      if (res.status === "ok") {
        if (res.relocationWas) {
          dispatch(resetCardsSync())
          updateCardsCall(pk, 0)
        } else {
          dispatch({ type: INCREASE_CARDS })
          updateCardsCall(pk, cardsAmount + 1)
        }
        resolve("Cards were successfuly updated")
      } else {
        dispatch({
          type: SET_WRITECARD_ERROR,
          payload: res.data ? res.data.error : res.error
        })
        reject("Error with writeCardCall")
      }
    })
  })
}

export const clearWritecardError = () => ({
  type: CLEAR_WRITECARD_ERROR
})

export const confirmDuplicate = () => ({
  type: CONFIRM_DUPLICATE
})

export const clearDuplicates = () => ({
  type: CLEAR_DUPLICATES
})

const resetCardsSync = () => ({
  type: RESET_CARDS
})

export const resetCards = () => dispatch => {
  dispatch(resetCardsSync())
  alert("Карты успешно сброшены.")
}

export const updateDayPrices = (needToFetch = false) => async (
  dispatch,
  getState
) => {
  const r = getState().reservation
  const { lastRoomID, guests_number, reserved_days, new_reserved_days } = r
  let { dayPrices } = r

  // At the moment we don't know whether the request was successful or not,
  // so we set "failed" to false
  let failed = false
  if (needToFetch && guests_number) {
    let fetchedPrices
    try {
      // Server responded with new prices array
      fetchedPrices = await getWubookPricesCall(lastRoomID, guests_number)
    } catch (err) {
      console.log(err.message)
      // Server responded with error and we got nothing
      // Update "failed" to true
      failed = true
      fetchedPrices = []
    }

    // Translate server's response into more usable data
    dayPrices = {}
    fetchedPrices.forEach(p => {
      dayPrices[moment(p.date, "DD/MM/YYYY").format("YYYY-MM-DD")] = p.price
    })
  }

  if (!guests_number) {
    // Guests number field is empty,
    // so we set "failed" to true to clear prices of every not payed day
    failed = true
  }

  let newReservedDays = []
  let newNewReservedDays = []

  if (dayPrices && Object.keys(dayPrices).length && !failed) {
    // Update prices to new
    reserved_days.forEach(d => {
      const newPrice = dayPrices[d.date]
      if (d.type === "newDay") {
        if (newPrice) d.price = newPrice
      }
      newReservedDays.push(d)
      if (new_reserved_days.some(nd => nd.id === d.id)) {
        newNewReservedDays.push(d)
      }
    })
  } else if (failed) {
    // Reset prices because of
    // error or empty guests_number field
    reserved_days.forEach(d => {
      if (d.type === "newDay") d.price = ""
      newReservedDays.push(d)
      if (new_reserved_days.some(nd => nd.id === d.id)) {
        newNewReservedDays.push(d)
      }
    })
  } else {
    // Do nothing
    newReservedDays = reserved_days
    newNewReservedDays = new_reserved_days
  }

  // Update dayPrices and days whose type is not equal to "payedDay"
  dispatch({
    type: SET_DAY_PRICES,
    payload: {
      dayPrices,
      reservedDays: newReservedDays,
      newReservedDays: newNewReservedDays
    }
  })
}
