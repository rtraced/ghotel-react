import {
  earlyCheckInService,
  lateCheckOutService,
  upgradeService
} from "../helpers/servicesNames"

if (!Array.prototype.every) {
  // eslint-disable-next-line
  Array.prototype.every = function(callbackfn, thisArg) {
    var T, k

    if (this == null) {
      throw new TypeError("this is null or not defined")
    }

    var O = Object(this)

    var len = O.length >>> 0

    if (typeof callbackfn !== "function") {
      throw new TypeError()
    }

    if (arguments.length > 1) {
      T = thisArg
    }

    k = 0

    while (k < len) {
      var kValue

      if (k in O) {
        kValue = O[k]

        var testResult = callbackfn.call(T, kValue, k, O)

        if (!testResult) {
          return false
        }
      }
      k++
    }
    return true
  }
}

const getErrorFields = (reservation, modes) => {
  const errorFields = []

  if (modes.refund) return errorFields

  const toValidate = ["guest_name", "booking_number", "guest_phone"]
  toValidate.forEach(f => (!reservation[f] ? errorFields.push(f) : true))

  if (reservation.guests_number < 1) errorFields.push("guests_number")

  if (
    !reservation.newRoomID &&
    !modes.create &&
    !reservation.isReady &&
    !reservation.dontSettle
  )
    errorFields.push("rooms")

  reservation.not_payed_reserved_days.forEach(d => {
    if (d.payment_date || !reservation.isReady) {
      if (!d.price) errorFields.push(`day-${d.id}-price`)
      if (!d.payment_type && d.payment_date)
        errorFields.push(`day-${d.id}-payment_type`)
    }
  })

  reservation.new_reserved_days.forEach(d => {
    if (d.payment_date) {
      if (!d.price) errorFields.push(`day-${d.id}-price`)
      if (!d.payment_type && d.payment_date)
        errorFields.push(`day-${d.id}-payment_type`)
    }
    if (modes.create && !d.price) errorFields.push(`day-${d.id}-price`)
  })

  const notCheckTime = type =>
    type !== earlyCheckInService && type !== lateCheckOutService

  let wasErrorWithPM = false
  const filteredServices = {}
  reservation.additional_services.forEach(s => {
    if (s.date && notCheckTime(s.service)) {
      if (!filteredServices[s.date]) filteredServices[s.date] = []
      filteredServices[s.date].push(s)
    }
  })

  for (let date in filteredServices) {
    const servicesGroup = filteredServices[date]

    const paymentMethod = servicesGroup[0].payment_type
    if (!servicesGroup.every(s => s.payment_type === paymentMethod)) {
      // eslint-disable-next-line
      servicesGroup.forEach(s => {
        let serviceErrStrBase

        switch (s.service) {
          case upgradeService:
            serviceErrStrBase = `upgrade-${s.id}`
            break

          default:
            serviceErrStrBase = `service-${s.id}`
            break
        }

        const serviceErrStr = serviceErrStrBase + "-payment_type"
        if (!errorFields.includes(serviceErrStr))
          errorFields.push(serviceErrStr)
        wasErrorWithPM = true
      })
    }
  }
  if (wasErrorWithPM) errorFields.push("payment_methods")

  const checkin = reservation.additional_services.find(
    s => s.service_type === "checkin"
  )
  const checkout = reservation.additional_services.find(
    s => s.service_type === "checkout"
  )
  const services = reservation.additional_services.filter(
    s => s.service_type === "service"
  )
  const upgrades = reservation.additional_services.filter(
    s => s.service_type === "upgrade"
  )

  if (checkin) {
    if (!checkin.payment_type) errorFields.push("checkin-payment_type")
  }

  if (checkout) {
    if (!checkout.payment_type) errorFields.push("checkout-payment_type")
  }

  services.forEach(s => {
    if (!s.date) errorFields.push(`service-${s.id}-date`)
    if (!s.quantity) errorFields.push(`service-${s.id}-quantity`)
    if (!s.payment_type) errorFields.push(`service-${s.id}-payment_type`)
  })

  upgrades.forEach(u => {
    if (!u.date) errorFields.push(`upgrade-${u.id}-date`)
    if (!u.price) errorFields.push(`upgrade-${u.id}-price`)
    if (!u.payment_type) errorFields.push(`upgrade-${u.id}-payment_type`)
  })

  return errorFields
}

export default getErrorFields
