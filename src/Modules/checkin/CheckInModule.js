// ghotel/service/

import moment from "moment"
import Logs from "../Logs"
import { earlyCheckInService } from "../helpers/servicesNames"
import { getEarlyCheckInData } from "../helpers/calculateCheckPrice"
import { CHECK_IN_CHANGED } from "../reservation/ReservationModule"

export const CHECK_IN_SET = "ghotel/service/CHECK_IN_SET"
export const CHECK_IN_TIME_CHANGED = "ghotel/service/CHECK_IN_TIME_CHANGED"
const CHECK_IN_PAYMENT_CHANGED = "ghotel/service/CHECK_IN_PAYMENT_CHANGED"
export const CHECK_IN_CLEAR = "ghotel/service/CHECK_IN_CLEAR"
export const CHECK_IN_DATE_CHANGED = "ghotel/service/CHECK_IN_DATE_CHANGED"

Logs.logHiddenAction(
  `В ранний заезд установлен payment_date = ${moment().format("YYYY-MM-DD")}`
)

const initial_state = {
  service_type: "checkin",
  payment_date: moment().format("YYYY-MM-DD"),
  payment_type: "",
  service: earlyCheckInService,
  quantity: 1,
  price: "",
  date: "",
  admin: "",
  time: "14:00",
  wasBought: false
}

export default function reducer(state = initial_state, action) {
  const { payload } = action

  switch (action.type) {
    case CHECK_IN_SET:
      return {
        ...state,
        ...payload,
        time: payload.time.substr(0, 5),
        wasBought: true
      }

    case CHECK_IN_CLEAR:
      return initial_state

    case CHECK_IN_TIME_CHANGED:
      const data = getEarlyCheckInData(
        payload.payedDays,
        payload.notPayedDays,
        payload.newDays,
        payload.currentTime,
        payload.newTime
      )

      return {
        ...state,
        price: data.price % 1 === 0 ? data.price : data.price.toFixed(2),
        date: data.date,
        time: payload.newTime.substr(0, 5)
      }

    case CHECK_IN_DATE_CHANGED:
      return {
        ...state,
        date: payload
      }

    case CHECK_IN_PAYMENT_CHANGED:
      return { ...state, payment_type: payload }

    default:
      return state
  }
}

export const onCheckInTimeChange = payload => (dispatch, getState) => {
  dispatch({ type: CHECK_IN_TIME_CHANGED, payload })
  const checkin = getState().checkin
  const time = payload.newTime
  const paymentType = checkin.payment_type || "Не выбрано"
  Logs.logEditAction(`${time} | ${paymentType}`, "Ранний заезд")

  Logs.logHiddenAction(
    `[Ранний заезд] date перед экшном CHECK_IN_CHANGED: ${checkin.date}`
  )
  Logs.logHiddenAction(
    `[Ранний заезд] payment_date перед экшном CHECK_IN_CHANGED: ${
      checkin.payment_date
    }`
  )

  dispatch({
    type: CHECK_IN_CHANGED,
    payload: {
      ...checkin,
      time
    }
  })

  const newCheckin = getState().checkin
  Logs.logHiddenAction(
    `[Ранний заезд] date после изменения времени: ${newCheckin.date}`
  )
  Logs.logHiddenAction(
    `[Ранний заезд] payment_date после изменения времени: ${
      newCheckin.payment_date
    }`
  )
}

export const onCheckInPaymentChange = payload => (dispatch, getState) => {
  dispatch({ type: CHECK_IN_PAYMENT_CHANGED, payload })
  const checkin = getState().checkin
  const time = checkin.time.substr(0, 5)
  const paymentType = checkin.payment_type || "Не выбрано"
  Logs.logEditAction(`${time} | ${paymentType}`, "Ранний заезд")

  Logs.logHiddenAction(
    `[Ранний заезд] date перед экшном CHECK_IN_CHANGED: ${checkin.date}`
  )
  Logs.logHiddenAction(
    `[Ранний заезд] payment_date перед экшном CHECK_IN_CHANGED: ${
      checkin.payment_date
    }`
  )

  dispatch({
    type: CHECK_IN_CHANGED,
    payload: checkin
  })

  const newCheckin = getState().checkin
  Logs.logHiddenAction(
    `[Ранний заезд] date после изменения времени: ${newCheckin.date}`
  )
  Logs.logHiddenAction(
    `[Ранний заезд] payment_date после изменения времени: ${
      newCheckin.payment_date
    }`
  )
}

export const onCheckInDateChange = payload => ({
  type: CHECK_IN_DATE_CHANGED,
  payload
})
