// ghotel/service/

import moment from "moment"
import Cookies from "js-cookie"
import Logs from "../Logs"

const SERVICE_DATE_CHANGED = "ghotel/service/SERVICE_DATE_CHANGED"
const SERVICE_QTY_CHANGED = "ghotel/service/SERVICE_QTY_CHANGED"
const SERVICE_PAYMENT_CHANGED = "ghotel/service/SERVICE_PAYMENT_CHANGED"
const SERVICE_SERVICE_CHANGED = "ghotel/service/SERVICE_SERVICE_CHANGED"
const SERVICE_PRICE_CHANGED = "ghotel/service/SERVICE_PRICE_CHANGED"
export const SERVICE_CLEAR = "ghotel/service/SERVICE_CLEAR"

Logs.logHiddenAction(
  `В услуги установлен payment_date = ${moment().format("YYYY-MM-DD")}`
)

const initial_state = {
  service_type: "service",
  payment_date: moment().format("YYYY-MM-DD"),
  payment_type: "",
  service: "",
  quantity: 0,
  price: "",
  date: "",
  admin: Cookies.get("admin")
}

export default function reducer(state = initial_state, action) {
  const { payload } = action

  switch (action.type) {
    case SERVICE_DATE_CHANGED:
      return { ...state, date: payload }

    case SERVICE_QTY_CHANGED:
      return { ...state, quantity: +payload }

    case SERVICE_PAYMENT_CHANGED:
      return { ...state, payment_type: payload }

    case SERVICE_SERVICE_CHANGED:
      return { ...state, service: payload }

    case SERVICE_PRICE_CHANGED:
      return { ...state, price: +payload }

    case SERVICE_CLEAR:
      return initial_state

    default:
      return state
  }
}

export const onServiceDateChange = payload => dispatch => {
  dispatch({ type: SERVICE_DATE_CHANGED, payload })
  Logs.logEditAction(payload, "Дата новой услуги")
}
export const onServiceServiceChange = payload => (dispatch, getState) => {
  const services = getState().reservation.services
  dispatch({ type: SERVICE_SERVICE_CHANGED, payload })
  Logs.logEditAction(payload, "Тип новой услуги")
  const newPrice = services.find(s => s.name === payload).price
  dispatch({
    type: SERVICE_PRICE_CHANGED,
    payload: newPrice
  })
  Logs.logEvent(`Стоимость новой услуги => ${newPrice} ₽`)
}
export const onServiceQtyChange = payload => dispatch => {
  dispatch({ type: SERVICE_QTY_CHANGED, payload })
  Logs.logEditAction(payload, "Количество новой услуги")
}
export const onServicePaymentChange = payload => dispatch => {
  dispatch({ type: SERVICE_PAYMENT_CHANGED, payload })
  Logs.logEditAction(payload, "Метод оплаты новой услуги")
}
