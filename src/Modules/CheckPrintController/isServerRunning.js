// Class
import CheckPrintController from "./index"

const CheckPrintControl = new CheckPrintController()

const isServerRunning = async () => {
  try {
    await CheckPrintControl.getKKTState()
  } catch (err) {
    return false
  }
  return true
}

export default isServerRunning
