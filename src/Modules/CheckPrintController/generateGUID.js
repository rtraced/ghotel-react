const GP = () => (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)

const generateGUID = () =>
  GP() + GP() + "-" + GP() + "-" + GP() + "-" + GP() + "-" + GP() + GP() + GP()

export default generateGUID
