import axios from "axios"
import Cookies from "js-cookie"

import endpoint from "./api"

import shiftCodes from "./shiftCodes"
import checkCodes from "./checkCodes"

import generateGUID from "./generateGUID"
import getRandom5DigitNumber from "./getRandom5DigitNumber"

class CheckPrintController {
  userData = {
    user: "Admin",
    password: ""
  }

  deviceData = {
    NumDevice: 0,
    IdDevice: ""
  }

  defaultCheckData = {
    NotPrint: false,
    NumberCopies: 0,
    TaxVariant: "",
    RRNCode: "",
    AuthorizationCode: ""
  }

  sellCheckData = {
    IsFiscalCheck: true,
    TypeCheck: checkCodes.sell
  }

  refundCheckData = {
    IsFiscalCheck: true,
    TypeCheck: checkCodes.refund,
    RRNCode: "",
    AuthorizationCode: ""
  }

  getAdminName = () => (Cookies.get("admin") ? Cookies.get("admin") : undefined)

  executeCommand = data => {
    const { user, password } = this.userData

    return axios({
      url: endpoint + "Execute",
      method: "post",
      data,
      headers: {
        Authorization: "Basic " + window.btoa(`${user}:${password}`)
      }
    }).then(res => res.data)
  }

  getKKTState = () =>
    this.executeCommand({
      Command: "GetDataKKT",
      ...this.deviceData,
      IdCommand: generateGUID()
    })

  getXReport = () =>
    this.executeCommand({
      Command: "XReport",
      ...this.deviceData,
      IdCommand: generateGUID()
    })

  getDailyCardsTotals = () =>
    this.executeCommand({
      Command: "Settlement",
      ...this.deviceData,
      IdCommand: generateGUID()
    })

  checkStatus = async () => {
    const state = await this.getKKTState()
    const status = state.Status
    const error = state.Error

    return {
      isStatusOK: status === 0,
      error
    }
  }

  openShift = () =>
    this.executeCommand({
      Command: "OpenShift",
      ...this.deviceData,
      CashierName: this.getAdminName(),
      IdCommand: generateGUID()
    })

  closeShift = () =>
    this.executeCommand({
      Command: "CloseShift",
      ...this.deviceData,
      CashierName: this.getAdminName(),
      IdCommand: generateGUID()
    })

  doActionWithCash = async data => {
    let isFinished = false
    let isCanceled = false
    while (!isFinished) {
      const statusInfo = await this.checkStatus()
      const { isStatusOK, error } = statusInfo

      isFinished = true
      if (!isStatusOK) {
        const confirmation = window.confirm(
          `Ошибка: \n${error} \nПопробовать снова?`
        )
        if (confirmation) {
          isFinished = false
        } else {
          isCanceled = true
        }
      }
    }

    if (isCanceled) return

    await this.refreshShift()

    return this.executeCommand(data)
  }

  depositCash = amount =>
    this.doActionWithCash({
      Command: "DepositingCash",
      ...this.deviceData,
      Amount: amount,
      IdCommand: generateGUID()
    })

  paymentCash = amount =>
    this.doActionWithCash({
      Command: "PaymentCash",
      ...this.deviceData,
      Amount: amount,
      IdCommand: generateGUID()
    })

  refreshShift = async () => {
    const state = await this.getKKTState()
    const shiftCode = state.Info.SessionState
    console.log("Получили статус смены")

    if (shiftCode === shiftCodes.closed) {
      console.log("Смена закрыта, открываем")
      return this.openShift()
    }

    if (shiftCode === shiftCodes.expired) {
      console.log("Смена открыта более 24 часов, закрываем и открываем")
      await this.closeShift()
      return this.openShift()
    }

    console.log(
      `Статус смены: ${shiftCode}. Имитируем положительный ответ сервера.`
    )
    return {
      Status: 0
    }
  }

  printCheck = async options => {
    let isFinished = false
    let isCanceled = false
    while (!isFinished) {
      const statusInfo = await this.checkStatus()
      const { isStatusOK, error } = statusInfo

      isFinished = true
      if (!isStatusOK) {
        const confirmation = window.confirm(
          `Ошибка: \n${error} \nПопробовать снова?`
        )
        if (confirmation) {
          isFinished = false
        } else {
          isCanceled = true
        }
      }
    }

    if (isCanceled)
      return {
        Error: "Печать отменена",
        escapeError: true
      }

    await this.refreshShift()

    return this.executeCommand(options)
  }

  printSellCheck = async ({ lines, values }) => {
    if (values.areValuesCorrect) {
      return this.printCheck({
        Command: "RegisterCheck",

        ...this.deviceData,
        ...this.defaultCheckData,
        ...this.sellCheckData,

        CashierName: this.getAdminName(),

        IdCommand: generateGUID(),

        // Строки чека
        CheckStrings: lines,

        Cash: values.cashReceivedFromClient,
        ElectronicPayment: values.cardSumm,

        PayByProcessing: values.payedByCard && !values.isNoRefund,
        ReceiptNumber: getRandom5DigitNumber()
      })
    }
    return {
      Error: "Некорректные значения",
      Status: 1
    }
  }

  printRefundCheck = async ({ lines, values }) =>
    this.printCheck({
      Command: "RegisterCheck",

      ...this.deviceData,
      ...this.defaultCheckData,
      ...this.refundCheckData,

      CashierName: this.getAdminName(),

      IdCommand: generateGUID(),

      Cash: values.refundedSumm,

      // Строки чека
      CheckStrings: lines
    })
}

export default CheckPrintController
