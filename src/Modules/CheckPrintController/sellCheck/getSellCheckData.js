import getLines from "./getLines"
import getValues from "./getValues"

export default reservation => ({
  lines: getLines(reservation),
  values: getValues(reservation)
})
