export default (items, paymentType) => {
  for (let item of items) {
    for (let subItem of item) {
      if (subItem.payment_type === paymentType) return true
    }
  }
  return false
}
