export default (shouldBe, caption = "вносимую сумму наличных") => {
  let clientCashAmount = 0

  // Request to input the client cash until it wouldn't be in correct format
  while (clientCashAmount === 0) {
    const value = window.prompt(
      `Введите ${caption} (до выдачи сдачи). \n\nСумма наличных к оплате: ${shouldBe} руб. \nОставьте поле пустым, чтобы использовать сумму чека без сдачи`
    )

    // Alert was canceled
    if (value === null) break

    // Received an empty string
    // That equals to check summ
    if (value === "") {
      clientCashAmount = shouldBe
      break
    }

    clientCashAmount = parseFloat(value)
    if (isNaN(clientCashAmount)) {
      // Received value is not a number
      window.alert(
        "Неверный формат. \nВведите целое число или число с 2 знаками после точки."
      )
      clientCashAmount = 0
    } else if (clientCashAmount < shouldBe) {
      // Received value is smaller than check cashSumm
      window.alert(
        "Введенная сумма меньше суммы чека. \nОна должна быть либо такой же, либо больше."
      )
      clientCashAmount = 0
    }
  }

  return clientCashAmount
}
