import paymentTypes from "../../helpers/paymentTypes"

const getItems = reservation => {
  const {
    new_reserved_days,
    not_payed_reserved_days,
    additional_services
  } = reservation

  // Get days from
  // - not payed days with filled payment_date field
  // - newly created days with filled payment_date field
  const daysFilterFunc = d =>
    d.payment_date !== "" && d.payment_type !== paymentTypes.bank

  const days = [
    ...not_payed_reserved_days.filter(daysFilterFunc),
    ...new_reserved_days.filter(daysFilterFunc)
  ]

  // Get services from additional services with false payed field
  const servicesFilterFunc = s =>
    !s.payed && s.payment_type !== paymentTypes.bank

  const services = additional_services.filter(servicesFilterFunc)

  return {
    days,
    services
  }
}

export default getItems
