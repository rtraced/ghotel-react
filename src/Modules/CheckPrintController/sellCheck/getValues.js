// Constants
import paymentTypes from "../../helpers/paymentTypes"

// Main functions
import getItems from "./getItems"

// Helpers
import checkPaymentType from "./checkPaymentType"
import getCashAmount from "./getCashAmount"

// checkMode used just to return some values to check should we print the check or not
// if checkMode is true, we don't perform any actions with user (input client cash amount for example)
const getValues = (reservation, checkMode) => {
  const { days, services } = getItems(reservation)

  // Values to return
  let cashSumm = 0,
    cardSumm = 0,
    cashReceivedFromClient = 0,
    payedByCash = false,
    payedByCard = false,
    isNoRefund = false,
    areValuesCorrect = true

  // Process days to count cash and card summs
  days.forEach(d => {
    const price = parseFloat(parseFloat(d.price).toFixed(2))

    if (d.payment_date) {
      switch (d.payment_type) {
        case paymentTypes.cash:
          cashSumm += price
          break
        case paymentTypes.card:
          cardSumm += price
          break
        default:
          break
      }
    }
  })

  // Process services to count cash and card summs
  services.forEach(s => {
    const price = parseFloat(parseFloat(s.price).toFixed(2))
    const quantity = parseInt(s.quantity, 10)

    if (s.payment_date) {
      switch (s.payment_type) {
        case paymentTypes.cash:
          cashSumm += price * quantity
          break
        case paymentTypes.card:
          cardSumm += price * quantity
          break
        default:
          break
      }
    }
  })

  // Determine the payment method used by client
  // Set payedBeCard to true if it's a card payment
  // And input the cash summ if it's a cash payment
  if (checkPaymentType([days, services], paymentTypes.card)) {
    payedByCard = true
  }

  if (checkPaymentType([days, services], paymentTypes.cash)) {
    payedByCash = true
    if (!checkMode)
      cashReceivedFromClient = getCashAmount(
        cashSumm,
        "сумму наличных, полученную от клиента"
      )
  }

  if (cashReceivedFromClient < cashSumm) {
    areValuesCorrect = false
  }

  // Set isNoRefund to true if reservation.has_refund === true
  if (reservation.has_refund) {
    isNoRefund = true
  }

  // Format summs to float numbers with 2-digit fraction
  // (to avoid JS float fraction issues)
  cashSumm = cashSumm.toFixed(2)
  cardSumm = cardSumm.toFixed(2)
  cashReceivedFromClient = cashReceivedFromClient.toFixed(2)

  return {
    cashSumm,
    cardSumm,
    payedByCash,
    payedByCard,
    isNoRefund,
    cashReceivedFromClient,
    areValuesCorrect
  }
}

export default getValues
