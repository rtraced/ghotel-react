// Main functions
import getItems from "./getItems"

const getValues = reservation => {
  const { days, services } = getItems(reservation)

  // Values to return
  let refundedSumm = 0

  // Process days to count refunded summ
  days.forEach(d => {
    const price = parseFloat(parseFloat(d.price).toFixed(2))

    refundedSumm += price
  })

  // Process services to count refunded summ
  services.forEach(s => {
    const price = parseFloat(parseFloat(s.price).toFixed(2))
    const quantity = parseInt(s.quantity, 10)

    refundedSumm += price * quantity
  })

  // Format refunded summ to float number with 2-digit fraction
  // (to avoid JS float fraction issues)
  refundedSumm = refundedSumm.toFixed(2)

  return {
    refundedSumm
  }
}

export default getValues
