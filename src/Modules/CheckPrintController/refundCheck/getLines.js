// Libraries
import moment from "moment"

// Main functions
import getItems from "./getItems"

const getLines = reservation => {
  const { days, services } = getItems(reservation)

  // Array of lines we need to return
  const lines = []

  // Extract lines from days
  days.forEach(d => {
    // Format price to float number with 2-digit fraction
    // (to avoid JS float fraction issues)
    const price = parseFloat(parseFloat(d.price).toFixed(2))

    lines.push({
      Register: {
        Name: `Проживание за ${moment(d.date).format("DD.MM.YYYY")}`,
        Quantity: 1,
        Price: price,
        Amount: price,
        Department: 0,
        Tax: -1,
        MeasurementUnit: "шт"
      }
    })
  })

  // Extract lines from days
  services.forEach(s => {
    // Format price and totalPrice to float number with 2-digit fraction
    // (to avoid JS float fraction issues)
    // And format quantity to integer (just in case)
    const price = parseFloat(parseFloat(s.price).toFixed(2))
    const quantity = parseInt(s.quantity, 10)
    const totalPrice = parseFloat(parseFloat(price * quantity).toFixed(2))

    lines.push({
      Register: {
        Name: `${s.name} за ${moment(s.date).format("DD.MM.YYYY")}`,
        Quantity: quantity,
        Price: price,
        Amount: totalPrice,
        Department: 0,
        Tax: -1,
        MeasurementUnit: "шт"
      }
    })
  })

  return lines
}

export default getLines
