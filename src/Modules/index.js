import { combineReducers } from "redux"
import reservation from "./reservation/ReservationModule"
import service from "./service/ServiceModule"
import upgrade from "./upgrade/UpgradeModule"
import checkin from "./checkin/CheckInModule"
import checkout from "./checkout/CheckOutModule"
import session from "./session/SessionModule"
import settings from "./settings/SettingsModule"

export default combineReducers({
  reservation,
  service,
  upgrade,
  checkin,
  checkout,
  session,
  settings
})
