import Cookies from "js-cookie"
import { loginAdminCall, getAdminsCall } from "../api"

const LOGIN_SUCCESS = "ghotel/session/LOGIN_SUCCESS"
const LOGIN_FAILED = "ghotel/session/LOGIN_FAILED"
const LOGOUT = "ghotel/session/LOGOUT"
const LOADING_STARTED = "ghotel/session/LOADING_STARTED"
const LOADING_ENDED = "ghotel/session/LOADING_ENDED"
const ERROR_SET = "ghotel/session/ERROR_SET"
const ERROR_CLEAR = "ghotel/session/ERROR_CLEAR"

const cAdmin = Cookies.get("admin")
const cID = Cookies.get("adminID")

const initialState = {
  admin: cAdmin || null,
  adminID: cID || null,
  isAuthorized: !!(cAdmin && cID),

  isLoading: false,
  error: ""
}

export default function reducer(state = initialState, action) {
  const { payload } = action

  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        admin: payload.admin,
        adminID: payload.adminID,
        isAuthorized: true
      }

    case LOGIN_FAILED:
      return state

    case LOGOUT:
      return {
        ...state,
        admin: null,
        adminID: null,
        isAuthorized: false
      }

    case LOADING_STARTED:
      return {
        ...state,
        isLoading: true
      }

    case LOADING_ENDED:
      return {
        ...state,
        isLoading: false
      }

    case ERROR_SET:
      return {
        ...state,
        error: payload.error
      }

    case ERROR_CLEAR:
      return {
        ...state,
        error: ""
      }

    default:
      return state
  }
}

export const loginAdmin = (pk, password, admin, key) => dispatch => {
  dispatch({ type: LOADING_STARTED })
  loginAdminCall(pk, password)
    .then(
      res => {
        if (res.result === "ok") {
          Cookies.set("admin", admin)
          Cookies.set("adminID", key)
          dispatch({
            type: LOGIN_SUCCESS,
            payload: {
              admin: admin,
              adminID: key
            }
          })
          dispatch({ type: ERROR_CLEAR })
        } else {
          dispatch({ type: LOGIN_FAILED })
          dispatch({ type: ERROR_SET, payload: { error: "Неверный пароль" } })
        }
      },
      err => dispatch({ type: ERROR_SET, payload: { error: err.error } })
    )
    .then(() => dispatch({ type: LOADING_ENDED }))
}

export const logoutAdmin = () => dispatch => {
  dispatch({ type: LOGOUT })
  Cookies.remove("admin")
  Cookies.remove("adminID")
  Cookies.remove("isBannerClosed")
}

export const setSessionError = error => ({
  type: ERROR_SET,
  payload: { error }
})

export const clearSessionError = () => ({
  type: ERROR_CLEAR
})

export const loginAdminAfterRegistration = (name, password) => dispatch => {
  getAdminsCall().then(admins => {
    const admin = admins.find(a => a.name === name)
    dispatch(loginAdmin(admin.pk, password, name, admin.uid))
  })
}
