// ghotel/service/

import moment from "moment"
import Logs from "../Logs"
import { upgradeService } from "./../helpers/servicesNames"

const UPGRADE_DATE_CHANGED = "ghotel/service/UPGRADE_DATE_CHANGED"
const UPGRADE_PAYMENT_CHANGED = "ghotel/service/UPGRADE_PAYMENT_CHANGED"
const UPGRADE_PRICE_CHANGED = "ghotel/service/UPGRADE_PRICE_CHANGED"
export const UPGRADE_CLEAR = "ghotel/service/UPGRADE_CLEAR"

Logs.logHiddenAction(
  `В улучшения установлен payment_date = ${moment().format("YYYY-MM-DD")}`
)

const initial_state = {
  service_type: "upgrade",
  payment_date: moment().format("YYYY-MM-DD"),
  payment_type: "",
  service: upgradeService,
  quantity: 1,
  price: "",
  date: "",
  admin: ""
}

export default function reducer(state = initial_state, action) {
  const { payload } = action

  switch (action.type) {
    case UPGRADE_DATE_CHANGED:
      return { ...state, date: payload }

    case UPGRADE_PAYMENT_CHANGED:
      return { ...state, payment_type: payload }

    case UPGRADE_CLEAR:
      return {
        ...initial_state,
        price: state.price
      }

    case UPGRADE_PRICE_CHANGED:
      return { ...state, price: payload ? +payload : "" }

    default:
      return state
  }
}

export const onUpgradeDateChange = payload => dispatch => {
  dispatch({ type: UPGRADE_DATE_CHANGED, payload })
  Logs.logEditAction(payload, "Дата нового улучшения")
}

export const onUpgradePaymentChange = payload => dispatch => {
  dispatch({ type: UPGRADE_PAYMENT_CHANGED, payload })
  Logs.logEditAction(payload, "Метод оплаты нового улучшения")
}

export const onUpgradePriceChange = payload => dispatch => {
  dispatch({ type: UPGRADE_PRICE_CHANGED, payload })
  Logs.logEditAction(payload, "Цена нового улучшения")
}
