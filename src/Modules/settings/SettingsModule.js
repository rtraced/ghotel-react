import { getSettingsCall, updateSettingsCall } from "../api"

const SET_SETTINGS = "ghotel/settings/SET_SETTINGS"
const UPDATE_SETTINGS = "ghotel/settings/UPDATE_SETTINGS"
const LOADING_ENDED = "ghotel/settings/LOADING_ENDED"

const initialState = {
  daysInPast: "",
  daysInFuture: "",
  printerIsOn: true,
  isLoading: true
}

export default function reducer(state = initialState, action) {
  const { payload } = action

  switch (action.type) {
    case SET_SETTINGS:
      return {
        ...state,
        daysInFuture: payload.daysInFuture,
        daysInPast: payload.daysInPast,
        printerIsOn: payload.printerIsOn
      }

    case UPDATE_SETTINGS:
      return {
        ...state,
        daysInFuture: payload.newDaysInFuture,
        daysInPast: payload.newDaysInPast
      }

    case LOADING_ENDED:
      return {
        ...state,
        isLoading: false
      }

    default:
      return state
  }
}

export const setSettings = () => async dispatch => {
  const settings = await getSettingsCall()
  dispatch({
    type: SET_SETTINGS,
    payload: {
      daysInFuture: settings[0].days_in_grid,
      daysInPast: settings[0].days_in_past,
      printerIsOn: settings[0].printerIsOn
    }
  })
  dispatch({ type: LOADING_ENDED })
}

export const updateSettings = (
  newDaysInFuture,
  newDaysInPast,
  newToday
) => async dispatch => {
  await updateSettingsCall(newDaysInFuture, newDaysInPast)

  dispatch({
    type: UPDATE_SETTINGS,
    payload: {
      newDaysInFuture,
      newDaysInPast,
      newToday
    }
  })
}
