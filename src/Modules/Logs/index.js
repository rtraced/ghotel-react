import moment from "moment"
import { submitLogsCall } from "../api"

class Logs {
  records = []
  hiddenRecords = []

  clear = () => {
    this.records = []
    this.hiddenRecords = []
  }

  addRecord = action => {
    console.log(action)
    this.records = this.records.concat({
      action,
      timestamp: moment().format("YYYY-MM-DD HH:mm:ss")
    })
  }

  addHiddenRecord = action => {
    this.hiddenRecords = this.hiddenRecords.concat({
      action,
      timestamp: moment().format("YYYY-MM-DD HH:mm:ss")
    })
  }

  send = pk => {
    const recordsCopy = this.records.slice(0)
    this.clear()
    submitLogsCall(pk, recordsCopy).then(res => console.log(res))
  }

  sendToEMail = pk => {
    const logStrings = this.records
      .concat(this.hiddenRecords)
      .sort((a, b) => moment(a.timestamp) - moment(b.timestamp))
      .map(r => r.action)

    // eslint-disable-next-line
    Email.send(
      "risenforces@gmail.com",
      "debug.ghotel@gmail.com",
      `Бронь с новыми услугами (${pk})`,
      logStrings.join("<br><br>"),
      { token: "4490bbce-21d4-4bd4-8596-32647043aed9" }
    )
  }

  logHiddenAction = str => this.addHiddenRecord(str)
  logPressAction = value => this.addRecord(`Нажатие «${value}»`)
  logEditAction = (value, target) =>
    this.addRecord(`Установка ${value} для ${target}`)
  logEvent = event => this.addRecord(`Событие «${event}»`)
  logError = error => this.addRecord(`Ошибка «${error}»`)
}

export default new Logs()
