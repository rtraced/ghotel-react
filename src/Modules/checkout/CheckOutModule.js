// ghotel/service/

import moment from "moment"
import Logs from "../Logs"
import { lateCheckOutService } from "../helpers/servicesNames"
import { getLateCheckOutData } from "../helpers/calculateCheckPrice"
import { CHECK_OUT_CHANGED } from "../reservation/ReservationModule"

export const CHECK_OUT_SET = "ghotel/service/CHECK_OUT_SET"
export const CHECK_OUT_TIME_CHANGED = "ghotel/service/CHECK_OUT_TIME_CHANGED"
const CHECK_OUT_PAYMENT_CHANGED = "ghotel/service/CHECK_OUT_PAYMENT_CHANGED"
export const CHECK_OUT_CLEAR = "ghotel/service/CHECK_OUT_CLEAR"
export const CHECK_OUT_DATE_CHANGED = "ghotel/service/CHECK_OUT_DATE_CHANGED"

Logs.logHiddenAction(
  `В поздний выезд установлен payment_date = ${moment().format("YYYY-MM-DD")}`
)

const initial_state = {
  service_type: "checkout",
  payment_date: moment().format("YYYY-MM-DD"),
  payment_type: "",
  service: lateCheckOutService,
  quantity: 1,
  price: "",
  date: "",
  admin: "",
  time: "12:00",
  wasBought: false
}

export default function reducer(state = initial_state, action) {
  const { payload } = action

  switch (action.type) {
    case CHECK_OUT_SET:
      return {
        ...state,
        ...payload,
        time: payload.time.substr(0, 5),
        wasBought: true
      }

    case CHECK_OUT_CLEAR:
      return initial_state

    case CHECK_OUT_TIME_CHANGED:
      const data = getLateCheckOutData(
        payload.payedDays,
        payload.notPayedDays,
        payload.newDays,
        payload.currentTime,
        payload.newTime
      )

      return {
        ...state,
        price: data.price % 1 === 0 ? data.price : data.price.toFixed(2),
        date: data.date,
        time: payload.newTime.substr(0, 5)
      }

    case CHECK_OUT_DATE_CHANGED:
      return {
        ...state,
        date: payload
      }

    case CHECK_OUT_PAYMENT_CHANGED:
      return { ...state, payment_type: payload }

    default:
      return state
  }
}

export const onCheckOutTimeChange = payload => (dispatch, getState) => {
  dispatch({ type: CHECK_OUT_TIME_CHANGED, payload })
  const checkout = getState().checkout
  const time = payload.newTime
  const paymentType = checkout.payment_type || "Не выбрано"
  Logs.logEditAction(`${time} | ${paymentType}`, "Поздний выезд")

  Logs.logHiddenAction(
    `[Поздний выезд] date перед экшном CHECK_OUT_CHANGED: ${checkout.date}`
  )
  Logs.logHiddenAction(
    `[Поздний выезд] payment_date перед экшном CHECK_OUT_CHANGED: ${
      checkout.payment_date
    }`
  )

  dispatch({
    type: CHECK_OUT_CHANGED,
    payload: {
      ...checkout,
      time
    }
  })

  const newCheckout = getState().checkout
  Logs.logHiddenAction(
    `[Поздний выезд] date после изменения времени: ${newCheckout.date}`
  )
  Logs.logHiddenAction(
    `[Поздний выезд] payment_date после изменения времени: ${
      newCheckout.payment_date
    }`
  )
}

export const onCheckOutPaymentChange = payload => (dispatch, getState) => {
  dispatch({ type: CHECK_OUT_PAYMENT_CHANGED, payload })
  const checkout = getState().checkout
  const time = checkout.time.substr(0, 5)
  const paymentType = checkout.payment_type || "Не выбрано"
  Logs.logEditAction(`${time} | ${paymentType}`, "Поздний выезд")

  Logs.logHiddenAction(
    `[Поздний выезд] date перед экшном CHECK_OUT_CHANGED: ${checkout.date}`
  )
  Logs.logHiddenAction(
    `[Поздний выезд] payment_date перед экшном CHECK_OUT_CHANGED: ${
      checkout.payment_date
    }`
  )

  dispatch({
    type: CHECK_OUT_CHANGED,
    payload: checkout
  })

  const newCheckout = getState().checkout
  Logs.logHiddenAction(
    `[Поздний выезд] date после изменения метода оплаты: ${newCheckout.date}`
  )
  Logs.logHiddenAction(
    `[Поздний выезд] payment_date после изменения метода оплаты: ${
      newCheckout.payment_date
    }`
  )
}

export const onCheckOutDateChange = payload => ({
  type: CHECK_OUT_DATE_CHANGED,
  payload
})
