import moment from "moment"

export default (payload, offset) => moment(payload).add(1 + offset, "days")
