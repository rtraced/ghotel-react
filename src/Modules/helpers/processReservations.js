import isReservationArchived from "./isReservationArchived"

// Check check-out late or not
const isCheckOutLate = checkOutTime => {
  return +checkOutTime.substr(0, 2) > 12
}

export default data => {
  const processedReservations = {}
  const processedRooms = {}

  /*
  * Transfer information about the reservation in general
  * and separately for each room in different objects
  * and calculate some additional parameters
  */
  data.forEach(reservation => {
    processedReservations[reservation.pk] = {
      bookingNumber: reservation.booking_number,
      start: reservation.start,
      end: reservation.end,
      clientName: reservation.guest_name,
      clientPhone: reservation.guest_phone,
      clientNote: reservation.note,
      isPayed: reservation.payed,
      isArchived: isReservationArchived(
        reservation.end,
        reservation.check_out_time
      ),
      isCheckOutLate: isCheckOutLate(reservation.check_out_time),
      hasRefund: reservation.has_refund
    }

    const reservedDays = reservation.reserved_days
    let roomID, date, paymentType
    reservedDays.forEach((day, index) => {
      roomID = day.room.room_id
      date = day.date
      if (!processedRooms[roomID]) processedRooms[roomID] = {}

      let migratedFrom = null,
        migratedTo = null
      const nextDay = reservedDays[index + 1],
        prevDay = reservedDays[index - 1]
      if (nextDay) {
        const nextDayRoom = nextDay.room.room_id
        if (nextDayRoom !== roomID) migratedTo = nextDayRoom
      }
      if (prevDay) {
        const prevDayRoom = prevDay.room.room_id
        if (prevDayRoom !== roomID) migratedFrom = prevDayRoom
      }

      paymentType = day.payment_type
      processedRooms[roomID][date] = {
        reservation: reservation.pk,
        migratedTo,
        migratedFrom,
        paymentType
      }
    })
  })

  return {
    reservations: processedReservations,
    rooms: processedRooms
  }
}
