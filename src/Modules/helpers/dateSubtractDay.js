import moment from "moment"

export default payload => moment(payload).subtract(1, "days")
