import moment from "moment"

export default (endDate, checkOutTime) => {
  const archivationDate = moment(endDate)
  const hours = +checkOutTime.substr(0, 2)
  archivationDate.set({ hours: hours + 10 })
  return moment().isAfter(archivationDate)
}
