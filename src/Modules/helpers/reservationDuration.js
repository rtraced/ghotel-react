import moment from "moment"

export default (start, end) => moment(end).diff(moment(start), "days")
