export const getCheckInPrice = allServices => {
  if (allServices.some(s => s.service_type === "checkin")) {
    return allServices.find(s => s.service_type === "checkin").price
  }
  return null
}

export const getCheckOutPrice = allServices => {
  if (allServices.some(s => s.service_type === "checkout")) {
    return allServices.find(s => s.service_type === "checkout").price
  }
  return null
}
