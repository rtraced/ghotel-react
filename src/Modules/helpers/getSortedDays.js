const getSortedDays = days => {
  const sortedDays = days.sort(
    (day1, day2) => new Date(day1.date) - new Date(day2.date)
  )
  return sortedDays
}

export default getSortedDays
