import moment from "moment"
import getSortedDays from "./getSortedDays"

const getFirstDayData = days => {
  const _days = getSortedDays(days)
  return {
    price: _days[0].price,
    date: _days[0].date
  }
}

const getLastDayData = days => {
  const _days = getSortedDays(days)
  return {
    price: _days[_days.length - 1].price,
    date: _days[_days.length - 1].date
  }
}

export const getEarlyCheckInData = (
  payedDays, // reservation.reserved_days
  notPayedDays, // reservation.existing_reserved_days_to_sell
  newDays, // reservation.reserved_days_to_sell
  currentTime, // reservation.check_in_time
  newTime = null, // time from checkin field
  newPrice = null
) => {
  let checkInTime = moment(currentTime.slice(0, -3), "HH:mm")
  if (newTime) {
    checkInTime = moment(newTime, "HH:mm")
  }
  let price = 0
  const days = [].concat(payedDays, notPayedDays, newDays)
  const dayData = getFirstDayData(days)
  if (days.length > 0) {
    price = dayData.price || 0
  }
  let totalPrice = ""
  if (
    checkInTime.isBefore(moment("14:00", "HH:mm")) &&
    checkInTime.isSameOrAfter(moment("8:00", "HH:mm"))
  ) {
    totalPrice = parseFloat(price * 0.5)
  } else if (checkInTime.isBefore(moment("8:00", "HH:mm"))) {
    totalPrice = parseFloat(price)
  }

  return {
    price: totalPrice,
    date: dayData.date
  }
}

export const getLateCheckOutData = (
  payedDays, // reservation.reserved_days
  notPayedDays, // reservation.existing_reserved_days_to_sell
  newDays, // reservation.reserved_days_to_sell
  currentTime, // reservation.check_out_time
  newTime = null, // time from checkout field
  newPrice = null
) => {
  let checkOutTime = moment(currentTime.slice(0, -3), "HH:mm")
  if (newTime) {
    checkOutTime = moment(newTime, "HH:mm")
  }
  let price = 0
  const days = [].concat(payedDays, notPayedDays, newDays)
  const dayData = getLastDayData(days)
  if (days.length > 0) {
    price = dayData.price || 0
  }
  let totalPrice = ""
  if (
    checkOutTime.isAfter(moment("12:00", "HH:mm")) &&
    checkOutTime.isSameOrBefore(moment("18:00", "HH:mm"))
  ) {
    totalPrice = parseFloat(price * 0.5)
  } else if (checkOutTime.isAfter(moment("18:00", "HH:mm"))) {
    totalPrice = parseFloat(price)
  }

  return {
    price: totalPrice,
    date: dayData.date
  }
}
