import { connect } from "react-redux"
import Login from "../Components/Login/Login"

import { loginAdmin, setSessionError } from "../Modules/session/SessionModule"

const mapStateToProps = state => ({
  isAuthorized: state.session.isAuthorized,

  isLoading: state.session.isLoading,
  error: state.session.error
})

const mapDispatchToProps = {
  loginAdmin,
  setSessionError
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)
