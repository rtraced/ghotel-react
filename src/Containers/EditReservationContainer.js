import { connect } from "react-redux"
import EditReservation from "../Components/EditReservation/EditReservation"
import {
  onEndChange,
  onStartChange,
  onDayPriceChange,
  onDayMethodChange,
  onReservationPayedChange,
  onDayPayedChange,
  onServiceCreate,
  onCreatedServiceQtyChange,
  onCreatedServicePriceChange,
  onCreatedServicePaymentChange,
  onServiceCopyClick,
  onServiceDeleteClick,
  onCreatedServiceDateChange,
  onReservationDayAdd,
  onReservationDayRemove,
  onDaySummCopy,
  onDayMethodCopy,
  onNameChange,
  onPhoneChange,
  onMailChange,
  onBookingNumberChange,
  onNoteChange,
  onClientNoteChange,
  onWubookNoteChange,
  onRefundChange,
  onUpgradeCreate,
  onUpgradeDelete,
  setUpgradeError,
  clearUpgradeError,
  getServices,
  getReservation,
  onSaleClick,
  onRefundClick,
  toggleEntryModal,
  onNotPayedDayPriceChange,
  onNotPayedDayMethodChange,
  onNotPayedDayPayedChange,
  saveReservation,
  clearReservation,
  onDayRefund,
  doMigration,
  refundCheckIn,
  refundCheckOut,
  hideSuccessModal,
  writeCard,
  clearWritecardError,
  onServiceRefundClick,
  hideErrorModal,
  onDontSettleChange,
  closeModalWithAllErrors,
  onGuestsNumberChange,
  setRooms,
  resetCards,
  hidePrintServerError
} from "../Modules/reservation/ReservationModule"
import {
  onServiceDateChange,
  onServiceServiceChange,
  onServiceQtyChange,
  onServicePaymentChange
} from "../Modules/service/ServiceModule"
import {
  onUpgradeDateChange,
  onUpgradePaymentChange,
  onUpgradePriceChange
} from "../Modules/upgrade/UpgradeModule"
import {
  onCheckInTimeChange,
  onCheckInPaymentChange
} from "../Modules/checkin/CheckInModule"
import {
  onCheckOutTimeChange,
  onCheckOutPaymentChange
} from "../Modules/checkout/CheckOutModule"
import diff from "lodash.difference"

const mapStateToProps = (state, ownProps) => ({
  // Whole reservation data
  reservation: state.reservation,

  // First and last dates of reservation
  start: state.reservation.start,
  end: state.reservation.end,
  initialEnd: state.reservation.initial_end,
  reservedDays: state.reservation.reserved_days,
  newReservedDays: state.reservation.new_reserved_days,
  payedReservedDays: state.reservation.payed_reserved_days,
  notPayedReservedDays: state.reservation.not_payed_reserved_days,
  days: state.reservation.days,
  services: state.reservation.services,
  service: state.service,
  servicesError: state.reservation.services_error,
  upgradesError: state.reservation.upgrades_error,
  createdServices: state.reservation.additional_services.filter(
    s => s.service_type === "service"
  ),
  createdUpgrades: state.reservation.additional_services.filter(
    s => s.service_type === "upgrade"
  ),
  allServices: state.reservation.additional_services,
  name: state.reservation.guest_name,
  guestsNumber: state.reservation.guests_number,
  roomDescription: state.reservation.room_description,
  note: state.reservation.note,
  clientNote: state.reservation.client_note,
  wubookNote: state.reservation.wubook_note,
  phone: state.reservation.guest_phone,
  mail: state.reservation.guest_mail,
  bookingNumber: state.reservation.booking_number,
  hasRefund: state.reservation.has_refund,
  payed: state.reservation.payed,
  newUpgrade: state.upgrade,
  globalCheckIn: state.reservation.check_in_time,
  localCheckIn: state.checkin.time,
  globalCheckOut: state.reservation.check_out_time,
  localCheckOut: state.checkout.time,
  checkInPrice: state.checkin.price,
  checkOutPrice: state.checkout.price,
  checkInPayment: state.checkin.payment_type,
  checkOutPayment: state.checkout.payment_type,
  wasCheckInBought: state.checkin.wasBought,
  wasCheckOutBought: state.checkout.wasBought,
  isShowingEntryModal: state.reservation.isShowingEntryModal,
  isSale: state.reservation.isSale,
  isRefund: state.reservation.isRefund,
  isReady: state.reservation.isReady,
  refundSumm: state.reservation.refundSumm,
  rooms: state.reservation.rooms,
  errorFields: state.reservation.errorFields,
  showModalWithAllErrors: state.reservation.showModalWithAllErrors,
  refundedItems: state.reservation.refundedItems,
  showSuccessModal: state.reservation.showSuccessModal,
  showErrorModal: state.reservation.showErrorModal,
  isLoading: state.reservation.isLoading,
  lastRoomID: state.reservation.lastRoomID,
  newRoomID: state.reservation.newRoomID,
  hasCard: state.reservation.hasCard,
  writecardError: state.reservation.writecardError,
  availableRooms: state.reservation.availableRooms,
  availableDaysToUpgrade: diff(
    state.reservation.days,
    state.reservation.additional_services
      .filter(s => s.service_type === "upgrade")
      .map(s => s.date)
  ),
  dontSettle: state.reservation.dontSettle,
  isFetching: state.reservation.isFetching,
  errorPK: state.reservation.errorPK,
  isPrintServerErrorModalActive:
    state.reservation.isPrintServerErrorModalActive,
  initialData: state.reservation.initialData
})

const mapDispatchToProps = {
  onEndChange,
  onStartChange,
  onDayPriceChange,
  onDayMethodChange,
  onReservationPayedChange,
  onDayPayedChange,
  onServiceDateChange,
  onServiceServiceChange,
  onServiceQtyChange,
  onServicePaymentChange,
  onServiceCreate,
  onCreatedServiceQtyChange,
  onCreatedServicePriceChange,
  onCreatedServicePaymentChange,
  onServiceCopyClick,
  onServiceDeleteClick,
  onCreatedServiceDateChange,
  onReservationDayAdd,
  onReservationDayRemove,
  onDaySummCopy,
  onDayMethodCopy,
  onNameChange,
  onPhoneChange,
  onMailChange,
  onBookingNumberChange,
  onNoteChange,
  onClientNoteChange,
  onWubookNoteChange,
  onRefundChange,
  onUpgradeDateChange,
  onUpgradePaymentChange,
  onUpgradePriceChange,
  onUpgradeCreate,
  onUpgradeDelete,
  setUpgradeError,
  clearUpgradeError,
  onCheckInTimeChange,
  onCheckOutTimeChange,
  onCheckInPaymentChange,
  onCheckOutPaymentChange,
  getServices,
  getReservation,
  onSaleClick,
  onRefundClick,
  toggleEntryModal,
  onNotPayedDayPriceChange,
  onNotPayedDayMethodChange,
  onNotPayedDayPayedChange,
  saveReservation,
  clearReservation,
  onDayRefund,
  doMigration,
  refundCheckIn,
  refundCheckOut,
  hideSuccessModal,
  writeCard,
  clearWritecardError,
  onServiceRefundClick,
  hideErrorModal,
  onDontSettleChange,
  closeModalWithAllErrors,
  onGuestsNumberChange,
  setRooms,
  resetCards,
  hidePrintServerError
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditReservation)
