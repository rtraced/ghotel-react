import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import Navbar from "../Components/Navbar/Navbar"
import { logoutAdmin } from "../Modules/session/SessionModule"

const mapStateToProps = state => ({
  admin: state.session.admin,
  isAuthorized: state.session.isAuthorized,
  printerIsOn: state.settings.printerIsOn
})

const mapDispatchToProps = {
  logoutAdmin
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Navbar)
)
