import { connect } from "react-redux"
import Settings from "../Components/Settings/Settings"

import { updateSettings } from "../Modules/settings/SettingsModule"

const mapStateToProps = state => ({
  daysInPast: state.settings.daysInPast,
  daysInFuture: state.settings.daysInFuture,
  today: state.settings.today,
  isLoading: state.settings.isLoading
})

const mapDispatchToProps = {
  updateSettings
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Settings)
