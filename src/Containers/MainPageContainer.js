import { connect } from "react-redux"
import MainPage from "../Components/MainPage/MainPage"

const mapStateToProps = state => ({
  daysInPast: state.settings.daysInPast,
  daysInFuture: state.settings.daysInFuture,
  today: state.settings.today,
  isSettingsLoading: state.settings.isLoading,
  admin: state.session.admin,
  adminID: state.session.adminID
})

export default connect(mapStateToProps)(MainPage)
