import { connect } from "react-redux"
import Registration from "../Components/Registration/Registration"

import { loginAdminAfterRegistration } from "../Modules/session/SessionModule"

const mapStateToProps = state => ({
  isAuthorized: state.session.isAuthorized
})

const mapDispatchToProps = {
  loginAdminAfterRegistration
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Registration)
