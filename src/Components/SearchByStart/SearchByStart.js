import React, { Component } from "react"
import { FormGroup, ControlLabel } from "react-bootstrap"
import { SingleDatePicker } from "react-dates"
import SearchResults from "../Common/SearchResults"
import { searchByStartCall } from "../../Modules/api"

const SearchBlock = props => (
  <div
    style={{
      backgroundColor: "#eee",
      width: 800,
      margin: "50px auto",
      padding: 30,
      borderRadius: 5
    }}
  >
    {props.children}
  </div>
)

class SearchByStart extends Component {
  state = {
    date: null,
    searchResult: null,
    datePickerFocused: false
  }

  setDate = date => {
    this.setState({ date })
    this.updateResult(date)
  }

  setResult = searchResult => this.setState({ searchResult })
  clearResult = () => this.setState({ searchResult: null })

  updateResult = async date => {
    if (!date) return this.clearResult()

    const formattedDate = date.format("YYYY-MM-DD")

    this.setResult(await searchByStartCall(formattedDate))
  }

  changeFocus = datePickerFocused => this.setState({ datePickerFocused })

  render() {
    return (
      <SearchBlock>
        <FormGroup style={{ marginBottom: 15 }}>
          <ControlLabel style={{ display: "block", marginBottom: 10 }}>
            Дата заезда
          </ControlLabel>
          <SingleDatePicker
            id="search-by-date-dp"
            date={this.state.date}
            onDateChange={this.setDate}
            focused={this.state.datePickerFocused}
            onFocusChange={({ focused }) => this.changeFocus(focused)}
            isOutsideRange={() => {}}
            placeholder="Дата"
          />
        </FormGroup>

        <SearchResults
          reservations={this.state.searchResult}
          hintText="Выберите дату для поиска"
        />
      </SearchBlock>
    )
  }
}

export default SearchByStart
