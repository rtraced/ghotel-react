import React, { Component } from "react"
import Logs from "../../Modules/Logs"
import { Row, Col, FormGroup, ControlLabel, Button } from "react-bootstrap"
import Select from "../Common/Select"
import moment from "moment"
import { getAvailableRoomsCall } from "../../Modules/api"

class Rooms extends Component {
  state = {
    isMigrationOpened: false,
    selectedDate: "",
    selectedRoom: "",
    availableRooms: []
  }

  toggle = () => {
    this.setState({ isMigrationOpened: !this.state.isMigrationOpened })
    Logs.logPressAction(
      `${
        this.state.isMigrationOpened ? "Закрыть" : "Открыть"
      } диалог переселения`
    )
  }

  getAvailableRooms = async date => {
    this.setState({ selectedRoom: "", availableRooms: [] })
    const availableRooms = await getAvailableRoomsCall(
      date,
      this.props.lastDate
    )
    this.setState({ availableRooms })
  }

  setDate = date => {
    this.setState({ selectedDate: date })
    Logs.logPressAction("Выбрана дата для переселения")
    this.getAvailableRooms(date)
  }

  setRoom = room => {
    this.setState({
      selectedRoom: this.state.availableRooms.find(r => r.room_id === room)
    })
    Logs.logPressAction("Выбрана комната для переселения")
  }

  submitMigrationForm = () => {
    const { selectedDate, selectedRoom } = this.state
    const { doMigration } = this.props

    if (!selectedDate || !selectedRoom) {
      if (!selectedDate) Logs.logError("Не выбрана дата для переселения")
      if (!selectedRoom) Logs.logError("Не выбрана комната для переселения")
      return
    }

    doMigration(this.state.selectedDate, this.state.selectedRoom)
    Logs.logEvent(
      `Выполнено переселение в комнату ${this.state.selectedRoom.room_id}`
    )
    this.toggle()
  }

  chooseWubookRoom = value => {
    const { availableRooms, start, doMigration } = this.props
    const room = availableRooms.find(r => r.room_id === value)
    doMigration(start, room)
    Logs.logEvent(`[Wubook] Выбрана комната ${room.room_id}`)
  }

  render() {
    const { reservedDays, rooms, days, modes } = this.props

    const migrationBlockStyle = {
      marginLeft: 8,
      marginTop: 8,
      marginBottom: 15
    }

    const toggleStyle = {
      color: "#337ab7",
      cursor: "pointer",
      display: "inline-block",
      lineHeight: "20px",
      borderBottom: "1px dashed",
      userSelect: "none"
    }

    const migrationFormStyle = {
      marginTop: 12
    }

    const today = moment(moment().format("YYYY-MM-DD"))
    const availableDays = days.filter(d => moment(d.value).isSameOrAfter(today))

    let lastDayRoomID = ""
    if (reservedDays.length) {
      const lastDay = reservedDays[reservedDays.length - 1]
      if (lastDay.room) {
        lastDayRoomID = lastDay.room.room_id
      }
    }

    return (
      <FormGroup
        controlId="formBasicText"
        validationState={this.props.validationState}
      >
        {this.props.isReady ? (
          <ControlLabel style={{ letterSpacing: 0.5 }}>
            {rooms.length > 1 ? "Комнаты" : "Комната"}
          </ControlLabel>
        ) : null}

        <table>
          <tbody>
            {rooms.map(r => (
              <tr style={{ marginBottom: 10 }} key={r.id}>
                <th
                  style={{
                    fontSize: 20,
                    fontWeight: 300,
                    paddingLeft: 8
                  }}
                >
                  {r.id}
                </th>
                <td style={{ paddingLeft: 12, paddingTop: 5, color: "#777" }}>
                  {r.date ? `с ${r.date}` : null}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {modes.sale &&
          (this.props.isReady ? (
            <div style={migrationBlockStyle}>
              <span style={toggleStyle} onClick={this.toggle}>
                Переселение
              </span>
              {this.state.isMigrationOpened && (
                <Row style={migrationFormStyle}>
                  <Col xs={12}>
                    <Select
                      name="Дата переселения"
                      value={this.state.selectedDate}
                      values={availableDays}
                      onChange={value => this.setDate(value)}
                      bsSize="small"
                      style={{ marginBottom: 10 }}
                    />
                  </Col>
                  <Col xs={12}>
                    <Select
                      name="Комната"
                      value={
                        this.state.selectedRoom
                          ? this.state.selectedRoom.room_id
                          : ""
                      }
                      values={this.state.availableRooms.map(r => ({
                        text: `${r.room_id} ${r.name}`,
                        value: r.room_id
                      }))}
                      onChange={value => this.setRoom(value)}
                      disabled={!this.state.availableRooms.length}
                      bsSize="small"
                      style={{ marginBottom: 15 }}
                    />
                  </Col>
                  <Col xs={12}>
                    <Button
                      bsSize="small"
                      bsStyle="success"
                      onClick={this.submitMigrationForm}
                    >
                      Переселить
                    </Button>
                  </Col>
                </Row>
              )}
            </div>
          ) : (
            <Select
              name="Комната"
              value={lastDayRoomID}
              values={this.props.availableRooms.map(r => ({
                text: `${r.room_id} ${r.name}`,
                value: r.room_id
              }))}
              onChange={value =>
                this.props.setRooms(
                  this.props.availableRooms.find(r => r.room_id === value)
                )
              }
              bsSize="small"
              style={{ marginBottom: 15 }}
            />
          ))}
      </FormGroup>
    )
  }
}

export default Rooms
