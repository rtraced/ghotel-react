import React, { Component, Fragment } from "react"
import Logs from "../../Modules/Logs"
import debounce from "lodash.debounce"
import {
  Grid,
  Row,
  Col,
  Jumbotron,
  Button,
  ButtonGroup,
  FormGroup,
  FormControl,
  ControlLabel
} from "react-bootstrap"
import {
  ModalTitle,
  ModalHR,
  ModalControls,
  ModalButton,
  ModalButtonWithLoading,
  ModalSuccessMessage,
  ModalError
} from "../Common/ModalParts"
import PrintServerErrorModal from "../Common/PrintServerErrorModal"
import TextField from "../Common/Textfield"
import Textarea from "../Common/TextArea"
import Checkbox from "../Common/Checkbox"
import DatePicker from "../Common/RangeDatePicker"
import Day from "../Common/Day"
import PayedDay from "../Common/PayedDay"
import Time from "../Common/CheckInCheckOutTime"
import RoomUpgrade from "../Common/RoomUpgrade"
import Service from "../Common/Service"
import TotalSumm from "./TotalSumm"
import Rooms from "./Rooms"
import Modal from "react-modal"
import history from "../../Modules/helpers/history"
import qs from "qs"
import LoadingModal from "../Common/LoadingModal"
import { deleteReservationCall, clearCardCall } from "../../Modules/api"
import { lateCheckOutService } from "../../Modules/helpers/servicesNames"
import isReservationArchived from "../../Modules/helpers/isReservationArchived"
import styled from "styled-components"
import BottomBarButtons, {
  BarButtonGroup,
  BarButton
} from "../Common/BottomBarButtons"
import moment from "moment"
import { modalDefaultStyles } from "../../App"

const getConfirmationText = guestName => `
Проверьте, чтобы в брони было корректно указано ФИО.
Оно должно быть указано в формате «Фамилия, Имя, Отчество (при наличии)».
Текущее ФИО в брони: ${guestName}

Если корректно - нажмите «OK», если нет - «Отмена» и отредактируйте поле «ФИО гостя»
`

const handlePrintBlankClick = reservation => {
  const confirmation = window.confirm(
    getConfirmationText(reservation.guest_name)
  )

  if (confirmation)
    window.location.href = `/print_blank.html?data=${JSON.stringify(
      reservation
    )}`
}

const handlePrintRefundBlankClick = reservation => {
  const confirmation = window.confirm(
    getConfirmationText(reservation.guest_name)
  )

  if (confirmation)
    window.location.href = `/print_refund_blank.html?data=${JSON.stringify(
      reservation
    )}`
}

const StyledJumbotron = props => (
  <Jumbotron style={{ padding: 30 }}>{props.children}</Jumbotron>
)

const CheckJumbotron = props => (
  <Jumbotron style={{ padding: 20 }}>{props.children}</Jumbotron>
)

const StyledTitle = props => (
  <h3 style={{ marginTop: 5, marginBottom: 25 }}>{props.children}</h3>
)

const CheckTitle = props => (
  <h4
    style={{
      fontWeight: 700,
      marginTop: 0,
      marginBottom: 25
    }}
  >
    {props.children}
  </h4>
)

const CheckBlock = props => (
  <Col xs={3} style={{ position: "sticky", top: 110 }}>
    {props.modes.sale ? (
      <SaleCheck
        notPayedReservedDays={props.notPayedReservedDays}
        newReservedDays={props.newReservedDays}
        allServices={props.allServices}
      />
    ) : (
      <RefundCheck refundedItems={props.refundedItems} />
    )}
  </Col>
)

const CheckRow = props => {
  const liStyles = {
    fontSize: 13,
    marginBottom: 10
  }

  return <li style={liStyles}>{props.children}</li>
}

const RefundCheck = props => {
  let id = 0

  const ulStyles = {
    listStyle: "none",
    paddingLeft: 5
  }

  return (
    <CheckJumbotron>
      <CheckTitle>Чек</CheckTitle>
      <ul style={ulStyles}>
        {props.refundedItems
          .filter(i => i.type === "day")
          .map(i => (
            <CheckRow key={id++}>
              <span style={{ color: "#c53834" }}>
                День {i.date} - {i.price} ₽
              </span>
            </CheckRow>
          ))}
        {props.refundedItems
          .filter(i => i.type === "service")
          .map(i => (
            <CheckRow key={id++}>
              <span style={{ color: "#c53834" }}>
                {i.name} ({i.quantity} шт) - {i.price * i.quantity} ₽
              </span>
            </CheckRow>
          ))}
      </ul>
    </CheckJumbotron>
  )
}

const SaleCheck = props => {
  let id = 0

  const ulStyles = {
    listStyle: "none",
    paddingLeft: 5
  }

  const isDayCorrect = d => d.price && d.payment_type && d.payment_date
  const isServiceCorrect = s =>
    s.price && s.payment_type && s.payment_date && s.quantity

  return (
    <CheckJumbotron>
      <CheckTitle>Чек</CheckTitle>
      <ul style={ulStyles}>
        {props.notPayedReservedDays.map(d =>
          isDayCorrect(d) ? (
            <CheckRow key={id++}>
              <span style={{ color: "#007d00" }}>
                Проживание за {d.date} - {d.price} ₽
              </span>
            </CheckRow>
          ) : null
        )}
        {props.newReservedDays.map(d =>
          isDayCorrect(d) ? (
            <CheckRow key={id++}>
              <span style={{ color: "#007d00" }}>
                Проживание за {d.date} - {d.price} ₽
              </span>
            </CheckRow>
          ) : null
        )}
        {props.allServices
          .filter(s => !s.payed)
          .map(s =>
            isServiceCorrect(s) ? (
              <CheckRow key={id++}>
                <span style={{ color: "#007d00" }}>
                  {s.service} ({s.quantity} шт) - {s.price * s.quantity} ₽
                </span>
              </CheckRow>
            ) : null
          )}
      </ul>
    </CheckJumbotron>
  )
}

const ModalText = props => (
  <p style={{ margin: 0, padding: "15px 20px" }}>{props.children}</p>
)

const SuccessModal = props => {
  return (
    <Modal
      isOpen={props.isOpen}
      defaultStyles={{
        ...modalDefaultStyles,
        content: {
          ...modalDefaultStyles.content,
          width: 800,
          maxWidth: "auto"
        }
      }}
    >
      <ModalTitle>Бронь обновлена!</ModalTitle>
      <ModalHR />
      <ModalText>Успешно</ModalText>
      <ModalHR />
      <ModalControls>
        <ButtonGroup style={{ marginLeft: 10 }}>
          <Button bsStyle="primary" disabled>
            Печать формы
          </Button>
          <Button href="/print_ruform.html">RU</Button>
          <Button href="/print_engform.html">EN</Button>
        </ButtonGroup>
        <ModalButton
          bsStyle="primary"
          onClick={() => handlePrintBlankClick(props.reservation)}
        >
          Печать БО
        </ModalButton>
        <ModalButton bsStyle="danger" onClick={props.resetCards}>
          Сбросить карты
        </ModalButton>
        <ModalButton bsStyle="primary" onClick={props.showWritecardModal}>
          Записать карту
        </ModalButton>
        <ModalButton
          bsStyle="danger"
          style={{ marginLeft: "auto" }}
          onClick={() => {
            props.hideModal()
            history.push("/")
          }}
        >
          Закрыть
        </ModalButton>
      </ModalControls>
    </Modal>
  )
}

const ErrorParagraph = styled.span`
  display: block;
  color: #d43f3a;
  margin-bottom: 10px;

  &:last-child {
    margin-bottom: 0;
  }
`

const AllErrorsModal = props => {
  const { errorFields } = props

  const getErrorString = code => {
    switch (code) {
      case "guest_name":
        return "Имя гостя не заполнено"
      case "booking_number":
        return "Номер брони не заполнен"
      case "guest_phone":
        return "Телефон гостя не заполнен"
      case "guests_number":
        return "Количество гостей не заполнено"
      case "rooms":
        return "Комната для заселения не выбрана"
      case "checkout-payment_type":
        return "Не выбран метод оплаты позднего выезда"
      case "checkin-payment_type":
        return "Не выбран метод оплаты раннего въезда"
      case "payment_methods":
        return "В один и тот же день не могут использоваться разные методы оплаты"
      default:
        if (/day-.*-price/.test(code))
          return "У одного или нескольких дней не выбрана стоимость"
        if (/day-.*-payment_type/.test(code))
          return "У одного или нескольких дней не выбран метод оплаты (или выбраны разные методы оплаты в один день)"
        if (/service-.*-date/.test(code))
          return "У одной или нескольких услуг не выбрана дата"
        if (/service-.*-quantity/.test(code))
          return "У одной или нескольких услуг не выбрано количество"
        if (/service-.*-payment_type/.test(code))
          return "У одной или нескольких услуг не выбран метод оплаты (или выбраны разные методы оплаты в один день)"
        if (/upgrade-.*-date/.test(code))
          return "У одного или нескольких улучшений не выбрана дата"
        if (/upgrade-.*-payment_type/.test(code))
          return "У одного или нескольких улучшений не выбран метод оплаты"
        return "Неизвестная ошибка"
    }
  }

  const strings = []
  errorFields.forEach(errCode => {
    const str = getErrorString(errCode)
    if (!strings.includes(str)) strings.push(str)
  })

  return (
    <Modal isOpen={props.isOpen}>
      <ModalTitle>Ошибка!</ModalTitle>
      <ModalHR />
      <ModalText>
        {strings.map((s, idx) => (
          <ErrorParagraph key={idx}>{s}</ErrorParagraph>
        ))}
      </ModalText>
      <ModalHR />
      <ModalControls>
        <ModalButton
          bsStyle="danger"
          style={{ marginLeft: "auto" }}
          onClick={props.hideModal}
        >
          Закрыть
        </ModalButton>
      </ModalControls>
    </Modal>
  )
}

const ErrorOnSaveModal = props => {
  return (
    <Modal isOpen={props.isOpen}>
      <ModalTitle>Ошибка!</ModalTitle>
      <ModalHR />
      <ModalText>
        Один или несколько новых дней невозможно добавить, так как они
        принадлежат брони {props.errorPK}
      </ModalText>
      <ModalHR />
      <ModalControls>
        <ModalButton
          bsStyle="danger"
          style={{ marginLeft: "auto" }}
          onClick={props.hideModal}
        >
          Закрыть
        </ModalButton>
      </ModalControls>
    </Modal>
  )
}

const ErrorOnDeleteModal = props => {
  return (
    <Modal isOpen={props.isOpen}>
      <ModalTitle>Ошибка!</ModalTitle>
      <ModalHR />
      <ModalText>Ошибка удаления брони.</ModalText>
      <ModalHR />
      <ModalControls>
        <ModalButton
          bsStyle="danger"
          style={{ marginLeft: "auto" }}
          onClick={props.hideModal}
        >
          Закрыть
        </ModalButton>
      </ModalControls>
    </Modal>
  )
}

const AfterMigrationModal = props => {
  return (
    <Modal isOpen={props.isOpen}>
      <ModalTitle>Внимание!</ModalTitle>
      <ModalHR />
      <ModalText>
        Было выполнено переселение, выписанные карты данной брони сброшены.
        Хотите перезаписать их?
      </ModalText>
      <ModalHR />
      <ModalControls>
        <ModalButton
          bsStyle="danger"
          onClick={() => {
            props.hideModal()
            Logs.logPressAction("Отказаться от перезаписи карт")
          }}
        >
          Отмена
        </ModalButton>
        <ModalButton
          bsStyle="success"
          onClick={() => {
            props.hideModal()
            Logs.logPressAction("Перезаписать карты")
            props.action()
          }}
        >
          Перезаписать
        </ModalButton>
      </ModalControls>
    </Modal>
  )
}

class WriteCardModal extends Component {
  state = {
    fetching: false,
    successMessage: null,
    isWrited: false
  }

  render() {
    return (
      <Modal isOpen={this.props.isOpen} style={{ content: { width: 540 } }}>
        <ModalTitle>Запись карты</ModalTitle>
        <ModalHR />
        <ModalText>Приложите карту</ModalText>
        {this.state.successMessage ? (
          <ModalSuccessMessage>{this.state.successMessage}</ModalSuccessMessage>
        ) : null}
        {this.props.error ? <ModalError>{this.props.error}</ModalError> : null}
        <ModalHR />
        <ModalControls>
          <ModalButton
            bsStyle="danger"
            onClick={() => {
              this.props.hideModal()
              this.setState({
                fetching: false,
                successMessage: null,
                isWrited: false
              })
              this.props.hideError()
              Logs.logPressAction("Закрыть диалог записи карты")
            }}
          >
            {this.state.isWrited ? "Завершить" : "Отмена"}
          </ModalButton>
          <ModalButtonWithLoading
            loading={this.state.fetching}
            bsStyle="success"
            onClick={() => {
              this.props.hideError()
              this.setState({ fetching: true, successMessage: null })
              Logs.logPressAction("Записать карту")
              this.props
                .action()
                .then(
                  rs => {
                    console.log("checkin end")
                    console.log(rs)
                    this.setState({
                      successMessage: "Карта успешно записана!",
                      isWrited: true
                    })
                    Logs.logEvent("Карта успешно записана")
                  },
                  err => {
                    console.log(err)
                    Logs.logError(
                      `Неудачная запись карты: ${this.props.error}`
                    )
                  }
                )
                .then(() => {
                  this.setState({ fetching: false })
                })
            }}
          >
            {this.state.isWrited ? "Записать еще" : "Записать"}
          </ModalButtonWithLoading>
        </ModalControls>
      </Modal>
    )
  }
}

class ClearCardModal extends Component {
  state = {
    fetching: false,
    successMessage: null,
    errorMessage: null,
    isCleaned: false
  }

  clearState = () =>
    this.setState({
      fetching: false,
      successMessage: null,
      errorMessage: null,
      isCleaned: false
    })

  render() {
    return (
      <Modal isOpen={this.props.isOpen} style={{ content: { width: 540 } }}>
        <ModalTitle>Очистка</ModalTitle>
        <ModalHR />
        <ModalText>Приложите карту</ModalText>
        {this.state.successMessage && (
          <ModalSuccessMessage>{this.state.successMessage}</ModalSuccessMessage>
        )}
        {this.state.errorMessage && (
          <ModalError>{this.state.errorMessage}</ModalError>
        )}
        <ModalHR />
        <ModalControls>
          <ModalButton
            bsStyle="danger"
            onClick={() => {
              this.props.hideModal()
              this.clearState()
              Logs.logPressAction("Закрыть диалог записи карты")
            }}
          >
            {this.state.isCleaned ? "Завершить" : "Отмена"}
          </ModalButton>
          {this.props.isOpenedOnDelete && (
            <ModalButton
              bsStyle={!this.props.hasCard ? "success" : "danger"}
              onClick={() => {
                Logs.logPressAction("Продолжить удаление брони")
                this.props.deleteReservation()
              }}
            >
              Продолжить удаление брони
            </ModalButton>
          )}
          <ModalButtonWithLoading
            loading={this.state.fetching}
            bsStyle="success"
            onClick={() => {
              this.setState({
                fetching: true,
                successMessage: null,
                errorMessage: null
              })
              Logs.logPressAction("Очистить карту")
              if (!this.props.hasCard) {
                this.setState({
                  fetching: false,
                  errorMessage: "В брони нет записанных карт"
                })
                Logs.logError("В брони нет записанных карт")
                return
              }
              clearCardCall()
                .then(res => {
                  if (res.status === "ok") {
                    this.setState({
                      successMessage: "Карта успешно очищена!",
                      isCleaned: true
                    })
                    Logs.logEvent("Карта успешно очищена")
                  } else {
                    this.setState({
                      errorMessage: res.error
                    })
                    Logs.logError(`Неудачная очистка карты: ${res.error}`)
                  }
                })
                .then(() => {
                  this.setState({ fetching: false })
                })
            }}
          >
            {this.state.isWrited ? "Очистить еще" : "Очистить"}
          </ModalButtonWithLoading>
        </ModalControls>
      </Modal>
    )
  }
}

const Services = props => (
  <StyledJumbotron>
    <StyledTitle>Дополнительные услуги</StyledTitle>
    <Service
      services={props.services}
      createdServices={props.createdServices}
      service={props.service}
      days={props.days}
      servicesError={props.servicesError}
      onDateChange={props.onServiceDateChange}
      onServiceChange={props.onServiceServiceChange}
      onQtyChange={props.onServiceQtyChange}
      onPaymentChange={props.onServicePaymentChange}
      onCreate={() => props.onServiceCreate(props.service)}
      onServiceQtyChange={props.onCreatedServiceQtyChange}
      onServicePaymentChange={props.onCreatedServicePaymentChange}
      onServiceDateChange={props.onCreatedServiceDateChange}
      onServiceCopy={props.onServiceCopyClick}
      onServiceDelete={props.onServiceDeleteClick}
      onServiceRefund={props.onServiceRefundClick}
      isRefund={props.isRefund}
      modes={props.modes}
      errorFields={props.errorFields}
    />
  </StyledJumbotron>
)

const Upgrades = props => (
  <StyledJumbotron>
    <StyledTitle>Улучшение номера</StyledTitle>
    <RoomUpgrade
      days={props.days}
      availableDays={props.availableDaysToUpgrade}
      createdUpgrades={props.createdUpgrades}
      upgradesError={props.upgradesError}
      newUpgrade={props.newUpgrade}
      onDateChange={props.onUpgradeDateChange}
      onPaymentChange={props.onUpgradePaymentChange}
      onPriceChange={props.onUpgradePriceChange}
      onCreate={() => props.onUpgradeCreate(props.newUpgrade)}
      onUpgradeDateChange={props.onCreatedServiceDateChange}
      onUpgradePriceChange={props.onCreatedServicePriceChange}
      onUpgradePaymentChange={props.onCreatedServicePaymentChange}
      onUpgradeCopy={props.onServiceCopyClick}
      onUpgradeDelete={props.onServiceDeleteClick}
      onUpgradeRefund={props.onServiceRefundClick}
      setUpgradeError={props.setUpgradeError}
      clearUpgradeError={props.clearUpgradeError}
      modes={props.modes}
      errorFields={props.errorFields}
    />
  </StyledJumbotron>
)

class EditReservation extends Component {
  constructor(props) {
    super(props)

    this.state = {
      outsideRangeEnd: null,
      mode: "",
      initialTotal: null,
      activeModals: [],
      isErrorOnDeleteShowed: false,
      isClearCardOpened: false,
      isClearCardOpenedOnDelete: false,
      guestsNumberWasEmpty: false
    }
  }

  async componentDidMount() {
    const q = qs.parse(this.props.location.search.slice(1))

    await this.props.getServices()
    await this.props.getReservation(q.pk)
    this.checkGuestsNumber()
    this.setInitialTotal()
    this.checkArchiveStatus()
    this.checkReadyStatus()
  }

  checkGuestsNumber = () => {
    if (!this.props.guestsNumber) {
      this.setState({
        guestsNumberWasEmpty: true
      })
    }
  }

  componentWillUnmount() {
    this.props.clearReservation()
  }

  openClearCard = () => this.setState({ isClearCardOpened: true })
  openClearCardOnDelete = () =>
    this.setState({ isClearCardOpened: true, isClearCardOpenedOnDelete: true })
  closeClearCard = () =>
    this.setState({
      isClearCardOpened: false,
      isClearCardOpenedOnDelete: false
    })

  checkReadyStatus = () => {
    const { isReady } = this.props
    if (!isReady) this.setMode("sale")
  }

  checkArchiveStatus = () => {
    const { end, globalCheckOut } = this.props
    if (isReservationArchived(end, globalCheckOut)) this.setMode("watch")
  }

  showModal = name =>
    this.setState(prev => ({
      activeModals: prev.activeModals.concat(name)
    }))

  hideModal = name =>
    this.setState(prev => ({
      activeModals: prev.activeModals.filter(m => m !== name)
    }))

  onDatesChange = (startDate, endDate) => {
    const fSD = startDate.format("YYYY-MM-DD")
    const fED = endDate.format("YYYY-MM-DD")
    if (this.props.start !== fSD) {
      this.props.onStartChange(fSD)
    }

    if (this.props.end !== fED) {
      this.props.onEndChange(fED)
    }
  }

  calcTotal = () => {
    let totalToPay = 0.0
    let totalPayed = 0.0
    this.props.reservedDays
      .filter(d => d.type !== "payedDay")
      .forEach(d => {
        totalToPay +=
          isNaN(parseFloat(d.price)) ||
          d.payment_date === "" ||
          d.payment_type === ""
            ? 0
            : parseFloat(d.price)
      })
    this.props.allServices
      .filter(s => s.payed !== true)
      .forEach(s => {
        totalToPay +=
          isNaN(parseFloat(s.price * s.quantity)) || s.payment_type === ""
            ? 0
            : parseFloat(s.price * s.quantity)
      })
    this.props.payedReservedDays.forEach(d => {
      totalPayed += isNaN(parseFloat(d.price)) ? 0 : parseFloat(d.price)
    })
    this.props.allServices
      .filter(s => s.payed === true)
      .forEach(s => {
        totalPayed += isNaN(parseFloat(s.price * s.quantity))
          ? 0
          : parseFloat(s.price * s.quantity)
      })

    return {
      toPay: totalToPay,
      payed: totalPayed
    }
  }

  getLastID = () => {
    const { reservedDays } = this.props

    const sortedByDate = reservedDays.sort((a, b) =>
      moment(a.date).isAfter(b.date) ? 1 : -1
    )
    const lastDayID = sortedByDate[sortedByDate.length - 1].id

    return lastDayID
  }

  getPayedDays = modes => {
    const { payedReservedDays } = this.props

    if (payedReservedDays.filter(d => d.payment_date !== "").length === 0)
      return null

    return (
      <StyledJumbotron>
        <StyledTitle>Оплаченные дни</StyledTitle>
        {payedReservedDays.map(day => (
          <PayedDay
            key={day.id}
            day={day}
            price={day.price}
            method={day.payment_type}
            modes={modes}
            dayRefund={this.props.onDayRefund}
            isLast={day.id === this.getLastID()}
          />
        ))}
      </StyledJumbotron>
    )
  }

  getNotPayedDays = modes => {
    const {
      notPayedReservedDays,
      onNotPayedDayPriceChange,
      onNotPayedDayMethodChange,
      onNotPayedDayPayedChange
    } = this.props

    if (notPayedReservedDays.length === 0) return null

    return (
      <StyledJumbotron>
        <StyledTitle>Неоплаченные дни</StyledTitle>
        {notPayedReservedDays.map(day => (
          <Day
            key={day.id}
            day={day}
            price={day.price}
            method={day.payment_type}
            onDayPriceChange={value => onNotPayedDayPriceChange(day.id, value)}
            onDayPaymentChange={value =>
              onNotPayedDayMethodChange(day.id, value)
            }
            onDayPayedChange={value => onNotPayedDayPayedChange(day.id, value)}
            payed={day.payment_date === "" ? false : true}
            modes={modes}
            dayRefund={this.props.onDayRefund}
            isLast={day.id === this.getLastID()}
            errorFields={this.props.errorFields}
            disabled={!modes.sale}
          />
        ))}
      </StyledJumbotron>
    )
  }

  existingDays = modes => {
    return (
      <Fragment>
        {this.getPayedDays(modes)}
        {this.getNotPayedDays(modes)}
      </Fragment>
    )
  }

  newDays = modes => {
    return (
      <StyledJumbotron>
        <StyledTitle>Новые дни</StyledTitle>

        {this.props.newReservedDays.map(day => (
          <Day
            key={day.id}
            day={day}
            price={day.price}
            method={day.payment_type}
            onDayPriceChange={value =>
              this.props.onDayPriceChange(day.id, value)
            }
            onDayPaymentChange={value =>
              this.props.onDayMethodChange(day.id, value)
            }
            onDayPayedChange={value =>
              this.props.onDayPayedChange(day.id, value)
            }
            payed={!!day.payment_date}
            modes={modes}
            errorFields={this.props.errorFields}
          />
        ))}
        <div style={{ marginTop: 10 }}>
          <Button bsStyle="primary" onClick={() => this.props.onDaySummCopy()}>
            Копировать стоимость с первого дня
          </Button>
          <Button
            style={{ marginLeft: 5 }}
            bsStyle="primary"
            onClick={() => this.props.onDayMethodCopy()}
          >
            Копировать метод с первого дня
          </Button>
        </div>
        <div style={{ marginTop: 10 }}>
          <Button
            onClick={() => {
              if (
                this.props.allServices.some(
                  s => s.service === lateCheckOutService && s.payed
                )
              ) {
                Logs.logError(
                  "В данной брони есть уже оплаченный поздний выезд, который не может быть изменен после добавления нового дня. Чтобы добавить день, необходимо сделать возврат услуги позднего выезда в режиме возврата."
                )
                if (
                  window.confirm(
                    "[Ошибка] В данной брони есть уже оплаченный поздний выезд, который не может быть изменен после добавления нового дня. Чтобы добавить день, необходимо сделать возврат услуги позднего выезда в режиме возврата. Сменить режим?"
                  )
                ) {
                  // Reload page
                  history.go(0)
                }
                return
              }
              this.props.onReservationDayAdd()
            }}
            bsStyle="success"
          >
            Добавить сутки
          </Button>
          {this.props.reservedDays.length > 1 &&
          this.props.newReservedDays.length > 0 ? (
              <Button
                onClick={() => this.props.onReservationDayRemove()}
                style={{ marginLeft: 5 }}
                bsStyle="danger"
              >
              Убрать сутки
              </Button>
            ) : null}
        </div>
      </StyledJumbotron>
    )
  }

  setMode = mode => this.setState({ mode })

  onSaleClick = () => {
    this.setMode("sale")
    Logs.logPressAction("Выбрать режим продажи")
  }
  onRefundClick = () => {
    this.setMode("refund")
    Logs.logPressAction("Выбрать режим возврата")
  }

  selectMode = () => (
    <div style={{ width: "100%", height: "100%", display: "flex" }}>
      <div style={{ margin: "auto", width: "100%", maxWidth: 300 }}>
        <Button
          bsStyle="success"
          style={{
            display: "block",
            width: "100%",
            marginBottom: 16,
            fontSize: 20
          }}
          onClick={this.onSaleClick}
        >
          Продажа
        </Button>
        <Button
          bsStyle="danger"
          style={{ display: "block", width: "100%", fontSize: 20 }}
          onClick={this.onRefundClick}
        >
          Возврат
        </Button>
      </div>
    </div>
  )

  setInitialTotal = () => {
    this.setState({ initialTotal: this.calcTotal() })
  }

  onNameChange = debounce(this.props.onNameChange, 300)
  onPhoneChange = debounce(this.props.onPhoneChange, 300)
  onMailChange = debounce(this.props.onMailChange, 300)
  onNoteChange = debounce(this.props.onNoteChange, 300)
  onClientNoteChange = debounce(this.props.onClientNoteChange, 300)
  onWubookNoteChange = debounce(this.props.onWubookNoteChange, 300)

  showErrorOnDelete = () => this.setState({ isErrorOnDeleteShowed: true })
  hideErrorOnDelete = () => this.setState({ isErrorOnDeleteShowed: false })

  deleteReservation = (isConfirmed = false) => {
    const {
      reservation: { pk },
      clearReservation,
      hasCard
    } = this.props
    if (!isConfirmed) Logs.logPressAction("Удалить бронь")

    const doAction = () =>
      deleteReservationCall(pk)
        .then(
          () => {
            Logs.logEvent(`Бронь ${pk} успешно удалена`)
            clearReservation()
            history.push("/")
          },
          () => {
            Logs.logError(`Бронь ${pk} не была удалена`)
            this.showErrorOnDelete()
          }
        )
        .then(() => {
          Logs.send(pk)
        })

    if (isConfirmed) {
      doAction()
      return
    }

    if (
      window.confirm(
        "Вы точно хотите удалить бронь? Её нельзя будет восстановить."
      )
    ) {
      if (!hasCard) {
        doAction()
      } else {
        this.openClearCardOnDelete()
      }
    }
  }

  render() {
    if (this.props.isFetching) {
      return <LoadingModal isOpen={true} />
    }

    if (!this.state.mode) return this.selectMode()

    const modes = {
      create: false,
      sale: this.state.mode === "sale",
      refund: this.state.mode === "refund",
      watch: this.state.mode === "watch"
    }

    return (
      <Grid fluid={true} style={{ marginTop: 20, paddingBottom: 105 }}>
        <Row>
          <Col xs={9}>
            <StyledJumbotron>
              <Row>
                <Col xs={3}>
                  <Rooms
                    reservedDays={this.props.reservedDays}
                    rooms={this.props.rooms}
                    days={this.props.days.map(d => ({
                      text: moment(d).format("DD MMMM"),
                      value: d
                    }))}
                    setRooms={this.props.setRooms}
                    doMigration={(date, room) => {
                      this.props.doMigration(date, room)
                      if (this.props.hasCard > 0)
                        this.showModal("aftermigration")
                    }}
                    lastDate={this.props.end}
                    availableRooms={this.props.availableRooms}
                    newRoomID={this.props.newRoomID}
                    isReady={this.props.isReady}
                    start={this.props.start}
                    modes={modes}
                    validationState={
                      this.props.errorFields.includes("rooms") ? "error" : null
                    }
                  />
                  {this.props.roomDescription && (
                    <FormGroup>
                      <ControlLabel>Описание комнаты</ControlLabel>
                      <p
                        style={{
                          fontSize: 20,
                          fontWeight: 300
                        }}
                      >
                        {this.props.roomDescription}
                      </p>
                    </FormGroup>
                  )}
                  <FormGroup style={{ marginBottom: 10 }}>
                    <ControlLabel>Количество гостей</ControlLabel>
                    <FormControl
                      value={this.props.guestsNumber}
                      onChange={e =>
                        this.props.onGuestsNumberChange(e.target.value)
                      }
                      type={"number"}
                      bsSize="small"
                      disabled={!this.state.guestsNumberWasEmpty}
                    />
                  </FormGroup>
                  <TextField
                    defaultValue={this.props.name}
                    onChange={this.onNameChange}
                    name="ФИО гостя"
                    placeholder="Иванов"
                    validationState={
                      this.props.errorFields.includes("guest_name")
                        ? "error"
                        : null
                    }
                    style={{ marginBottom: 10 }}
                    disabled={!(modes.sale || modes.refund)}
                  />
                  <TextField
                    value={this.props.bookingNumber}
                    onChange={this.props.onBookingNumberChange}
                    name="Номер брони"
                    placeholder="123456789"
                    validationState={
                      this.props.errorFields.includes("booking_number")
                        ? "error"
                        : null
                    }
                    style={{ marginBottom: 10 }}
                    disabled={!modes.sale}
                  />
                  <TextField
                    defaultValue={this.props.phone}
                    onChange={this.onPhoneChange}
                    name="Телефон гостя"
                    placeholder="+71234567890"
                    validationState={
                      this.props.errorFields.includes("guest_phone")
                        ? "error"
                        : null
                    }
                    style={{ marginBottom: 10 }}
                    disabled={!modes.sale}
                  />
                  <TextField
                    defaultValue={this.props.mail}
                    onChange={this.onMailChange}
                    name="Почта гостя"
                    placeholder="ivanov@gmail.com"
                    style={{ marginBottom: 10 }}
                    disabled={!modes.sale}
                  />
                  <FormGroup>
                    <ControlLabel>Карты</ControlLabel>
                    <p
                      style={{
                        fontSize: 20,
                        fontWeight: 300
                      }}
                    >
                      {this.props.hasCard}
                    </p>
                  </FormGroup>
                </Col>
                <Col xs={4}>
                  <Textarea
                    defaultValue={this.props.note}
                    onChange={this.onNoteChange}
                    name="Примечание"
                    placeholder="Полезное примечание"
                    style={{ marginBottom: 10 }}
                    disabled={modes.refund || modes.watch}
                  />
                  <Textarea
                    defaultValue={this.props.clientNote}
                    onChange={this.onClientNoteChange}
                    name="Примечание клиента"
                    placeholder="Примечание клиента"
                    style={{ marginBottom: 10 }}
                    disabled={true}
                  />
                  <Textarea
                    defaultValue={this.props.wubookNote}
                    onChange={this.onWubookNoteChange}
                    name="Примечание Wubook"
                    placeholder="Примечание из Wubook"
                    style={{ marginBottom: 10 }}
                    formStyle={{ minHeight: 200 }}
                    disabled={true}
                  />
                </Col>
                <Col xs={5}>
                  <Checkbox
                    name="Оплата"
                    title="Бронь оплачена полностью"
                    value={this.props.payed}
                    onChange={value =>
                      this.props.onReservationPayedChange(value)
                    }
                    formStyle={{ marginBottom: 15 }}
                    disabled={!modes.sale}
                  />
                  {!this.props.isReady && (
                    <Checkbox
                      name="Возвратность"
                      title="Бронь невозвратная"
                      value={this.props.hasRefund}
                      onChange={() =>
                        this.props.onRefundChange(!this.props.hasRefund)
                      }
                      formStyle={{ marginBottom: 15 }}
                    />
                  )}
                  <DatePicker
                    name={"Даты проживания"}
                    startDate={this.props.start}
                    endDate={this.props.end}
                    disabled={!modes.sale ? true : "startDate"}
                    isOutsideRange={this.props.initialEnd}
                    onDatesChange={(start, end) =>
                      this.onDatesChange(start, end)
                    }
                  />
                </Col>
              </Row>
            </StyledJumbotron>

            {this.existingDays(modes)}
            {modes.sale ? this.newDays(modes) : null}

            <StyledJumbotron>
              <StyledTitle>Время заезда и выезда</StyledTitle>
              <Time
                type="checkin"
                onChange={v =>
                  this.props.onCheckInTimeChange({
                    payedDays: this.props.payedReservedDays,
                    notPayedDays: this.props.notPayedReservedDays,
                    newDays: this.props.newReservedDays,
                    currentTime: this.props.globalCheckIn,
                    newTime: v
                  })
                }
                time={this.props.localCheckIn}
                price={this.props.checkInPrice}
                name={"Время заезда"}
                payment={this.props.checkInPayment}
                disabled={this.props.wasCheckInBought || modes.watch}
                onPaymentChange={v => this.props.onCheckInPaymentChange(v)}
                modes={modes}
                onRefund={this.props.refundCheckIn}
                errorFields={this.props.errorFields}
              />
              <Time
                type="checkout"
                onChange={v =>
                  this.props.onCheckOutTimeChange({
                    payedDays: this.props.payedReservedDays,
                    notPayedDays: this.props.notPayedReservedDays,
                    newDays: this.props.reservedDays,
                    currentTime: this.props.globalCheckOut,
                    newTime: v
                  })
                }
                time={this.props.localCheckOut}
                price={this.props.checkOutPrice}
                name={"Время выезда"}
                payment={this.props.checkOutPayment}
                disabled={this.props.wasCheckOutBought || modes.watch}
                onPaymentChange={v => this.props.onCheckOutPaymentChange(v)}
                modes={modes}
                onRefund={this.props.refundCheckOut}
                errorFields={this.props.errorFields}
              />
            </StyledJumbotron>

            {modes.sale || this.props.createdServices.length ? (
              <Services {...this.props} modes={modes} />
            ) : null}
            {modes.sale || this.props.createdUpgrades.length ? (
              <Upgrades {...this.props} modes={modes} />
            ) : null}
          </Col>
          <CheckBlock {...this.props} modes={modes} />
        </Row>
        <div
          style={{
            position: "fixed",
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
            transform: "translateZ(0)",
            width: "100%",
            backgroundColor: "#fff",
            boxShadow: "0 -2px 5px 0 rgba(0, 0, 0, .27)",
            bottom: 0,
            left: 0,
            marginBottom: 0,
            padding: 25
          }}
        >
          <TotalSumm
            value={this.calcTotal()}
            initialValue={this.state.initialTotal}
            refundSumm={this.props.refundSumm}
            modes={modes}
          />
          <BottomBarButtons>
            <BarButtonGroup>
              <Button bsStyle="primary" disabled>
                Печать формы
              </Button>
              <Button href="/print_ruform.html">RU</Button>
              <Button href="/print_engform.html">EN</Button>
            </BarButtonGroup>
            <BarButton
              bsStyle="primary"
              onClick={() => handlePrintBlankClick(this.props.reservation)}
            >
              Печать БО
            </BarButton>
            {modes.refund && (
              <BarButton
                bsStyle="primary"
                onClick={() =>
                  handlePrintRefundBlankClick(this.props.reservation)
                }
              >
                Печать бланка возврата
              </BarButton>
            )}
            {modes.refund && (
              <BarButton
                bsStyle="danger"
                onClick={() => this.deleteReservation()}
                disabled={modes.watch}
              >
                Удалить бронь
              </BarButton>
            )}
            {modes.sale &&
              this.props.initialData.isReady &&
              (this.props.initialData.hasRefund ||
                this.props.initialData.reservedDays.some(
                  d => d.payment_date !== ""
                )) && (
                <BarButton
                  bsStyle="primary"
                  onClick={() => this.showModal("writecard")}
                >
                  Записать карту
                </BarButton>
              )}
            {modes.refund && (
              <BarButton
                bsStyle="warning"
                onClick={() => {
                  Logs.logPressAction("Открыть диалог очистки карт")
                  this.openClearCard()
                }}
                disabled={modes.watch || modes.sale}
              >
                Очистить карту
              </BarButton>
            )}
            <BarButton
              bsStyle="success"
              onClick={() =>
                this.props.saveReservation({
                  modes,
                  showWritecardModal: () => this.showModal("writecard")
                })
              }
              disabled={modes.watch}
            >
              Сохранить
            </BarButton>
          </BottomBarButtons>
        </div>
        <SuccessModal
          isOpen={this.props.showSuccessModal}
          pk={this.props.reservation.pk}
          reservation={this.props.reservation}
          hideModal={this.props.hideSuccessModal}
          isWubook={!this.props.reservation.isReady}
          showWritecardModal={() => {
            Logs.logPressAction("Открыть диалог записи карт")
            this.showModal("writecard")
          }}
          resetCards={() => {
            Logs.logPressAction("Сбросить карты")
            this.props.resetCards()
          }}
        />
        <AllErrorsModal
          isOpen={this.props.showModalWithAllErrors}
          hideModal={this.props.closeModalWithAllErrors}
          errorFields={this.props.errorFields}
        />
        <ErrorOnSaveModal
          isOpen={this.props.showErrorModal}
          errorPK={this.props.errorPK}
          hideModal={this.props.hideErrorModal}
        />
        <ErrorOnDeleteModal
          isOpen={this.state.isErrorOnDeleteShowed}
          hideModal={this.hideErrorOnDelete}
        />
        <PrintServerErrorModal
          isOpen={this.props.isPrintServerErrorModalActive}
          hideModal={this.props.hidePrintServerError}
          tryAgainAction={() => this.props.saveReservation(modes)}
        />
        <AfterMigrationModal
          isOpen={this.state.activeModals.includes("aftermigration")}
          hideModal={() => this.hideModal("aftermigration")}
          action={() => this.showModal("writecard")}
        />
        <WriteCardModal
          isOpen={this.state.activeModals.includes("writecard")}
          hideModal={() => this.hideModal("writecard")}
          action={this.props.writeCard}
          error={this.props.writecardError}
          hideError={this.props.clearWritecardError}
        />
        <ClearCardModal
          isOpen={this.state.isClearCardOpened}
          isOpenedOnDelete={this.state.isClearCardOpenedOnDelete}
          hideModal={this.closeClearCard}
          deleteReservation={() => this.deleteReservation(true)}
        />
        <LoadingModal isOpen={this.props.isLoading} />
      </Grid>
    )
  }
}

export default EditReservation
