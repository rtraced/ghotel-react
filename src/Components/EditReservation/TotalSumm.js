import React, { Fragment } from "react"

const format = num => parseFloat(num).toFixed(2)

const TotalSumm = props => {
  const secondRowStyles = !props.modes.watch
    ? { fontSize: 15, color: "#555" }
    : { fontSize: 20, color: "#222" }
  const secondRowPriceStyles = !props.modes.watch
    ? { fontSize: 17, fontWeight: 500, color: "#f1637e" }
    : { fontSize: 22, fontWeight: 500, color: "crimson" }

  return (
    <div style={{ marginRight: "auto" }}>
      <span style={{ fontSize: 20 }}>
        {!props.modes.watch &&
          (props.modes.refund ? (
            <Fragment>
              Возврат будет выполнен на сумму{" "}
              <span style={{ fontSize: 22, fontWeight: 500, color: "crimson" }}>
                {format(props.refundSumm)}
                {" ₽"}
              </span>
            </Fragment>
          ) : (
            <Fragment>
              Продажа будет выполнена на сумму{" "}
              <span style={{ fontSize: 22, fontWeight: 500, color: "crimson" }}>
                {format(props.value.toPay)}
                {" ₽"}
              </span>
            </Fragment>
          ))}
      </span>
      {props.value.payed ? (
        <Fragment>
          {!props.modes.watch && <br />}
          <span style={secondRowStyles}>
            Ранее по этому заказу было оплачено{" "}
            <span style={secondRowPriceStyles}>
              {format(props.initialValue.payed)}
              {" ₽"}
            </span>
          </span>
        </Fragment>
      ) : null}
    </div>
  )
}

export default TotalSumm
