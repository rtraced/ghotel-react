import React, { Component } from "react"
import Textfield from "../Common/Textfield"
import debounce from "lodash.debounce"
import CheckBoxComponent from "../Common/Checkbox"
import SearchResults from "../Common/SearchResults"
import { searchCall } from "../../Modules/api"

const SearchBlock = props => (
  <div
    style={{
      backgroundColor: "#eee",
      width: 800,
      margin: "50px auto",
      padding: 30,
      borderRadius: 5
    }}
  >
    {props.children}
  </div>
)

class Search extends Component {
  constructor(props) {
    super(props)

    this.setQuery = debounce(this.setQuery, 500)
  }

  state = {
    query: "",
    isRefund: false,
    searchResult: null
  }

  setQuery = query => {
    this.setState({ query })
    this.updateResult(query, this.state.isRefund)
  }

  setRefund = isRefund => {
    this.setState({ isRefund })
    this.updateResult(this.state.query, isRefund)
  }

  setResult = searchResult => this.setState({ searchResult })
  clearResult = () => this.setState({ searchResult: null })

  updateResult = async (query, isRefund) => {
    if (!query && !isRefund) return this.clearResult()

    this.setResult(await searchCall(query, isRefund))
  }

  render() {
    return (
      <SearchBlock>
        <Textfield
          name={"Номер брони или имя клиента"}
          onChange={this.setQuery}
          style={{ marginBottom: 15 }}
        />

        <CheckBoxComponent
          title={"Только невозвратные"}
          value={this.state.isRefund}
          onChange={this.setRefund}
          style={{ marginBottom: 15 }}
        />

        <SearchResults
          reservations={this.state.searchResult}
          hintText="Введите что-нибудь для поиска"
        />
      </SearchBlock>
    )
  }
}

export default Search
