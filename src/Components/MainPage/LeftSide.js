import React, { Component } from "react"
import Tasks from "./Tasks"
import TodayCheckIns from "./TodayCheckIns"
import styled from "styled-components"
import TodayNoShow from "./TodayNoShow"

const LeftSideBlock = styled.div`
  width: 460px;
  flex-shrink: 0;
  overflow-y: scroll;

  &::-webkit-scrollbar {
    width: 6px;
  }

  &::-webkit-scrollbar-track {
    background: #dde2ec;
  }

  &::-webkit-scrollbar-thumb {
    background: #333;
  }
`

class LeftSide extends Component {
  render() {
    return (
      <LeftSideBlock>
        <Tasks
          tasks={this.props.tasks}
          completeTask={this.props.completeTask}
          author={this.props.admin}
        />
        <TodayCheckIns
          data={this.props.wubookToday}
          updateWubook={this.props.updateWubook}
        />
        <TodayNoShow data={this.props.noShowToday} />
      </LeftSideBlock>
    )
  }
}

export default LeftSide
