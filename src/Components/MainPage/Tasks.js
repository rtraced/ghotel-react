import React, { Component } from "react"
import styled from "styled-components"
import Modal from "react-modal"
import { Form, Button, Glyphicon } from "react-bootstrap"
import {
  ModalTitle,
  ModalHR,
  ModalContent,
  ModalControls,
  ModalButton,
  ModalButtonWithLoading
} from "../Common/ModalParts"
import Select from "../Common/Select"
import { getAdminsCall, createTaskCall } from "../../Modules/api"
import TextArea from "../Common/TextArea"

const taskModalStyle = { content: { minWidth: "auto", width: 300 } }

const TasksTitle = styled.h2`
  font-size: 24px;
  font-weight: 700;
  margin-top: 0;
`

const TasksHint = styled.p`
  font-size: 14px;
  margin-top: 0;
  color: #da4e4e;
`

const TasksList = ({ tasks, hasActiveTasks, completeTask }) => {
  if (!hasActiveTasks)
    return <div style={{ marginBottom: 10 }}>Нет активных задач.</div>
  const activeTasks = tasks.filter(t => !t.completed)

  return (
    <div>
      {activeTasks.map((t, key) => (
        <Task key={key} data={t} completeTask={completeTask} />
      ))}
    </div>
  )
}

const StyledTaskBlock = styled.span`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 10px;
  padding: 6px 6px 6px 12px;
  border-radius: 2px;
  background-color: #f7f7f7;
  vertical-align: middle;
  box-shadow: 0 1px 3px -1px rgba(0, 0, 0, 0.27);
`

const StyledTaskGlyph = styled(Glyphicon)`
  color: #444;
  padding: 3px;
  margin-left: 6px;
  align-self: flex-start;
  transform: translateY(-1px);
  cursor: ${props => (props.isFetching ? "auto" : "pointer")};
`

class Task extends Component {
  state = {
    isFetching: false
  }

  mounted = true

  completeTask = async () => {
    this.startFetching()
    await this.props.completeTask(this.props.data)
    if (this.mounted) this.stopFetching()
  }

  startFetching = () => this.setState({ isFetching: true })
  stopFetching = () => this.setState({ isFetching: false })

  componentWillUnmount() {
    this.mounted = false
  }

  render() {
    return (
      <StyledTaskBlock>
        <span>
          {this.props.data.task} (от {this.props.data.author}){" "}
        </span>
        <StyledTaskGlyph
          glyph={this.state.isFetching ? "time" : "remove"}
          onClick={this.state.isFetching ? () => {} : this.completeTask}
        />
      </StyledTaskBlock>
    )
  }
}

const AddTaskButton = props => (
  <Button
    bsSize="small"
    bsStyle="success"
    onClick={() => props.openModal("add-task")}
    style={{ marginTop: 5, width: "100%" }}
  >
    <Glyphicon glyph="plus" style={{ marginRight: 5 }} /> Добавить задачу
  </Button>
)

const ModalError = styled.div`
  color: crimson;
  margin-top: 15px;
  font-size: 14px;
`

class AddTaskModal extends Component {
  state = {
    admin: "",
    task: "",
    admins: [],
    isLoading: false,
    isAdminsFetching: true,
    error: ""
  }

  mounted = false

  async componentDidMount() {
    this.mounted = true

    const admins = await getAdminsCall()
    if (this.mounted) this.setAdmins(admins)
  }

  componentWillUnmount() {
    this.mounted = false
  }

  setAdmins = admins => this.setState({ admins, isAdminsFetching: false })

  changeAdmin = admin => this.setState({ admin })
  changeTask = task => this.setState({ task })

  setError = error => this.setState({ error })
  clearError = () => this.setState({ error: "" })

  startLoading = () => this.setState({ isLoading: true })
  stopLoading = () => this.setState({ isLoading: false })

  submitForm = e => {
    e.preventDefault()
    const { admin, task } = this.state
    const { author } = this.props
    this.clearError()

    if (!admin || !task) {
      this.setError("Заполните поля")
      return
    }

    this.startLoading()
    createTaskCall(
      this.state.admins.find(a => a.name === admin).uid,
      task,
      author
    ).then(() => {
      this.stopLoading()
      this.props.hideModal()
      this.props.openModal("success")
    })
  }

  render() {
    if (this.state.isAdminsFetching)
      return (
        <Modal isOpen={this.props.isOpen} style={taskModalStyle}>
          <ModalTitle>Создать новую задачу</ModalTitle>
          <ModalHR />
          <ModalContent>Загрузка...</ModalContent>
        </Modal>
      )

    return (
      <Modal isOpen={this.props.isOpen} style={taskModalStyle}>
        <ModalTitle>Создать новую задачу</ModalTitle>
        <ModalHR />
        <Form onSubmit={this.submitForm}>
          <ModalContent>
            <Select
              name="Задача для администратора"
              value={this.state.admin}
              values={this.state.admins.map(a => ({
                value: a.name,
                text: a.name
              }))}
              onChange={this.changeAdmin}
              style={{ marginBottom: 10 }}
            />
            <TextArea
              name="Текст задачи"
              value={this.state.task}
              onChange={this.changeTask}
            />
            {this.state.error ? (
              <ModalError>{this.state.error}</ModalError>
            ) : null}
          </ModalContent>
          <ModalHR />
          <ModalControls>
            <ModalButton bsStyle="danger" onClick={this.props.hideModal}>
              Отмена
            </ModalButton>
            <ModalButtonWithLoading
              bsStyle="success"
              type="submit"
              loading={this.state.isLoading}
            >
              Добавить задачу
            </ModalButtonWithLoading>
          </ModalControls>
        </Form>
      </Modal>
    )
  }
}

const SuccessModal = props => (
  <Modal isOpen={props.isOpen} style={taskModalStyle}>
    <ModalTitle>Успешно!</ModalTitle>
    <ModalHR />
    <ModalContent>Задача создана.</ModalContent>
    <ModalHR />
    <ModalControls>
      <ModalButton bsStyle="danger" onClick={props.hideModal}>
        Закрыть
      </ModalButton>
    </ModalControls>
  </Modal>
)

class Tasks extends Component {
  state = {
    openedModals: []
  }

  openModal = name =>
    this.setState(prev => ({ openedModals: prev.openedModals.concat(name) }))
  closeModal = name =>
    this.setState(prev => ({
      openedModals: prev.openedModals.filter(m => m !== name)
    }))

  render() {
    const hasActiveTasks = this.props.tasks.some(t => !t.completed)

    return (
      <div style={{ padding: 30 }}>
        <TasksTitle>Задачи для вас</TasksTitle>
        {hasActiveTasks ? (
          <TasksHint>
            Чтобы отметить задачу как выполненную, нажмите на крестик
          </TasksHint>
        ) : null}
        <TasksList
          tasks={this.props.tasks}
          completeTask={this.props.completeTask}
          hasActiveTasks={hasActiveTasks}
        />
        <AddTaskButton openModal={this.openModal} />
        <AddTaskModal
          isOpen={this.state.openedModals.includes("add-task")}
          author={this.props.author}
          openModal={this.openModal}
          hideModal={() => this.closeModal("add-task")}
        />
        <SuccessModal
          isOpen={this.state.openedModals.includes("success")}
          hideModal={() => this.closeModal("success")}
        />
      </div>
    )
  }
}

export default Tasks
