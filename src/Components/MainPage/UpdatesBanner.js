import React, { Component } from "react"
import styled from "styled-components"
import Cookies from "js-cookie"

const UpdatesBannerBlock = styled.div`
  position: absolute;
  left: 30px;
  bottom: 30px;
  width: 100%;
  max-width: 500px;
  min-height: 150px;
  background: crimson;
  z-index: 9999999999;
  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.27);
`

const UpdatesBannerHead = styled.div`
  text-align: right;
  background: #7b1f31;
`

const UpdatesBannerClose = styled.div`
  display: inline-block;
  font-size: 36px;
  line-height: 24px;
  padding: 8px;
  color: #fff;
  cursor: pointer;
`

const UpdatesBannerText = styled.div`
  font-size: 16px;
  padding: 12px;
  color: #fff;
`

class UpdatesBanner extends Component {
  state = {
    isClosed: true
  }

  componentDidMount() {
    let isClosed = Cookies.get("isBannerClosed")
    if (!isClosed) isClosed = false

    this.setState({
      isClosed
    })
  }

  close = () => {
    Cookies.set("isBannerClosed", true)
    this.setState({ isClosed: true })
  }

  render() {
    if (this.state.isClosed) return null

    return (
      <UpdatesBannerBlock>
        <UpdatesBannerHead>
          <UpdatesBannerClose onClick={this.close}>&times;</UpdatesBannerClose>
        </UpdatesBannerHead>
        <UpdatesBannerText>
          Теперь карты записываются только после сохранения брони. При создании
          частично оплаченной брони, карты записываются только на оплаченный
          период.
        </UpdatesBannerText>
      </UpdatesBannerBlock>
    )
  }
}

export default UpdatesBanner
