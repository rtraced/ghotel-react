import React, { Component } from "react"
import processReservations from "../../Modules/helpers/processReservations"
import LeftSide from "./LeftSide"
import Grid from "../Grid/Grid"
import qs from "qs"
import moment from "moment"
import LoadingModal from "../Common/LoadingModal"
import {
  completeTaskCall,
  getModelCall,
  getReservationsCall,
  getTasksCall,
  getWubookListCall,
  checkNewWubookCall,
  getNoShowTodayCall
} from "../../Modules/api"
import styled from "styled-components"
import UpdatesBanner from "./UpdatesBanner"

const MainPageBlock = styled.div`
  width: 100%;
  max-width: 100%;
  height: 100%;
  display: flex;
  justify-content: space-between;
`

const GridBlock = styled.div`
  overflow: scroll;

  &::-webkit-scrollbar {
    width: 12px;
    height: 12px;
  }

  &::-webkit-scrollbar-track {
    background: #dde2ec;
  }

  &::-webkit-scrollbar-thumb {
    background: #473e80;
  }
`

class MainPage extends Component {
  state = {
    // Data received first
    model: null,
    data: null,
    gridToday: moment().format("YYYY-MM-DD"),
    tasks: null,
    wubook: null,
    wubookLoading: false,
    noShowToday: null,

    // Data fetch status
    isFetching: true
  }

  getWubook = async () => {
    const isLaterThan8 = +moment().format("HH") > 7

    if (isLaterThan8) {
      return await getWubookListCall(moment().format("YYYY-MM-DD"))
    } else {
      const [wubookYesterday, wubookToday] = await Promise.all([
        getWubookListCall(
          moment()
            .subtract(1, "days")
            .format("YYYY-MM-DD")
        ),
        getWubookListCall(moment().format("YYYY-MM-DD"))
      ])

      return wubookYesterday.concat(wubookToday)
    }
  }

  getAllData = async () => {
    const yesterday = moment().subtract(1, "days")

    const dateObj = {
      year: yesterday.format("YYYY"),
      month: yesterday.format("MM"),
      day: yesterday.format("DD")
    }

    console.log(dateObj)

    const [model, reservations, tasks, noShowToday] = await Promise.all([
      getModelCall(),
      getReservationsCall(),
      getTasksCall(this.props.adminID),
      getNoShowTodayCall(dateObj)
    ])

    const wubook = await this.getWubook()

    this.setState({
      model,
      data: processReservations(reservations),
      tasks,
      wubook,
      noShowToday,
      isFetching: false
    })
  }

  updateWubook = () => {
    this.setState({ wubookLoading: true })
    checkNewWubookCall()
      .then(async res => {
        const newWubook = await this.getWubook()
        console.log(res, newWubook)
        if (JSON.stringify(newWubook) !== JSON.stringify(this.state.wubook)) {
          this.setState({ wubookToday: newWubook })
          alert("Загружены новые заезды")
        } else {
          alert("Новых заездов нет")
        }
      })
      .catch(err => {
        console.log(err.message)
        alert("Ошибка сервера")
      })
      .then(() => {
        this.setState({ wubookLoading: false })
      })
  }

  setGridToday = gridToday => this.setState({ gridToday })

  completeTask = task => {
    return completeTaskCall(task).then(() => {
      this.setState(prev => ({
        tasks: prev.tasks.filter(t => t.pk !== task.pk)
      }))
    })
  }

  // Fetch all the required data on component mount
  componentDidMount() {
    const q = qs.parse(this.props.location.search.slice(1))
    if (q.gridToday) this.setGridToday(q.gridToday)
    this.getAllData()
  }

  render() {
    const { daysInFuture, daysInPast, isSettingsLoading, admin } = this.props
    const { isFetching, data, model, tasks, wubook, noShowToday } = this.state

    if (isFetching || isSettingsLoading) {
      return <LoadingModal isOpen={true} />
    }

    return (
      <MainPageBlock>
        <LeftSide
          tasks={tasks}
          wubookToday={wubook}
          noShowToday={noShowToday}
          updateWubook={this.updateWubook}
          completeTask={this.completeTask}
          admin={admin}
        />
        <GridBlock>
          <Grid
            daysInFuture={daysInFuture}
            daysInPast={daysInPast}
            today={this.state.gridToday}
            data={data}
            model={model}
          />
        </GridBlock>
        <UpdatesBanner />
        <LoadingModal isOpen={this.state.wubookLoading} />
      </MainPageBlock>
    )
  }
}

export default MainPage
