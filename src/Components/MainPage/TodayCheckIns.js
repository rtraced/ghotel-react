import React, { Component, Fragment } from "react"
import styled from "styled-components"
import moment from "moment"
import history from "../../Modules/helpers/history"
import { Table, Glyphicon } from "react-bootstrap"
import ReactTooltip from "react-tooltip"

const calculateTotal = reservation => {
  let total = 0
  for (let i = 0; i < reservation.reserved_days.length; i++) {
    total = total + parseFloat(reservation.reserved_days[i].price)
  }

  for (let i = 0; i < reservation.additional_services.length; i++) {
    total = total + parseFloat(reservation.additional_services[i].price)
  }

  return Math.round(total)
}

const CheckInsTitle = styled.h2`
  font-size: 24px;
  font-weight: 700;
  margin: 0;
`

const RTD = styled.td`
  vertical-align: middle !important;
  padding: 16px 8px;
  width: ${props => props.width}px;
  text-align: ${props => props.textAlign};
`

const CheckInsHint = styled.p`
  font-size: 14px;
  margin-top: 0;
  color: #da4e4e;
`

const GRCell = styled.td.attrs({
  colSpan: 3
})`
  font-weight: 700;
  color: #555;
  text-align: center;
  cursor: default;

  &:hover {
    background-color: #fff;
  }
`

const GRHeader = props => (
  <tr>
    <GRCell>Групповая бронь</GRCell>
  </tr>
)

const GRFooter = props => (
  <tr>
    <GRCell>Общая стоимость групповой брони: {props.total} ₽</GRCell>
  </tr>
)

const ReservationRow = ({ reservation }) => (
  <tr
    style={{ cursor: "pointer" }}
    data-for="today-checkins-tooltip"
    data-tip={reservation.booking_number}
    data-scroll-hide="false"
    onClick={() => {
      history.push("/reservation?pk=" + reservation.pk)
    }}
  >
    <RTD width={150}>{reservation.guest_name}</RTD>
    <RTD width={110}>{moment(reservation.end).format("DD MMMM")}</RTD>
    <RTD width={80} textAlign="right">
      {calculateTotal(reservation)} ₽
    </RTD>
  </tr>
)

const CheckInsTable = ({ data }) => {
  if (!data.length) {
    return (
      <div style={{ marginBottom: 10 }}>
        Брони с заселением сегодня не найдены.
      </div>
    )
  }

  const groupedByBN = {}
  data.forEach(r => {
    if (!groupedByBN[r.booking_number]) groupedByBN[r.booking_number] = []
    groupedByBN[r.booking_number].push(r)
  })

  const rows = []
  let idx = 0
  for (let bn in groupedByBN) {
    let total = 0
    const isGroup = groupedByBN[bn].length > 1

    if (isGroup) rows.push(<GRHeader key={idx++} />)

    // eslint-disable-next-line
    groupedByBN[bn].forEach(r => {
      rows.push(<ReservationRow reservation={r} key={idx++} />)
      total += calculateTotal(r)
    })

    if (isGroup) rows.push(<GRFooter total={total} key={idx++} />)
  }

  return (
    <Fragment>
      <Table
        responsive
        className="table-hover condensed"
        style={{ width: "100%" }}
      >
        <thead>
          <tr>
            <th>Гость</th>
            <th>Дата выезда</th>
            <th style={{ textAlign: "right" }}>Цена</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </Table>
      <ReactTooltip
        id="today-checkins-tooltip"
        getContent={bn => `Номер: ${bn}`}
        place="top"
      />
    </Fragment>
  )
}

const UpdateButton = styled.span`
  padding: 5px 10px;
  font-size: 12px;
  display: inline-block;
  color: #fff;
  background-color: #444;
  border-radius: 3px;
  line-height: 1.3;
  cursor: pointer;
`

class TodayCheckIns extends Component {
  render() {
    return (
      <div style={{ padding: 30 }}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            marginBottom: 10
          }}
        >
          <CheckInsTitle>Заезды сегодня</CheckInsTitle>
          <UpdateButton onClick={this.props.updateWubook}>
            <Glyphicon glyph="refresh" style={{ marginRight: 8 }} />
            Загрузить новые
          </UpdateButton>
        </div>

        {this.props.data.length ? (
          <CheckInsHint>Для заселения нажмите на нужную бронь</CheckInsHint>
        ) : null}
        <CheckInsTable data={this.props.data} />
      </div>
    )
  }
}

export default TodayCheckIns
