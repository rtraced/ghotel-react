import React, { Component, Fragment } from "react"
import Logs from "../../Modules/Logs"
import debounce from "lodash.debounce"
import qs from "qs"
import {
  Grid,
  Row,
  Col,
  Jumbotron,
  Button,
  ButtonGroup,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap"
import history from "../../Modules/helpers/history"
import TextField from "../Common/Textfield"
import Textarea from "../Common/TextArea"
import Checkbox from "../Common/Checkbox"
import DatePicker from "../Common/RangeDatePicker"
import Day from "../Common/Day"
import Time from "../Common/CheckInCheckOutTime"
import RoomUpgrade from "../Common/RoomUpgrade"
import Service from "../Common/Service"
import {
  ModalTitle,
  ModalHR,
  ModalContent,
  ModalControls,
  ModalButton,
  ModalButtonWithLoading,
  ModalSuccessMessage,
  ModalError
} from "../Common/ModalParts"
import TotalSumm from "./TotalSumm"
import Rooms from "./Rooms"
import Modal from "react-modal"
import LoadingModal from "../Common/LoadingModal"
import styled from "styled-components"
import BottomBarButtons, {
  BarButtonGroup,
  BarButton
} from "../Common/BottomBarButtons"
import PrintServerErrorModal from "../Common/PrintServerErrorModal"

const handlePrintBlankClick = reservation => {
  const confirmation = window.confirm(`
    Проверьте, чтобы в брони было корректно указано ФИО.
    Оно должно быть указано в формате «Фамилия, Имя, Отчество (при наличии)».
    Текущее ФИО в брони: ${reservation.guest_name}

    Если корректно - нажмите «OK», если нет - «Отмена» и отредактируйте поле «ФИО гостя»
  `)

  if (confirmation)
    window.location.href = `/print_blank.html?data=${JSON.stringify(
      reservation
    )}`
}

const StyledJumbotron = props => (
  <Jumbotron style={{ padding: 30 }}>{props.children}</Jumbotron>
)

const CheckJumbotron = props => (
  <Jumbotron style={{ padding: 20 }}>{props.children}</Jumbotron>
)

const StyledTitle = props => (
  <h3 style={{ marginTop: 5, marginBottom: 25 }}>{props.children}</h3>
)

const CheckTitle = props => (
  <h4
    style={{
      fontWeight: 700,
      marginTop: 0,
      marginBottom: 25
    }}
  >
    {props.children}
  </h4>
)

const CheckBlock = props => (
  <Col xs={3} style={{ position: "sticky", top: 110 }}>
    <Check
      newReservedDays={props.newReservedDays}
      allServices={props.allServices}
    />
  </Col>
)

const CheckRow = props => {
  const liStyles = {
    fontSize: 13,
    marginBottom: 10
  }

  return <li style={liStyles}>{props.children}</li>
}

const Check = props => {
  let id = 0

  const ulStyles = {
    listStyle: "none",
    paddingLeft: 5
  }

  const isDayCorrect = d => d.price && d.payment_type && d.payment_date
  const isServiceCorrect = s =>
    s.price && s.payment_type && s.payment_date && s.quantity

  return (
    <CheckJumbotron>
      <CheckTitle>Чек</CheckTitle>
      <ul style={ulStyles}>
        {props.newReservedDays.map(d =>
          isDayCorrect(d) ? (
            <CheckRow key={id++}>
              <span style={{ color: "#007d00" }}>
                Проживание за {d.date} - {d.price} ₽
              </span>
            </CheckRow>
          ) : null
        )}
        {props.allServices.map(s =>
          isServiceCorrect(s) ? (
            <CheckRow key={id++}>
              <span style={{ color: "#007d00" }}>
                {s.service} ({s.quantity} шт) - {s.price * s.quantity} ₽
              </span>
            </CheckRow>
          ) : null
        )}
      </ul>
    </CheckJumbotron>
  )
}

const SuccessModal = props => {
  return (
    <Modal isOpen={props.isOpen} style={{ content: { width: 740 } }}>
      <ModalTitle>Бронь создана!</ModalTitle>
      <ModalHR />
      <ModalContent>Успешно</ModalContent>
      <ModalHR />
      <ModalControls>
        <ButtonGroup style={{ marginLeft: 10 }}>
          <Button bsStyle="primary" disabled>
            Печать формы
          </Button>
          <Button href="/print_ruform.html">RU</Button>
          <Button href="/print_engform.html">EN</Button>
        </ButtonGroup>
        <ModalButton
          bsStyle="primary"
          onClick={() => handlePrintBlankClick(props.reservation)}
        >
          Печать БО
        </ModalButton>
        <ModalButton bsStyle="primary" onClick={props.showWritecardModal}>
          Записать карту
        </ModalButton>
        <ModalButton
          bsStyle="danger"
          style={{ marginLeft: "auto" }}
          onClick={() => {
            props.hideModal()
            history.push("/")
          }}
        >
          Закрыть
        </ModalButton>
      </ModalControls>
    </Modal>
  )
}

const ModalText = props => (
  <p style={{ margin: 0, padding: "15px 20px" }}>{props.children}</p>
)

const ErrorParagraph = styled.span`
  display: block;
  color: #d43f3a;
  margin-bottom: 10px;

  &:last-child {
    margin-bottom: 0;
  }
`

const AllErrorsModal = props => {
  const { errorFields } = props

  const getErrorString = code => {
    switch (code) {
      case "guest_name":
        return "Имя гостя не заполнено"
      case "booking_number":
        return "Номер брони не заполнен"
      case "guest_phone":
        return "Телефон гостя не заполнен"
      case "guests_number":
        return "Количество гостей не заполнено"
      case "rooms":
        return "Комната для заселения не выбрана"
      case "checkout-payment_type":
        return "Не выбран метод оплаты позднего выезда"
      case "checkin-payment_type":
        return "Не выбран метод оплаты раннего въезда"
      case "payment_methods":
        return "В один и тот же день не могут использоваться разные методы оплаты"
      default:
        if (/day-.*-price/.test(code))
          return "У одного или нескольких дней не выбрана стоимость"
        if (/day-.*-payment_type/.test(code))
          return "У одного или нескольких дней не выбран метод оплаты (или выбраны разные методы оплаты в один день)"
        if (/service-.*-date/.test(code))
          return "У одной или нескольких услуг не выбрана дата"
        if (/service-.*-quantity/.test(code))
          return "У одной или нескольких услуг не выбрано количество"
        if (/service-.*-payment_type/.test(code))
          return "У одной или нескольких услуг не выбран метод оплаты (или выбраны разные методы оплаты в один день)"
        if (/upgrade-.*-date/.test(code))
          return "У одного или нескольких улучшений не выбрана дата"
        if (/upgrade-.*-payment_type/.test(code))
          return "У одного или нескольких улучшений не выбран метод оплаты"
        return "Неизвестная ошибка"
    }
  }

  const strings = []
  errorFields.forEach(errCode => {
    const str = getErrorString(errCode)
    if (!strings.includes(str)) strings.push(str)
  })

  return (
    <Modal isOpen={props.isOpen}>
      <ModalTitle>Ошибка!</ModalTitle>
      <ModalHR />
      <ModalText>
        {strings.map((s, idx) => (
          <ErrorParagraph key={idx}>{s}</ErrorParagraph>
        ))}
      </ModalText>
      <ModalHR />
      <ModalControls>
        <ModalButton
          bsStyle="danger"
          style={{ marginLeft: "auto" }}
          onClick={props.hideModal}
        >
          Закрыть
        </ModalButton>
      </ModalControls>
    </Modal>
  )
}

const ErrorModal = props => {
  return (
    <Modal isOpen={props.isOpen}>
      <ModalTitle>Ошибка!</ModalTitle>
      <ModalHR />
      <ModalContent>
        Один или несколько новых дней невозможно добавить, так как они
        принадлежат брони {props.errorPK}
      </ModalContent>
      <ModalHR />
      <ModalControls>
        <ModalButton
          bsStyle="danger"
          style={{ marginLeft: "auto" }}
          onClick={props.hideModal}
        >
          Закрыть
        </ModalButton>
      </ModalControls>
    </Modal>
  )
}

const FDParagraph = styled.p`
  margin-bottom: 20px;
`

const FDItem = styled.p`
  padding-left: 15px;
`

const FDMessage = styled.p`
  margin-top: 20px;
`

const FDQuestion = styled.p`
  margin-top: 20px;
  font-weight: 700;
`

const FoundDuplicatesModal = props => {
  const { duplicatesData } = props

  const isMultiple = duplicatesData.duplicates.length > 1

  return (
    <Modal isOpen={props.isOpen}>
      <ModalTitle>Ошибка!</ModalTitle>
      <ModalHR />
      <ModalContent>
        <FDParagraph>
          {isMultiple ? "Найдены брони" : "Найдена бронь"} с таким же номером:
        </FDParagraph>
        {duplicatesData.duplicates.map((d, idx) => (
          <FDItem key={idx}>
            Дата заселения: {d.start} | Комната: {d.room || "Не выбрана"}
          </FDItem>
        ))}
        {!isMultiple && <FDMessage>{duplicatesData.message}</FDMessage>}
        <FDQuestion>
          Создавая бронь, вы подтверждаете, что проверили данную бронь на
          дублирование
        </FDQuestion>
      </ModalContent>
      <ModalHR />
      <ModalControls>
        <ModalButton bsStyle="success" onClick={props.hideModal}>
          Не создавать бронь
        </ModalButton>
        <ModalButton
          bsStyle="danger"
          onClick={() => {
            props.confirmDuplicate()
            props.saveNewReservation()
          }}
        >
          Создать бронь
        </ModalButton>
      </ModalControls>
    </Modal>
  )
}

class WriteCardModal extends Component {
  state = {
    fetching: false,
    successMessage: null,
    isWrited: false
  }

  render() {
    return (
      <Modal isOpen={this.props.isOpen} style={{ content: { width: 540 } }}>
        <ModalTitle>Запись карты</ModalTitle>
        <ModalHR />
        <ModalContent>Приложите карту</ModalContent>
        {this.state.successMessage ? (
          <ModalSuccessMessage>{this.state.successMessage}</ModalSuccessMessage>
        ) : null}
        {this.props.error ? <ModalError>{this.props.error}</ModalError> : null}
        <ModalHR />
        <ModalControls>
          <ModalButton
            bsStyle="danger"
            onClick={() => {
              Logs.logPressAction("Закрыть диалог записи карт")
              this.props.hideModal()
              this.props.hideError()
            }}
          >
            {this.state.isWrited ? "Завершить" : "Отмена"}
          </ModalButton>
          <ModalButtonWithLoading
            loading={this.state.fetching}
            bsStyle="success"
            onClick={() => {
              Logs.logPressAction("Запись карты")
              this.props.hideError()
              this.setState({ fetching: true, successMessage: null })
              this.props
                .action()
                .then(
                  () => {
                    Logs.logEvent("Карта успешно записана")
                    this.setState({
                      successMessage: "Карта успешно записана!",
                      isWrited: true
                    })
                  },
                  () => {
                    Logs.logError(`Неудачная запись карты: ${this.props.error}`)
                  }
                )
                .then(() => {
                  this.setState({ fetching: false })
                })
            }}
          >
            {this.state.isWrited ? "Записать еще" : "Записать"}
          </ModalButtonWithLoading>
        </ModalControls>
      </Modal>
    )
  }
}

const Services = props => (
  <StyledJumbotron>
    <StyledTitle>Дополнительные услуги</StyledTitle>
    <Service
      services={props.services}
      createdServices={props.createdServices}
      service={props.service}
      days={props.days}
      servicesError={props.servicesError}
      onDateChange={props.onServiceDateChange}
      onServiceChange={props.onServiceServiceChange}
      onQtyChange={props.onServiceQtyChange}
      onPaymentChange={props.onServicePaymentChange}
      onCreate={() => props.onServiceCreate(props.service)}
      onServiceQtyChange={props.onCreatedServiceQtyChange}
      onServicePaymentChange={props.onCreatedServicePaymentChange}
      onServiceDateChange={props.onCreatedServiceDateChange}
      onServiceCopy={props.onServiceCopyClick}
      onServiceDelete={props.onServiceDeleteClick}
      onServiceRefund={props.onServiceRefundClick}
      isRefund={props.isRefund}
      modes={props.modes}
      errorFields={props.errorFields}
    />
  </StyledJumbotron>
)

const Upgrades = props => (
  <StyledJumbotron>
    <StyledTitle>Улучшение номера</StyledTitle>
    <RoomUpgrade
      days={props.days}
      availableDays={props.availableDaysToUpgrade}
      createdUpgrades={props.createdUpgrades}
      upgradesError={props.upgradesError}
      newUpgrade={props.newUpgrade}
      onDateChange={props.onUpgradeDateChange}
      onPaymentChange={props.onUpgradePaymentChange}
      onPriceChange={props.onUpgradePriceChange}
      onCreate={() => props.onUpgradeCreate(props.newUpgrade)}
      onUpgradeDateChange={props.onCreatedServiceDateChange}
      onUpgradePriceChange={props.onCreatedServicePriceChange}
      onUpgradePaymentChange={props.onCreatedServicePaymentChange}
      onUpgradeCopy={props.onServiceCopyClick}
      onUpgradeDelete={props.onServiceDeleteClick}
      onUpgradeRefund={props.onServiceRefundClick}
      setUpgradeError={props.setUpgradeError}
      clearUpgradeError={props.clearUpgradeError}
      modes={props.modes}
      errorFields={props.errorFields}
    />
  </StyledJumbotron>
)

class NewReservation extends Component {
  constructor(props) {
    super(props)

    this.state = {
      notPayedConfirmed: false,
      initialTotal: 0,
      activeModals: []
    }
  }

  componentDidMount() {
    const q = qs.parse(this.props.location.search.slice(1))

    this.props.createNewReservation(q.room, q.roomType, q.start, q.end)
    this.props.getServices()
  }

  componentWillUnmount() {
    this.props.clearReservation()
  }

  showModal = name =>
    this.setState(prev => ({
      activeModals: prev.activeModals.concat(name)
    }))

  hideModal = name =>
    this.setState(prev => ({
      activeModals: prev.activeModals.filter(m => m !== name)
    }))

  onDatesChange = (startDate, endDate) => {
    const fSD = startDate.format("YYYY-MM-DD")
    const fED = endDate.format("YYYY-MM-DD")
    if (this.props.start !== fSD) {
      this.props.onStartChange(fSD)
    }

    if (this.props.end !== fED) {
      this.props.onEndChange(fED)
    }
  }

  calcTotal = () => {
    let totalToPay = 0.0
    let totalPayed = 0.0
    this.props.reservedDays
      .filter(d => d.type !== "payedDay")
      .forEach(d => {
        totalToPay +=
          isNaN(parseFloat(d.price)) ||
          d.payment_date === "" ||
          d.payment_type === ""
            ? 0
            : parseFloat(d.price)
      })
    this.props.allServices
      .filter(s => s.payed !== true)
      .forEach(s => {
        totalToPay +=
          isNaN(parseFloat(s.price * s.quantity)) || s.payment_type === ""
            ? 0
            : parseFloat(s.price * s.quantity)
      })
    this.props.payedReservedDays.forEach(d => {
      totalPayed += isNaN(parseFloat(d.price)) ? 0 : parseFloat(d.price)
    })
    this.props.allServices
      .filter(s => s.payed === true)
      .forEach(s => {
        totalPayed += isNaN(parseFloat(s.price * s.quantity))
          ? 0
          : parseFloat(s.price * s.quantity)
      })

    return {
      toPay: totalToPay,
      payed: totalPayed
    }
  }

  confirmNotPayed = () => {
    this.setState({ notPayedConfirmed: true })
  }

  newDays = modes => {
    return (
      <StyledJumbotron>
        <StyledTitle>Новые дни</StyledTitle>

        {this.props.newReservedDays.map(day => (
          <Day
            key={day.id}
            day={day}
            price={day.price}
            method={day.payment_type}
            onDayPriceChange={value =>
              this.props.onDayPriceChange(day.id, value)
            }
            onDayPaymentChange={value =>
              this.props.onDayMethodChange(day.id, value)
            }
            onDayPayedChange={value =>
              this.props.onDayPayedChange(day.id, value)
            }
            payed={!!day.payment_date}
            modes={modes}
            notPayedConfirmed={this.state.notPayedConfirmed}
            confirmNotPayed={this.confirmNotPayed}
            errorFields={this.props.errorFields}
          />
        ))}
        <div style={{ marginTop: 10 }}>
          {this.props.newReservedDays.length > 1 ? (
            <Fragment>
              <Button
                bsStyle="primary"
                onClick={() => this.props.onDaySummCopy()}
              >
                Копировать цену с первого дня
              </Button>
              <Button
                style={{ marginLeft: 5 }}
                bsStyle="primary"
                onClick={() => this.props.onDayMethodCopy()}
              >
                Копировать метод с первого дня
              </Button>
            </Fragment>
          ) : null}
        </div>
        <div style={{ marginTop: 10 }}>
          <Button
            onClick={() => this.props.onReservationDayAdd()}
            bsStyle="success"
          >
            Добавить сутки
          </Button>
          {this.props.newReservedDays.length > 1 ? (
            <Button
              onClick={() => this.props.onReservationDayRemove()}
              style={{ marginLeft: 5 }}
              bsStyle="danger"
            >
              Убрать сутки
            </Button>
          ) : null}
        </div>
      </StyledJumbotron>
    )
  }

  setInitialTotal = () => {
    this.setState({ initialTotal: this.calcTotal() })
  }

  onNameChange = debounce(this.props.onNameChange, 300)
  onPhoneChange = debounce(this.props.onPhoneChange, 300)
  onMailChange = debounce(this.props.onMailChange, 300)
  onNoteChange = debounce(this.props.onNoteChange, 300)

  render() {
    const modes = {
      create: true,
      sale: false,
      refund: false,
      watch: false
    }

    return (
      <Grid fluid={true} style={{ marginTop: 20, paddingBottom: 105 }}>
        <Row>
          <Col xs={9}>
            <StyledJumbotron>
              <Row>
                <Col xs={3}>
                  <Rooms rooms={this.props.rooms} />
                  <FormGroup
                    style={{ marginBottom: 10 }}
                    validationState={
                      this.props.errorFields.includes("guests_number")
                        ? "error"
                        : null
                    }
                  >
                    <ControlLabel>Количество гостей</ControlLabel>
                    <FormControl
                      value={this.props.guestsNumber}
                      onChange={e =>
                        this.props.onGuestsNumberChange(e.target.value)
                      }
                      type="number"
                      bsSize="small"
                    />
                  </FormGroup>
                  <TextField
                    defaultValue={this.props.name}
                    onChange={this.onNameChange}
                    name="ФИО гостя"
                    placeholder="Иванов"
                    validationState={
                      this.props.errorFields.includes("guest_name")
                        ? "error"
                        : null
                    }
                    style={{ marginBottom: 10 }}
                  />
                  <TextField
                    value={this.props.bookingNumber}
                    onChange={this.props.onBookingNumberChange}
                    name="Номер брони"
                    placeholder="123456789"
                    validationState={
                      this.props.errorFields.includes("booking_number")
                        ? "error"
                        : null
                    }
                    style={{ marginBottom: 10 }}
                  />
                  <TextField
                    defaultValue={this.props.phone}
                    onChange={this.onPhoneChange}
                    name="Телефон гостя"
                    placeholder="+71234567890"
                    validationState={
                      this.props.errorFields.includes("guest_phone")
                        ? "error"
                        : null
                    }
                    style={{ marginBottom: 10 }}
                  />
                  <TextField
                    defaultValue={this.props.mail}
                    onChange={this.onMailChange}
                    name="Почта гостя"
                    placeholder="ivanov@gmail.com"
                    style={{ marginBottom: 10 }}
                  />
                </Col>
                <Col xs={4}>
                  <Textarea
                    defaultValue={this.props.note}
                    onChange={this.onNoteChange}
                    name="Примечание"
                    placeholder="Полезное примечание"
                    style={{ marginBottom: 10 }}
                  />
                </Col>
                <Col xs={5}>
                  <Checkbox
                    name="Оплата"
                    title="Бронь оплачена полностью"
                    value={this.props.payed}
                    onChange={value => {
                      if (this.state.notPayedConfirmed) {
                        return this.props.onReservationPayedChange(value)
                      } else {
                        if (
                          window.confirm(
                            "Вы действительно хотите создать бронь без оплаты?"
                          )
                        ) {
                          this.confirmNotPayed()
                          return this.props.onReservationPayedChange(value)
                        }
                      }
                    }}
                    formStyle={{ marginBottom: 15 }}
                  />
                  <Checkbox
                    name="Возвратность"
                    title="Бронь невозвратная"
                    value={this.props.hasRefund}
                    onChange={() =>
                      this.props.onRefundChange(!this.props.hasRefund)
                    }
                    formStyle={{ marginBottom: 15 }}
                  />

                  <DatePicker
                    name={"Даты проживания"}
                    startDate={this.props.start}
                    endDate={this.props.end}
                    onDatesChange={(start, end) =>
                      this.onDatesChange(start, end)
                    }
                  />
                </Col>
              </Row>
            </StyledJumbotron>

            {this.props.guestsNumber > 0 && this.newDays(modes)}

            <StyledJumbotron>
              <StyledTitle>Время заезда и выезда</StyledTitle>
              <Time
                type="checkin"
                onChange={v =>
                  this.props.onCheckInTimeChange({
                    payedDays: this.props.payedReservedDays,
                    notPayedDays: this.props.notPayedReservedDays,
                    newDays: this.props.newReservedDays,
                    currentTime: this.props.globalCheckIn,
                    newTime: v
                  })
                }
                onMount={() =>
                  this.props.setCheckInTime(this.props.globalCheckIn)
                }
                time={this.props.localCheckIn}
                price={this.props.checkInPrice}
                name={"Время заезда"}
                payment={this.props.checkInPayment}
                disabled={this.props.wasCheckInBought}
                onPaymentChange={v => this.props.onCheckInPaymentChange(v)}
                modes={modes}
                errorFields={this.props.errorFields}
              />
              <Time
                type="checkout"
                onChange={v =>
                  this.props.onCheckOutTimeChange({
                    payedDays: this.props.payedReservedDays,
                    notPayedDays: this.props.notPayedReservedDays,
                    newDays: this.props.reservedDays,
                    currentTime: this.props.globalCheckOut,
                    newTime: v
                  })
                }
                onMount={() =>
                  this.props.setCheckOutTime(this.props.globalCheckOut)
                }
                time={this.props.localCheckOut}
                price={this.props.checkOutPrice}
                name={"Время выезда"}
                payment={this.props.checkOutPayment}
                disabled={this.props.wasCheckOutBought}
                onPaymentChange={v => this.props.onCheckOutPaymentChange(v)}
                modes={modes}
                errorFields={this.props.errorFields}
              />
            </StyledJumbotron>
            <Services {...this.props} modes={modes} />
            <Upgrades {...this.props} modes={modes} />
          </Col>
          <CheckBlock {...this.props} />
        </Row>
        <div
          style={{
            position: "fixed",
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
            transform: "translateZ(0)",
            width: "100%",
            backgroundColor: "#fff",
            boxShadow: "0 -2px 5px 0 rgba(0, 0, 0, .27)",
            bottom: 0,
            left: 0,
            marginBottom: 0,
            padding: 25
          }}
        >
          <TotalSumm
            value={this.calcTotal()}
            initialValue={this.state.initialTotal}
            refundSumm={this.props.refundSumm}
            modes={modes}
          />
          <BottomBarButtons>
            <BarButtonGroup style={{ marginRight: 20 }}>
              <Button bsStyle="primary" disabled>
                Печать формы
              </Button>
              <Button href="/print_ruform.html">RU</Button>
              <Button href="/print_engform.html">EN</Button>
            </BarButtonGroup>
            <BarButton
              style={{ marginRight: 20 }}
              bsStyle="primary"
              onClick={() => handlePrintBlankClick(this.props.reservation)}
            >
              Печать БО
            </BarButton>
            <BarButton
              style={{ float: "right" }}
              bsStyle="success"
              onClick={this.props.saveNewReservation}
            >
              Создать
            </BarButton>
          </BottomBarButtons>
        </div>
        <SuccessModal
          isOpen={this.props.showSuccessModal}
          hideModal={this.props.hideSuccessModal}
          pk={this.props.reservation.pk}
          reservation={this.props.reservation}
          showWritecardModal={() => {
            Logs.logPressAction("Открыть диалог записи карт")
            this.showModal("writecard")
          }}
        />
        <ErrorModal
          isOpen={this.props.showErrorModal}
          errorPK={this.props.errorPK}
          hideModal={this.props.hideErrorModal}
        />
        <AllErrorsModal
          isOpen={this.props.showModalWithAllErrors}
          hideModal={this.props.closeModalWithAllErrors}
          errorFields={this.props.errorFields}
        />
        <PrintServerErrorModal
          isOpen={this.props.isPrintServerErrorModalActive}
          hideModal={this.props.hidePrintServerError}
          tryAgainAction={() => this.props.saveReservation(modes)}
        />
        <WriteCardModal
          isOpen={this.state.activeModals.includes("writecard")}
          hideModal={() => this.hideModal("writecard")}
          action={this.props.writeCard}
          error={this.props.writecardError}
          hideError={this.props.clearWritecardError}
          pk={this.props.reservation.pk}
        />
        <FoundDuplicatesModal
          isOpen={
            this.props.isDuplicatesFound && !this.props.isDuplicateConfirmed
          }
          duplicatesData={this.props.duplicatesData}
          confirmDuplicate={this.props.confirmDuplicate}
          saveNewReservation={this.props.saveNewReservation}
          hideModal={this.props.clearDuplicates}
        />
        <LoadingModal isOpen={this.props.isLoading} />
      </Grid>
    )
  }
}

export default NewReservation
