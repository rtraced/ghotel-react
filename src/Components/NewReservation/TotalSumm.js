import React, { Fragment } from "react"

const format = num => parseFloat(num).toFixed(2)

const TotalSumm = props => (
  <div style={{ marginRight: "auto" }}>
    <span style={{ fontSize: 20 }}>
      Продажа будет выполнена на сумму{" "}
      <span style={{ fontSize: 22, fontWeight: 500, color: "crimson" }}>
        {format(props.value.toPay)}
        {" ₽"}
      </span>
    </span>
    {props.value.payed ? (
      <Fragment>
        <br />
        <span style={{ fontSize: 15, color: "#555" }}>
          Ранее по этому заказу было оплачено{" "}
          <span style={{ fontSize: 17, fontWeight: 500, color: "#f1637e" }}>
            {format(props.initialValue.payed)}
            {" ₽"}
          </span>
        </span>
      </Fragment>
    ) : null}
  </div>
)

export default TotalSumm
