import React, { Component } from "react"
import moment from "moment"
import { Form, FormGroup, ControlLabel, Button } from "react-bootstrap"
import { SingleDatePicker } from "react-dates"
import TextField from "../Common/Textfield"
import history from "../../Modules/helpers/history"
import LoadingModal from "../Common/LoadingModal"

const SettingsBlock = props => (
  <div
    style={{
      backgroundColor: "#eee",
      width: 500,
      margin: "50px auto",
      padding: 30,
      borderRadius: 5
    }}
  >
    {props.children}
  </div>
)

const ReturnToday = props => (
  <span
    onClick={props.clearToday}
    style={{
      display: "block",
      color: "#337ab7",
      cursor: "pointer",
      marginBottom: 15
    }}
  >
    Вернуть сегодняшнюю дату сетки
  </span>
)

class Settings extends Component {
  state = {
    daysInFuture: "",
    daysInPast: "",
    today: null,
    datePickerFocused: false
  }

  componentDidMount = () => {
    this.setState({
      daysInFuture: this.props.daysInFuture,
      daysInPast: this.props.daysInPast,
      today: moment(this.props.today)
    })
  }

  componentDidUpdate = prevProps => {
    if (prevProps !== this.props)
      this.setState({
        daysInFuture: this.props.daysInFuture,
        daysInPast: this.props.daysInPast,
        today: moment(this.props.today)
      })
  }

  changeDaysInFuture = daysInFuture => this.setState({ daysInFuture })
  changeDaysInPast = daysInPast => this.setState({ daysInPast })
  changeToday = today => this.setState({ today })
  clearToday = () => this.setState({ today: moment() })

  changeFocus = datePickerFocused => this.setState({ datePickerFocused })

  handleSubmit = e => {
    e.preventDefault()
    this.props.updateSettings(this.state.daysInFuture, this.state.daysInPast)

    const formattedNewToday = this.state.today.format("YYYY-MM-DD")
    if (formattedNewToday !== moment().format("YYYY-MM-DD")) {
      history.push(`/?gridToday=${formattedNewToday}`)
      return
    }
    history.push("/")
  }

  render() {
    if (this.props.isLoading) return <LoadingModal isOpen={true} />

    return (
      <SettingsBlock>
        <Form onSubmit={this.handleSubmit}>
          <TextField
            name="Дней в будущее в сетке"
            value={this.state.daysInFuture}
            onChange={this.changeDaysInFuture}
            style={{ marginBottom: 15 }}
          />
          <TextField
            name="Дней в прошлое в сетке"
            value={this.state.daysInPast}
            onChange={this.changeDaysInPast}
            style={{ marginBottom: 15 }}
          />
          <FormGroup style={{ marginBottom: 15 }}>
            <ControlLabel style={{ display: "block", marginBottom: 5 }}>
              Текущая дата сетки
            </ControlLabel>
            <SingleDatePicker
              id="settings-dp"
              date={this.state.today}
              onDateChange={this.changeToday}
              focused={this.state.datePickerFocused}
              onFocusChange={({ focused }) => this.changeFocus(focused)}
              placeholder="Дата"
            />
          </FormGroup>
          <ReturnToday clearToday={this.clearToday} />
          <Button bsStyle="success" type="submit" style={{ width: "100%" }}>
            Сохранить
          </Button>
        </Form>
      </SettingsBlock>
    )
  }
}

export default Settings
