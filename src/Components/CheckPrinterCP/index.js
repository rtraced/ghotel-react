import React, { Component } from "react"
import styled from "styled-components"
import { Button } from "react-bootstrap"
import PrintServerErrorModal from "../Common/PrintServerErrorModal"
import CheckPrintController from "../../Modules/CheckPrintController"
import isServerRunning from "../../Modules/CheckPrintController/isServerRunning"
import LoadingModal from "../Common/LoadingModal"
import getCashAmount from "../../Modules/CheckPrintController/sellCheck/getCashAmount"

const CheckPrinterCPBlock = styled.div`
  background-color: #eee;
  width: 320px;
  margin: 50px auto;
  padding: 30px;
  border-radius: 5px;
`

const BlockButton = styled(Button)`
  display: block;
  width: 100%;
  margin-bottom: 20px;

  &:last-child {
    margin-bottom: 0;
  }
`

class CheckPrinterCP extends Component {
  constructor(props) {
    super(props)

    this.CheckPrintControl = new CheckPrintController()
  }

  state = {
    isLoading: false,
    isServerOFFModalActive: false
  }

  startLoading = () =>
    this.setState({
      isLoading: true
    })

  stopLoading = () =>
    this.setState({
      isLoading: false
    })

  showServerOFFModal = () =>
    this.setState({
      isServerOFFModalActive: true
    })

  hideServerOFFModal = () =>
    this.setState({
      isServerOFFModalActive: false
    })

  inputSumm = options => {
    const { badge, caption, maxValue, maxValueErrorText } = options
    let summ = 0

    // Request to input the summ until it wouldn't be in correct format
    while (summ === 0) {
      const value = window.prompt(
        `Сумма ${badge}:${caption ? `\n\n${caption}` : ""}`
      )

      // Alert was canceled
      if (value === null) break

      summ = parseFloat(value)
      if (isNaN(summ)) {
        // Received value is not a number
        window.alert(
          "Неверный формат. \nВведите целое число или число с 2 знаками после точки."
        )
        summ = 0
      } else if (maxValue && summ > maxValue) {
        window.alert(
          maxValueErrorText || "Введенная сумма больше допустимого значения."
        )
        summ = 0
      }
    }

    return summ
  }

  doAction = async action => {
    const isServerOK = await isServerRunning()
    if (isServerOK) {
      action()
    } else {
      this.showServerOFFModal()
    }
  }

  alertResult = result => {
    setTimeout(() => {
      if (result.Error) {
        window.alert(`Ошибка: \n\n${result.Error}`)
      } else {
        window.alert("Команда успешно выполнена")
      }
    }, 300)
  }

  getXReport = () =>
    this.doAction(async () => {
      this.startLoading()
      const result = await this.CheckPrintControl.getXReport()
      this.stopLoading()
      this.alertResult(result)
    })

  paymentCash = () =>
    this.doAction(async () => {
      this.startLoading()
      const state = await this.CheckPrintControl.getKKTState()
      const maxSumm = state.Info && state.Info.BalanceCash

      const summ = this.inputSumm({
        badge: "наличных для снятия",
        caption: `Текущее количество наличных в кассе: ${maxSumm}`,
        maxValue: maxSumm,
        maxValueErrorText: "Введенная сумма больше количества денег в кассе."
      })

      if (!summ) {
        this.stopLoading()
        return
      }

      const result = await this.CheckPrintControl.paymentCash(summ)
      this.stopLoading()
      this.alertResult(result)
    })

  depositCash = () =>
    this.doAction(async () => {
      this.startLoading()

      setTimeout(async () => {
        const summ = this.inputSumm({
          badge: "вносимых наличных"
        })

        if (!summ) {
          this.stopLoading()
          return
        }

        const result = await this.CheckPrintControl.depositCash(summ)
        this.stopLoading()
        this.alertResult(result)
      }, 300)
    })

  payByCash = () =>
    this.doAction(async () => {
      this.startLoading()

      setTimeout(async () => {
        const summ = this.inputSumm({
          badge: "продажи наличными"
        })

        if (!summ) {
          this.stopLoading()
          return
        }

        const result = await this.CheckPrintControl.printSellCheck({
          lines: [
            {
              Register: {
                Name: "Услуги проживания",
                Quantity: 1,
                Price: summ,
                Amount: summ,
                Department: 0,
                Tax: -1,
                MeasurementUnit: "шт"
              }
            }
          ],
          values: {
            cashSumm: summ,
            cardSumm: 0,
            payedByCash: true,
            payedByCard: false,
            isNoRefund: false,
            cashReceivedFromClient: getCashAmount(summ),
            areValuesCorrect: true
          }
        })
        this.stopLoading()
        this.alertResult(result)
      })
    })

  payByCard = () =>
    this.doAction(async () => {
      this.startLoading()

      setTimeout(async () => {
        const summ = this.inputSumm({
          badge: "безналичной оплаты"
        })

        if (!summ) {
          this.stopLoading()
          return
        }

        const result = await this.CheckPrintControl.printSellCheck({
          lines: [
            {
              Register: {
                Name: "Услуги проживания",
                Quantity: 1,
                Price: summ,
                Amount: summ,
                Department: 0,
                Tax: -1,
                MeasurementUnit: "шт"
              }
            }
          ],
          values: {
            cashSumm: 0,
            cardSumm: summ,
            payedByCash: false,
            payedByCard: true,
            isNoRefund: false,
            cashReceivedFromClient: 0,
            areValuesCorrect: true
          }
        })
        this.stopLoading()
        this.alertResult(result)
      })
    })

  getDailyCardsTotals = () =>
    this.doAction(async () => {
      this.startLoading()
      const result = await this.CheckPrintControl.getDailyCardsTotals()
      this.stopLoading()
      this.alertResult(result)
    })

  openShift = () =>
    this.doAction(async () => {
      this.startLoading()
      const result = await this.CheckPrintControl.openShift()
      this.stopLoading()
      this.alertResult(result)
    })

  closeShift = () =>
    this.doAction(async () => {
      this.startLoading()
      const confirmation = window.confirm(
        "Будет произведено закрытие смены. Данная операция выполняется один раз в сутки с 23:30 до 23:59. Продолжить?"
      )
      if (confirmation) {
        const result = await this.CheckPrintControl.closeShift()
        this.stopLoading()
        this.alertResult(result)
      } else {
        this.stopLoading()
      }
    })

  render() {
    const { isLoading, isServerOFFModalActive } = this.state

    if (isLoading) return <LoadingModal isOpen={true} />

    return (
      <CheckPrinterCPBlock>
        <BlockButton bsStyle="primary" onClick={this.openShift}>
          Открыть смену
        </BlockButton>
        <BlockButton bsStyle="primary" onClick={this.closeShift}>
          Закрыть смену
        </BlockButton>
        <BlockButton bsStyle="primary" onClick={this.getXReport}>
          X-Отчет
        </BlockButton>
        <BlockButton bsStyle="primary" onClick={this.getDailyCardsTotals}>
          Получить итоги дня по картам
        </BlockButton>
        <BlockButton bsStyle="primary" onClick={this.paymentCash}>
          Инкассация
        </BlockButton>
        <BlockButton bsStyle="primary" onClick={this.depositCash}>
          Внесение
        </BlockButton>
        <BlockButton bsStyle="primary" onClick={this.payByCash}>
          Продажа наличными
        </BlockButton>
        <BlockButton bsStyle="primary" onClick={this.payByCard}>
          Безналичная оплата
        </BlockButton>
        <PrintServerErrorModal
          isOpen={isServerOFFModalActive}
          hideModal={this.hideServerOFFModal}
        />
      </CheckPrinterCPBlock>
    )
  }
}

export default CheckPrinterCP
