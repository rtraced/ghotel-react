import React, { Component } from "react"
import { FormGroup, ControlLabel } from "react-bootstrap"
import "react-dates/initialize"
import { SingleDatePicker } from "react-dates"
import "react-dates/lib/css/_datepicker.css"
import moment from "moment"

class DatePicker extends Component {
  state = {
    focusedInput: null
  }

  isInclusivelyAfterDay(day, lastDay) {
    if (moment(day).isAfter(lastDay)) {
      return true
    } else {
      return false
    }
  }

  render() {
    return (
      <FormGroup controlId="formBasicText">
        {this.props.name ? (
          <ControlLabel style={{ display: "block" }}>
            {this.props.name}
          </ControlLabel>
        ) : null}
        <SingleDatePicker
          date={this.props.date} // momentPropTypes.momentObj or null
          onDateChange={date => this.props.onDateChange(date)} // PropTypes.func.isRequired
          focused={this.state.focused} // PropTypes.bool
          onFocusChange={({ focused }) => this.setState({ focused })} // PropTypes.func.isRequired
          id="your_unique_id" // PropTypes.string.isRequired
          placeholder="Дата"
        />
      </FormGroup>
    )
  }
}

export default DatePicker
