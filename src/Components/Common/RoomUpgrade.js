import React, { Fragment } from "react"
import moment from "moment"
import {
  Grid,
  Row,
  Col,
  Button,
  Table,
  FormGroup,
  FormControl
} from "react-bootstrap"
import debounce from "lodash.debounce"
import Select from "./Select"
import SelectNoLabel from "./SelectNoLabel"
import Textfield from "./Textfield"
import paymentMethod from "../../Modules/helpers/paymentMethods"
import StyledErrorMessage from "./StyledErrorMessage"

const UpgradeRow = props => (
  <tr>
    <td>
      <SelectNoLabel
        values={props.days.map(d => ({
          text: moment(d).format("DD MMMM"),
          value: d
        }))}
        bsSize="small"
        value={props.upgrade.date}
        disabled={props.upgrade.payed}
        validationState={
          props.errorFields.find(i => i === `upgrade-${props.upgrade.id}-date`)
            ? "error"
            : null
        }
        onChange={v => {
          if (!props.availableDays.includes(v) && v) {
            props.setUpgradeError("Нельзя создать 2 улучшения в один день")
            return
          } else {
            props.clearUpgradeError()
          }
          props.onUpgradeDateChange(v, props.upgrade.id)
        }}
      />
    </td>
    <td>
      <FormGroup
        validationState={
          props.errorFields.find(i => i === `upgrade-${props.upgrade.id}-price`)
            ? "error"
            : null
        }
        style={{ marginBottom: 0 }}
      >
        <FormControl
          defaultValue={props.upgrade.price}
          placeholder="Стоимость"
          type="number"
          bsSize="small"
          disabled={props.upgrade.payed}
          onChange={v =>
            props.onUpgradePriceChange(v.target.value, props.upgrade.id)
          }
        />
      </FormGroup>
    </td>
    <td>
      <SelectNoLabel
        values={paymentMethod.map(p => ({
          text: p,
          value: p
        }))}
        bsSize="small"
        value={props.upgrade.payment_type}
        disabled={props.upgrade.payed}
        validationState={
          props.errorFields.find(
            i => i === `upgrade-${props.upgrade.id}-payment_type`
          )
            ? "error"
            : null
        }
        onChange={v => props.onUpgradePaymentChange(v, props.upgrade.id)}
      />
    </td>
    <td style={{ textAlign: "right" }}>
      {props.modes.sale || props.modes.create ? (
        <Button
          onClick={() => props.onUpgradeCopy(props.upgrade.id)}
          bsStyle="success"
          bsSize="small"
        >
          Копировать
        </Button>
      ) : null}
      {!props.upgrade.payed ? (
        <Button
          onClick={() => props.onUpgradeDelete(props.upgrade.id)}
          bsStyle="danger"
          bsSize="small"
          style={{ marginLeft: 10 }}
        >
          Удалить
        </Button>
      ) : null}
      {props.modes.refund ? (
        <Button
          onClick={() => props.onUpgradeRefund(props.upgrade.id)}
          bsStyle="danger"
          bsSize="small"
          style={{ marginLeft: 10 }}
        >
          Возврат
        </Button>
      ) : null}
    </td>
  </tr>
)

const RoomUpgrade = props => {
  return (
    <Fragment>
      <Grid>
        <Row>
          <Col xs={12} md={12}>
            {props.createdUpgrades.length > 0 ? (
              <Table
                responsive
                className="table-hover condensed"
                style={{ width: "100%" }}
              >
                <thead>
                  <tr>
                    <th style={{ width: 160 }}>Дата проживания</th>
                    <th style={{ width: 80 }}>Стоимость</th>
                    <th style={{ width: 180 }}>Способ оплаты</th>
                  </tr>
                </thead>
                <tbody>
                  {props.createdUpgrades.map(upgrade => (
                    <UpgradeRow
                      {...props}
                      upgrade={upgrade}
                      key={upgrade.id}
                      errorFields={props.errorFields}
                    />
                  ))}
                </tbody>
              </Table>
            ) : null}
          </Col>
        </Row>
      </Grid>
      {props.modes.sale || props.modes.create ? (
        <Fragment>
          <Row style={{ marginBottom: 15 }}>
            <Col md={3} xs={3}>
              <Select
                name="Дата проживания"
                value={props.newUpgrade.date}
                onChange={v => props.onDateChange(v)}
                values={props.availableDays.map(d => ({
                  text: moment(d).format("DD MMMM"),
                  value: d
                }))}
              />
            </Col>
            <Col md={3} xs={3}>
              <Textfield
                defaultValue={props.newUpgrade.price}
                onChange={debounce(props.onPriceChange, 300)}
                placeholder="Стоимость улучшения"
                name="Стоимость"
                type="number"
              />
            </Col>
            <Col md={3} xs={3}>
              <Select
                name="Оплата"
                value={props.newUpgrade.payment_type}
                onChange={v => props.onPaymentChange(v)}
                values={paymentMethod.map(m => ({ value: m, text: m }))}
              />
            </Col>
          </Row>
          <Grid fluid={true}>
            <Row style={{ display: "flex" }}>
              <Button bsStyle="success" onClick={() => props.onCreate()}>
                Добавить улучшение
              </Button>
              {props.upgradesError ? (
                <StyledErrorMessage>{props.upgradesError}</StyledErrorMessage>
              ) : null}
            </Row>
          </Grid>
        </Fragment>
      ) : null}
    </Fragment>
  )
}

export default RoomUpgrade
