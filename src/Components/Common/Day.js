import React from "react"
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Checkbox,
  Button,
  Glyphicon
} from "react-bootstrap"
import TextField from "./Textfield"
import Select from "./Select"
import moment from "moment"
import "moment/locale/ru"
import paymentMethods from "../../Modules/helpers/paymentMethods"

moment.locale("ru")

const PayedCheckbox = props => {
  return (
    <FormGroup>
      <ControlLabel>{props.title}</ControlLabel>
      <label
        style={{
          width: 34,
          height: 34,
          backgroundColor: props.disabled
            ? "#eee"
            : props.value
              ? "#5cb85c"
              : "#fff",
          border: "1px solid",
          borderColor: props.value ? "#4cae4c" : "#ced4da",
          borderRadius: 5,
          display: "flex",
          cursor: !props.disabled && "pointer"
        }}
      >
        <Glyphicon
          glyph="ok"
          style={{
            margin: "auto",
            fontSize: 18,
            color: props.disabled ? "#eee" : "#fff",
            transform: "translate(-1px, -1px)"
          }}
        />
        <Checkbox
          value={props.value}
          onChange={() => props.onChange(!props.value)}
          style={{ display: "none" }}
          disabled={props.disabled}
        />
      </label>
    </FormGroup>
  )
}

const Day = props => {
  return (
    <Grid fluid={true}>
      <Row>
        <Col xs={3}>
          <div style={{ marginTop: 30 }}>
            <h4 style={{ fontSize: 20 }}>
              {moment(props.day.date).format("DD MMMM")}
            </h4>
          </div>
        </Col>
        <Col xs={2}>
          <TextField
            onChange={value => props.onDayPriceChange(value)}
            value={props.price}
            validationState={
              props.errorFields.includes(`day-${props.day.id}-price`)
                ? "error"
                : null
            }
            name="Стоимость"
            placeholder="Стоимость"
            type="number"
            disabled={props.disabled}
          />
        </Col>
        <Col xs={3}>
          <Select
            onChange={value => props.onDayPaymentChange(value)}
            value={props.method}
            validationState={
              props.errorFields.includes(`day-${props.day.id}-payment_type`)
                ? "error"
                : null
            }
            values={paymentMethods.map(p => ({ value: p, text: p }))}
            name="Метод оплаты"
            disabled={props.disabled}
          />
        </Col>
        <Col xs={2}>
          <PayedCheckbox
            onChange={
              props.modes.create
                ? value => {
                  if (props.notPayedConfirmed) {
                    return props.onDayPayedChange(value)
                  } else {
                    if (
                      window.confirm(
                        "Вы действительно хотите создать бронь без оплаты?"
                      )
                    ) {
                      props.confirmNotPayed()
                      return props.onDayPayedChange(value)
                    }
                  }
                }
                : value => props.onDayPayedChange(value)
            }
            value={props.payed}
            style={{ marginTop: 10 }}
            disabled={props.disabled}
            title="Оплачен"
          />
        </Col>
        {props.modes.refund && props.isLast && (
          <Col xs={2}>
            <Button
              bsStyle="danger"
              bsSize="small"
              style={{ marginTop: 27 }}
              onClick={() => props.dayRefund(props.day.id)}
            >
              Возврат
            </Button>
          </Col>
        )}
      </Row>
    </Grid>
  )
}

export default Day
