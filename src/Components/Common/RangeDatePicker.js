import React, { Component } from "react"
import { FormGroup, ControlLabel } from "react-bootstrap"
import "react-dates/initialize"
import { DateRangePicker } from "react-dates"
import "react-dates/lib/css/_datepicker.css"
import moment from "moment"

class RangeDatePicker extends Component {
  state = {
    focusedInput: null,
    startDate: null,
    endDate: null
  }

  isInclusivelyAfterDay = (day, lastDay) => {
    return moment(day).isAfter(moment(lastDay))
  }

  render() {
    return (
      <FormGroup controlId="formBasicText">
        <ControlLabel style={{ display: "block", letterSpacing: 0.5 }}>
          {this.props.name}
        </ControlLabel>
        <DateRangePicker
          startDate={this.props.startDate ? moment(this.props.startDate) : null} // momentPropTypes.momentObj or null,
          endDate={this.props.endDate ? moment(this.props.endDate) : null} // momentPropTypes.momentObj or null,
          startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
          endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
          disabled={this.props.disabled ? this.props.disabled : false}
          startDatePlaceholderText="Заезд"
          endDatePlaceholderText="Выезд"
          onDatesChange={({ startDate, endDate }) =>
            this.props.onDatesChange(startDate, endDate)
          } // PropTypes.func.isRequired,
          focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
          onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
          isOutsideRange={
            this.props.isOutsideRange
              ? day =>
                !this.isInclusivelyAfterDay(day, this.props.isOutsideRange)
              : () => false
          }
          isDayBlocked={
            !this.props.isOutsideRange
              ? day =>
                day.format("YYYY-MM-DD") === this.props.startDate ||
                  day.format("YYYY-MM-DD") === this.props.endDate
              : () => false
          }
        />
      </FormGroup>
    )
  }
}

export default RangeDatePicker
