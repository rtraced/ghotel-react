import React from "react"
import ErrorMessage from "./ErrorMessage"

const StyledErrorMessage = props => (
  <ErrorMessage
    style={{
      marginBottom: 0,
      marginLeft: 15,
      padding: "6px 12px"
    }}
  >
    {props.children}
  </ErrorMessage>
)

export default StyledErrorMessage
