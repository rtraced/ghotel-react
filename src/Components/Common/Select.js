import React from "react"
import { FormGroup, ControlLabel, FormControl } from "react-bootstrap"
import uuid from "uuid/v4"

const Select = props => {
  return (
    <FormGroup
      controlId="formControlsSelect"
      style={props.style || { marginBottom: 0 }}
      validationState={props.validationState}
    >
      <ControlLabel>{props.name}</ControlLabel>
      <FormControl
        componentClass="select"
        placeholder="select"
        value={props.value}
        onChange={e => props.onChange(e.target.value)}
        style={{ cursor: props.disabled ? null : "pointer" }}
        disabled={props.disabled}
        bsSize={props.bsSize}
      >
        <option value="">Выберите</option>
        {props.values.map(value => (
          <option key={uuid()} value={value.value}>
            {value.text}
          </option>
        ))}
      </FormControl>
    </FormGroup>
  )
}

export default Select
