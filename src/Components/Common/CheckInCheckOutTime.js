import React, { Component } from "react"
import debounce from "lodash.debounce"
import TimeField from "react-simple-timefield"
import Select from "./Select"
import { Button } from "react-bootstrap"
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Grid,
  Row,
  Col
} from "react-bootstrap"
import paymentMethods from "../../Modules/helpers/paymentMethods"

const PriceBlock = props => (
  <FormGroup style={{ marginBottom: 0 }}>
    <ControlLabel style={{ marginBottom: 0 }}>Стоимость</ControlLabel>
    <FormControl.Static style={{ marginBottom: 0, paddingBottom: 0 }}>
      {props.price}
    </FormControl.Static>
  </FormGroup>
)

class Time extends Component {
  state = {
    value: null
  }

  render() {
    const { modes } = this.props

    return (
      <Grid fluid={true}>
        <Row style={{ marginBottom: 15 }}>
          <Col xs={3} md={3}>
            <FormGroup style={{ marginBottom: 0 }}>
              <ControlLabel>{this.props.name}</ControlLabel>
              <TimeField
                value={this.props.time}
                onChange={debounce(this.props.onChange, 300)}
                input={<FormControl />}
                disabled={this.props.disabled || modes.refund}
              />
            </FormGroup>
          </Col>
          <Col xs={3} md={3}>
            <Select
              value={this.props.payment}
              name={"Метод оплаты"}
              onChange={v => this.props.onPaymentChange(v)}
              values={paymentMethods.map(m => ({ text: m, value: m }))}
              disabled={this.props.disabled || modes.refund}
              validationState={
                this.props.errorFields.includes(
                  `${this.props.type}-payment_type`
                )
                  ? "error"
                  : null
              }
            />
          </Col>
          {this.props.price ? (
            <Col xs={2} md={2}>
              <PriceBlock price={this.props.price} />
            </Col>
          ) : null}
          {this.props.price && modes.refund ? (
            <Col xs={2} md={2}>
              <Button
                bsStyle="danger"
                style={{ marginTop: 25 }}
                onClick={this.props.onRefund}
              >
                Возврат
              </Button>
            </Col>
          ) : null}
        </Row>
      </Grid>
    )
  }
}

export default Time
