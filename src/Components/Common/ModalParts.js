import styled from "styled-components"
import { Button } from "react-bootstrap"
import ButtonWithLoading from "react-bootstrap-button-loader"

export const ModalTitle = styled.h2`
  font-size: 18px;
  font-weight: 600;
  margin: 0;
  padding: 20px;
`

export const ModalContent = styled.div`
  padding: 15px 20px;
`

export const ModalHR = styled.hr`
  margin: 0;
  border-color: #e2e2e2;
`

export const ModalControls = styled.div`
  display: flex;
  justify-content: flex-start;
  padding: 15px;
  padding-left: 5px;
`

export const ModalButton = styled(Button)`
  margin-left: 10px;
`

export const ModalButtonWithLoading = styled(ButtonWithLoading)`
  margin-left: 10px;
  padding-left: ${props => (props.loading ? 0 : "12px")};
`

export const ModalSuccessMessage = styled.p`
  font-size: 12px;
  color: #5cb85c;
  padding: 10px 20px;
  margin: 0;
  margin-top: -10px;
`

export const ModalError = styled.p`
  font-size: 12px;
  color: #d9534f;
  padding: 10px 20px;
  margin: 0;
  margin-top: -10px;
`

export const ModalTextBlock = styled.div`
  padding: 15px 20px;
`
