import React from "react"
import styled from "styled-components"
import { Table } from "react-bootstrap"
import moment from "moment"
import history from "../../Modules/helpers/history"

const getRooms = reservation => {
  const rooms = []
  reservation.reserved_days.forEach(d => {
    if (!rooms.includes(d.room.room_id)) rooms.push(d.room.room_id)
  })
  return rooms.join(", ")
}

const calculateTotal = reservation => {
  let total = 0
  for (let i = 0; i < reservation.reserved_days.length; i++) {
    total = total + parseFloat(reservation.reserved_days[i].price)
  }

  for (let i = 0; i < reservation.additional_services.length; i++) {
    total = total + parseFloat(reservation.additional_services[i].price)
  }

  return Math.round(total)
}

const RTD = styled.td`
  vertical-align: middle;
  padding: 16px 8px;
  width: ${props => props.width}px;
  text-align: ${props => props.textAlign};
`

const ReservationRow = ({ reservation }) => (
  <tr
    style={{ cursor: "pointer" }}
    onClick={() => {
      history.push("/reservation?pk=" + reservation.pk)
    }}
  >
    <RTD width={100}>{reservation.booking_number}</RTD>
    <RTD width={180}>{reservation.guest_name}</RTD>
    <RTD width={100}>{getRooms(reservation)}</RTD>
    <RTD width={200}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <span>{moment(reservation.start).format("DD MMMM")}</span>
        <span>==></span>
        <span>{moment(reservation.end).format("DD MMMM")}</span>
      </div>
    </RTD>
    <RTD width={80} textAlign="right">
      {calculateTotal(reservation)} ₽
    </RTD>
  </tr>
)

const TypeHint = styled.span`
  background-color: #2caf41;
  color: #fff;
  text-align: center;
  padding: 15px;
  display: block;
`

const Error = styled.span`
  background-color: #e64646;
  color: #fff;
  text-align: center;
  padding: 15px;
  display: block;
`

const SearchResults = ({ reservations, hintText }) => {
  if (!reservations) {
    return <TypeHint>{hintText}</TypeHint>
  }

  if (!reservations.length) {
    return <Error>Ничего не найдено</Error>
  }

  const alreadyCreated = reservations.filter(r => r.isReady)

  const sortedByDate = alreadyCreated.sort(
    (a, b) =>
      moment(a.start).isAfter(moment(b.start))
        ? 1
        : moment(a.start).isBefore(moment(b.start))
          ? -1
          : 0
  )

  const rows = sortedByDate.map(r => (
    <ReservationRow reservation={r} key={r.pk} />
  ))

  return (
    <Table
      responsive
      className="table-hover condensed"
      style={{ width: "100%" }}
    >
      <thead>
        <tr>
          <th>Номер брони</th>
          <th>Гость</th>
          <th>Комната</th>
          <th>Даты</th>
          <th style={{ textAlign: "right" }}>Цена</th>
        </tr>
      </thead>
      <tbody>{rows}</tbody>
    </Table>
  )
}

export default SearchResults
