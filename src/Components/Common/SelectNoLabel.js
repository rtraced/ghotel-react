import React from "react"
import { FormGroup, FormControl } from "react-bootstrap"
import uuid from "uuid/v4"

const TextField = props => {
  return (
    <FormGroup
      controlId="formControlsSelect"
      style={{ marginBottom: 0 }}
      validationState={props.validationState}
    >
      <FormControl
        style={props.style}
        componentClass="select"
        placeholder="select"
        value={props.value}
        disabled={props.disabled}
        onChange={e => props.onChange(e.target.value)}
        bsSize={props.bsSize}
      >
        <option value="">Выберите</option>
        {props.values.map(value => (
          <option key={uuid()} value={value.value}>
            {value.text}
          </option>
        ))}
      </FormControl>
    </FormGroup>
  )
}

export default TextField
