import React, { Fragment } from "react"
import {
  Grid,
  Row,
  Col,
  Button,
  Table,
  FormGroup,
  FormControl
} from "react-bootstrap"
import Select from "./Select"
import SelectNoLabel from "./SelectNoLabel"
import moment from "moment"
import qty from "../../Modules/helpers/qty"
import paymentMethod from "../../Modules/helpers/paymentMethods"
import StyledErrorMessage from "./StyledErrorMessage"

moment.locale("ru")

const TDText = props => (
  <td style={{ fontSize: 14, verticalAlign: "middle" }}>{props.children}</td>
)

const ServiceRow = props => {
  return (
    <tr>
      <td>
        <SelectNoLabel
          values={props.days.map(d => ({
            text: moment(d).format("DD MMMM"),
            value: d
          }))}
          bsSize="small"
          value={props.service.date}
          disabled={props.service.payed}
          validationState={
            props.errorFields.find(
              i => i === `service-${props.service.id}-date`
            )
              ? "error"
              : null
          }
          onChange={v => props.onServiceDateChange(v, props.service.id)}
        />
      </td>
      <TDText>{props.service.service}</TDText>
      <TDText>{props.service.price}</TDText>
      <td>
        <FormGroup
          validationState={
            props.errorFields.find(
              i => i === `service-${props.service.id}-quantity`
            )
              ? "error"
              : null
          }
          style={{ marginBottom: 0 }}
        >
          <FormControl
            value={props.service.quantity}
            type={"number"}
            bsSize="small"
            disabled={props.service.payed}
            onChange={v =>
              props.onServiceQtyChange(v.target.value, props.service.id)
            }
          />
        </FormGroup>
      </td>
      <td>
        <SelectNoLabel
          values={paymentMethod.map(p => ({
            text: p,
            value: p
          }))}
          bsSize="small"
          value={props.service.payment_type}
          disabled={props.service.payed}
          validationState={
            props.errorFields.find(
              i => i === `service-${props.service.id}-payment_type`
            )
              ? "error"
              : null
          }
          onChange={v => props.onServicePaymentChange(v, props.service.id)}
        />
      </td>
      <td style={{ textAlign: "right" }}>
        {(props.modes.sale || props.modes.create) && (
          <Button
            onClick={() => props.onServiceCopy(props.service.id)}
            bsStyle="success"
            bsSize="small"
          >
            Копировать
          </Button>
        )}
        {!props.service.payed && (
          <Button
            onClick={() => props.onServiceDelete(props.service.id)}
            bsStyle="danger"
            bsSize="small"
            style={{ marginLeft: 10 }}
          >
            Удалить
          </Button>
        )}
        {props.modes.refund && (
          <Button
            onClick={() => props.onServiceRefund(props.service.id)}
            bsStyle="danger"
            bsSize="small"
            style={{ marginLeft: 10 }}
          >
            Возврат
          </Button>
        )}
      </td>
    </tr>
  )
}

const Service = props => {
  return (
    <Fragment>
      <Grid fluid={true}>
        <Row>
          <Col xs={12} md={12}>
            {props.createdServices.length ? (
              <Table
                responsive
                className="table-hover condensed"
                style={{ width: "100%" }}
              >
                <thead>
                  <tr>
                    <th>Дата проживания</th>
                    <th>Услуга</th>
                    <th>Стоимость</th>
                    <th style={{ width: 60 }}>Количество</th>
                    <th style={{ width: 130 }}>Способ оплаты</th>
                  </tr>
                </thead>
                <tbody>
                  {props.createdServices.map(service => (
                    <ServiceRow
                      {...props}
                      service={service}
                      key={service.id}
                      errorFields={props.errorFields}
                    />
                  ))}
                </tbody>
              </Table>
            ) : null}
          </Col>
        </Row>
      </Grid>
      {props.modes.sale || props.modes.create ? (
        <Fragment>
          <Row style={{ marginBottom: 15 }}>
            <Col md={3} xs={3}>
              <Select
                name="Дата проживания"
                value={props.service.date || ""}
                values={props.days.map(d => ({
                  text: moment(d).format("DD MMMM"),
                  value: d
                }))}
                onChange={value => props.onDateChange(value)}
              />
            </Col>
            <Col md={3} xs={3}>
              <Select
                name="Услуга"
                value={props.service.service}
                values={props.services.map(s => ({
                  text: s.name,
                  value: s.name
                }))}
                onChange={value => props.onServiceChange(value)}
              />
            </Col>
            <Col md={3} xs={3}>
              <Select
                name="Количество"
                value={props.service.quantity}
                values={qty.map(q => ({ value: q, text: q }))}
                onChange={value => props.onQtyChange(value)}
              />
            </Col>
            <Col md={3} xs={3}>
              <Select
                name="Оплата"
                value={props.service.payment_type}
                values={paymentMethod.map(m => ({ value: m, text: m }))}
                onChange={value => props.onPaymentChange(value)}
              />
            </Col>
          </Row>
          <Grid fluid={true}>
            <Row style={{ display: "flex" }}>
              <Button bsStyle="success" onClick={() => props.onCreate()}>
                Добавить услугу
              </Button>
              {props.servicesError ? (
                <StyledErrorMessage>{props.servicesError}</StyledErrorMessage>
              ) : null}
            </Row>
          </Grid>
        </Fragment>
      ) : null}
    </Fragment>
  )
}

export default Service
