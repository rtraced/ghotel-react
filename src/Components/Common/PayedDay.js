import React from "react"
import { Grid, Row, Col, Button } from "react-bootstrap"
import moment from "moment"
import "moment/locale/ru"

moment.locale("ru")

const StyledCol = props => {
  return (
    <Col {...props} style={{ fontSize: 18, marginBottom: 15 }}>
      {props.children}
    </Col>
  )
}

const Day = props => {
  return (
    <Grid fluid={true}>
      <Row>
        <StyledCol xs={3}>{moment(props.day.date).format("DD MMMM")}</StyledCol>
        <StyledCol xs={3}>
          {props.price}
          {" ₽"}
        </StyledCol>
        <StyledCol xs={3}>{props.method}</StyledCol>
        {props.modes.refund && props.isLast && (
          <StyledCol xs={3}>
            <Button
              bsStyle="danger"
              bsSize="small"
              onClick={() => props.dayRefund(props.day.id)}
              style={{ marginTop: -3 }}
            >
              Возврат
            </Button>
          </StyledCol>
        )}
      </Row>
    </Grid>
  )
}

export default Day
