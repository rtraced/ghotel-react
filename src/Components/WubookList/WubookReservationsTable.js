import React from "react"
import { Table } from "react-bootstrap"
import moment from "moment"
import history from "../../Modules/helpers/history"

const calculateTotal = reservation => {
  let total = 0
  for (let i = 0; i < reservation.reserved_days.length; i++) {
    total = total + parseFloat(reservation.reserved_days[i].price)
  }

  for (let i = 0; i < reservation.additional_services.length; i++) {
    total = total + parseFloat(reservation.additional_services[i].price)
  }

  return Math.round(total)
}

const Status = ({ reservation }) => {
  if (reservation.canceled) {
    return (
      <td
        style={{
          color: "red",
          verticalAlign: "middle",
          width: 70,
          textAlign: "right"
        }}
      >
        Отменена
      </td>
    )
  } else {
    return (
      <td
        style={{
          color: "green",
          verticalAlign: "middle",
          width: 70,
          textAlign: "right"
        }}
      >
        Подтверждена
      </td>
    )
  }
}

const RTD = props => (
  <td
    style={{
      verticalAlign: "middle",
      padding: "16px 8px",
      width: props.w,
      textAlign: props.textAlign || "left"
    }}
  >
    {props.children}
  </td>
)

const ReservationRow = ({ reservation }) => (
  <tr
    style={{ cursor: "pointer" }}
    onClick={() => {
      history.push("/reservation?pk=" + reservation.pk)
    }}
  >
    <RTD w={100}>{reservation.booking_number}</RTD>
    <RTD w={180}>{reservation.guest_name}</RTD>
    <RTD w={200}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <span>{moment(reservation.start).format("DD MMMM")}</span>
        <span>==></span>
        <span>{moment(reservation.end).format("DD MMMM")}</span>
      </div>
    </RTD>
    <RTD w={80} textAlign="right">
      {calculateTotal(reservation)} ₽
    </RTD>
    <Status reservation={reservation} />
  </tr>
)

const DateRow = ({ date }) => (
  <tr>
    <td
      style={{
        textAlign: "center",
        padding: 15,
        backgroundColor: "#444",
        color: "#fff",
        letterSpacing: 0.5
      }}
      colSpan={5}
    >
      Брони с заездом {moment(date).format("DD MMMM")}
    </td>
  </tr>
)

const MReservationHeader = () => (
  <tr>
    <td
      style={{
        fontWeight: 700,
        textAlign: "center",
        padding: 10
      }}
      colSpan={5}
    >
      Групповая бронь
    </td>
  </tr>
)

const MReservationFooter = ({ price }) => (
  <tr>
    <td
      style={{
        fontWeight: 700,
        textAlign: "center",
        padding: 10
      }}
      colSpan={5}
    >
      Общая стоимость групповой брони составляет {price} ₽
    </td>
  </tr>
)

const NotFoundError = () => (
  <span
    style={{
      backgroundColor: "#e64646",
      color: "#fff",
      textAlign: "center",
      padding: 15,
      display: "block"
    }}
  >
    Брони с этой датой заезда не найдены
  </span>
)

const WubookReservationsTable = ({ selectedDate, reservations }) => {
  let reservationsToDisplay
  if (selectedDate) {
    const formattedDate = selectedDate.format("YYYY-MM-DD")
    if (reservations[formattedDate]) {
      reservationsToDisplay = { [formattedDate]: reservations[formattedDate] }
    } else {
      reservationsToDisplay = null
    }
  } else {
    reservationsToDisplay = reservations
  }

  let rows
  if (!reservationsToDisplay) {
    return <NotFoundError />
  }

  rows = []
  let key = 0
  for (let date in reservationsToDisplay) {
    rows.push(<DateRow date={date} key={key++} />)

    const reservations = reservationsToDisplay[date]
    for (let bookingNumber in reservations) {
      const isCanceled =
        reservations[bookingNumber].length === 2 &&
        reservations[bookingNumber].some(r => r.canceled)
      let isMultiple = reservations[bookingNumber].length > 1 && !isCanceled

      if (isMultiple) rows.push(<MReservationHeader key={key++} />)

      let totalPrice = 0

      // eslint-disable-next-line
      reservations[bookingNumber].forEach(r => {
        totalPrice += calculateTotal(r)
        rows.push(<ReservationRow reservation={r} key={key++} />)
      })

      if (isMultiple)
        rows.push(<MReservationFooter price={totalPrice} key={key++} />)
    }
  }

  return (
    <Table
      responsive
      className="table-hover condensed"
      style={{ width: "100%" }}
    >
      <thead>
        <tr>
          <th>Номер брони</th>
          <th>Гость</th>
          <th>Даты</th>
          <th style={{ textAlign: "right" }}>Цена</th>
          <th style={{ textAlign: "right" }}>Статус</th>
        </tr>
      </thead>
      <tbody>{rows}</tbody>
    </Table>
  )
}

export default WubookReservationsTable
