import React, { Component } from "react"
import { FormGroup, ControlLabel, Button } from "react-bootstrap"
import { SingleDatePicker } from "react-dates"
import moment from "moment"
import { getWubookReservationsCall } from "../../Modules/api"
import LoadingModal from "../Common/LoadingModal"
import WubookReservationsTable from "./WubookReservationsTable"

const processReservations = reservations => {
  const groupedByDate = {}
  reservations.forEach(r => {
    if (!groupedByDate[r.start]) groupedByDate[r.start] = []
    if (!groupedByDate[r.start][r.booking_number])
      groupedByDate[r.start][r.booking_number] = []
    groupedByDate[r.start][r.booking_number].push(r)
  })

  const sortedByDate = {}
  Object.keys(groupedByDate)
    .sort(
      (a, b) =>
        moment(a).isAfter(moment(b))
          ? 1
          : moment(a).isBefore(moment(b))
            ? -1
            : 0
    )
    .forEach(key => {
      sortedByDate[key] = groupedByDate[key]
    })

  return sortedByDate
}

const WubookWarning = () => (
  <p style={{ margin: "0 0 15px", color: "crimson", textAlign: "center" }}>
    Нажмите на бронь для открытия карточки брони и дальнейшего заселения
  </p>
)

const WubookListBlock = props => (
  <div
    style={{
      backgroundColor: "#eee",
      width: 800,
      margin: "50px auto",
      padding: 30,
      borderRadius: 5
    }}
  >
    {props.children}
  </div>
)

class WubookList extends Component {
  state = {
    reservations: [],
    selectedDate: null,

    isLoading: false,
    datePickerFocused: false
  }

  setReservations = reservations => this.setState({ reservations })
  selectDate = selectedDate => this.setState({ selectedDate })
  clearDate = () => this.setState({ selectedDate: null })

  startLoading = () => this.setState({ isLoading: true })
  endLoading = () => this.setState({ isLoading: false })
  changeFocus = datePickerFocused => this.setState({ datePickerFocused })

  componentDidMount() {
    this.startLoading()
    getWubookReservationsCall().then(res => {
      this.setReservations(processReservations(res))
      this.endLoading()
    })
  }

  render() {
    if (this.state.isLoading) {
      return <LoadingModal isOpen={true} />
    }

    return (
      <WubookListBlock>
        <FormGroup style={{ marginBottom: 15 }}>
          <ControlLabel style={{ display: "block", marginBottom: 10 }}>
            Дата заезда
          </ControlLabel>
          <SingleDatePicker
            id="wubook-list-dp"
            date={this.state.selectedDate}
            onDateChange={date => this.selectDate(date)}
            focused={this.state.datePickerFocused}
            onFocusChange={({ focused }) => this.changeFocus(focused)}
            isOutsideRange={() => {}}
            placeholder="Дата"
          />
        </FormGroup>
        <Button
          bsStyle="warning"
          style={{ marginBottom: 15 }}
          onClick={this.clearDate}
        >
          Очистить дату
        </Button>
        <WubookWarning />
        <WubookReservationsTable
          selectedDate={this.state.selectedDate}
          reservations={this.state.reservations}
        />
      </WubookListBlock>
    )
  }
}

export default WubookList
