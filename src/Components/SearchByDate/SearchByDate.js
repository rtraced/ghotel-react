import React, { Component } from "react"
import { FormGroup, ControlLabel } from "react-bootstrap"
import { SingleDatePicker } from "react-dates"
import CheckBoxComponent from "../Common/Checkbox"
import SearchResults from "../Common/SearchResults"
import { searchByDateCall } from "../../Modules/api"

const SearchBlock = props => (
  <div
    style={{
      backgroundColor: "#eee",
      width: 800,
      margin: "50px auto",
      padding: 30,
      borderRadius: 5
    }}
  >
    {props.children}
  </div>
)

class SearchByDate extends Component {
  state = {
    date: null,
    isRefund: false,
    searchResult: null,
    datePickerFocused: false
  }

  setDate = date => {
    this.setState({ date })
    this.updateResult(date, this.state.isRefund)
  }

  setRefund = isRefund => {
    this.setState({ isRefund })
    this.updateResult(this.state.date, isRefund)
  }

  setResult = searchResult => this.setState({ searchResult })
  clearResult = () => this.setState({ searchResult: null })

  updateResult = async (date, isRefund) => {
    if (!date) return this.clearResult()

    const formattedDate = date.format("YYYY-MM-DD")

    this.setResult(await searchByDateCall(formattedDate, isRefund))
  }

  changeFocus = datePickerFocused => this.setState({ datePickerFocused })

  render() {
    return (
      <SearchBlock>
        <FormGroup style={{ marginBottom: 15 }}>
          <ControlLabel style={{ display: "block", marginBottom: 10 }}>
            Дата создания или редактирования брони
          </ControlLabel>
          <SingleDatePicker
            id="search-by-date-dp"
            date={this.state.date}
            onDateChange={this.setDate}
            focused={this.state.datePickerFocused}
            onFocusChange={({ focused }) => this.changeFocus(focused)}
            isOutsideRange={() => {}}
            placeholder="Дата"
          />
        </FormGroup>

        <CheckBoxComponent
          title={"Только невозвратные"}
          value={this.state.isRefund}
          onChange={this.setRefund}
          style={{ marginBottom: 15 }}
        />

        <SearchResults
          reservations={this.state.searchResult}
          hintText="Выберите дату для поиска"
        />
      </SearchBlock>
    )
  }
}

export default SearchByDate
