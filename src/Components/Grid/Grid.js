import React from "react"
import moment from "moment"
import Cookies from "js-cookie"
import classNames from "classnames"

import {
  onNewReservationClick,
  onReservationClick
} from "../../Modules/helpers/gridFunctions"

import "./styles.css"
import TableHead from "./components/TableHead"
import TableBody from "./components/TableBody"
import GridTooltip from "./components/GridTooltip"

const handlers = {
  filled: onReservationClick,
  empty: onNewReservationClick
}

class Grid extends React.Component {
  state = {
    // Grid Size
    size: +Cookies.get("grid-size") || 100,

    // User Selection
    selection: {
      isSelected: false,
      row: null,
      cells: {}
    }
  }

  setSize = size => {
    this.setState({ size })
    Cookies.set("grid-size", size)
  }

  set100Size = () => this.setSize(100)
  set80Size = () => this.setSize(80)
  set60Size = () => this.setSize(60)

  SizeControl = {
    set100Size: this.set100Size,
    set80Size: this.set80Size,
    set60Size: this.set60Size
  }

  render() {
    const { data, model, daysInPast, daysInFuture, today } = this.props

    // Calculate the first day in the table and the total number of days
    const daysCount = +daysInPast + 1 + +daysInFuture
    const firstDay = moment(today).subtract(daysInPast, "days")

    const gridClassNames = classNames({
      "r-grid": true,
      "s-100": this.state.size === 100,
      "s-80": this.state.size === 80,
      "s-60": this.state.size === 60
    })

    let multiplier
    switch (this.state.size) {
      case 100:
        multiplier = 50
        break

      case 80:
        multiplier = 44
        break

      case 60:
        multiplier = 38
        break

      default:
        multiplier = 50
        break
    }

    return (
      <div className={gridClassNames} onMouseMove={this.updateTooltipCoords}>
        <TableHead
          firstDate={firstDay}
          daysCount={daysCount}
          size={this.state.size}
          multiplier={multiplier}
          SizeControl={this.SizeControl}
        />
        <TableBody
          multiplier={multiplier}
          model={model}
          data={data}
          firstDate={firstDay}
          daysCount={daysCount}
          today={today}
          clickHandlers={handlers}
          activateTooltip={this.activateTooltip}
          disableTooltip={this.disableTooltip}
        />

        <GridTooltip />
      </div>
    )
  }
}

export default Grid
