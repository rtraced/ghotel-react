import React, { Component } from "react"
import BodyRow from "./BodyRow"
import changeDate from "../helpers/changeDate"
import isDayInRange from "../helpers/isDayInRange"

class TableBody extends Component {
  render() {
    const {
      model,
      data,
      firstDate,
      daysCount,
      today,
      clickHandlers
    } = this.props
    model.sort((a, b) => a.room_id - b.room_id)
    const rows = []

    // Create a row for each room.
    model.forEach((item, index) => {
      const { room_id: id, name } = item
      const days = data.rooms[id]
      const reservations = {}
      for (let day in days) {
        // Remove unnecessary items from the list
        if (
          !isDayInRange(day, firstDate, changeDate(firstDate, daysCount - 1))
        ) {
          delete days[day]
          continue
        }
        const reservation = days[day].reservation
        if (!reservations[reservation])
          reservations[reservation] = data.reservations[reservation]
      }

      rows.push(
        <BodyRow
          id={id}
          name={name}
          days={days}
          reservations={reservations}
          firstDate={firstDate}
          daysCount={daysCount}
          today={today}
          key={index}
          clickHandlers={clickHandlers}
          multiplier={this.props.multiplier}
        />
      )
    })

    return <div className="tbody">{rows}</div>
  }
}

export default TableBody
