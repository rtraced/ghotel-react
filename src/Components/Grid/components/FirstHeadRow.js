import React, { Component } from "react"
import WeekdayCell from "./WeekdayCell"
import changeDate from "../helpers/changeDate"

class FirstHeadRow extends Component {
  render() {
    const { firstDate, daysCount } = this.props
    const row = []

    let date = firstDate
    for (let i = 1; i <= daysCount; i++) {
      row.push(<WeekdayCell date={new Date(date)} key={i} />)
      date = changeDate(date, 1)
    }

    return <div className="thead-row">{row}</div>
  }
}

export default FirstHeadRow
