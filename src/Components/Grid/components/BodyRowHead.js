import React, { Component } from "react"

class BodyRowHead extends Component {
  render() {
    const { id, name } = this.props

    return (
      <div className="tbody-head">
        <div className="tbody-headcell room-id">{id}</div>
        <div className="tbody-headcell room-name">{name}</div>
      </div>
    )
  }
}

export default BodyRowHead
