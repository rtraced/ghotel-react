import React, { Fragment } from "react"
import ReactTooltip from "react-tooltip"
import styled from "styled-components"

const TooltipValueTitle = styled.h3`
  font-size: 14px;
  font-weight: bold;
  color: #444444;
  margin: 4px 0 6px 0;
`

const TooltipValueText = styled.p`
  font-size: 15px;
  margin-top: 5px;
  margin-bottom: 16px;
  color: #333333;

  &:last-child {
    margin-bottom: 0;
  }
`

const GridTooltip = props => (
  <ReactTooltip
    id="grid-tooltip"
    getContent={data => {
      const parsedData = JSON.parse(data)
      if (!data) return null
      return (
        <Fragment>
          <TooltipValueTitle>Номер брони</TooltipValueTitle>
          <TooltipValueText>{parsedData.bookingNumber}</TooltipValueText>
          {parsedData.clientName && (
            <Fragment>
              <TooltipValueTitle>Фамилия клиента</TooltipValueTitle>
              <TooltipValueText>{parsedData.clientName}</TooltipValueText>
            </Fragment>
          )}
          {parsedData.clientPhone && (
            <Fragment>
              <TooltipValueTitle>Телефон клиента</TooltipValueTitle>
              <TooltipValueText>{parsedData.clientPhone}</TooltipValueText>
            </Fragment>
          )}
          {parsedData.clientNote && (
            <Fragment>
              <TooltipValueTitle>Примечание</TooltipValueTitle>
              <TooltipValueText>{parsedData.clientNote}</TooltipValueText>
            </Fragment>
          )}
        </Fragment>
      )
    }}
    place="left"
  />
)

export default GridTooltip
