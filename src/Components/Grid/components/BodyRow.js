import React, { Component } from "react"
import BodyRowHead from "./BodyRowHead"
import BodyRowBody from "./BodyRowBody"
import styled from "styled-components"

const StyledBodyRow = styled.div`
  display: flex;
  width: ${({ daysCount, multiplier }) => 237 + daysCount * multiplier}px

  &:first-child {
    margin-top: -1px;
  }

  & > * {
    flex-shrink: 0;
  }
`

class BodyRow extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps !== this.props || nextState !== this.state
  }

  render() {
    const {
      id,
      name,
      data,
      days,
      reservations,
      firstDate,
      daysCount,
      today,
      clickHandlers,
      multiplier
    } = this.props

    return (
      <StyledBodyRow daysCount={daysCount} multiplier={multiplier}>
        <BodyRowHead id={id} name={name} />
        <BodyRowBody
          id={id}
          name={name}
          data={data}
          days={days}
          reservations={reservations}
          firstDate={firstDate}
          daysCount={daysCount}
          today={today}
          onMouseDown={this.handleMouseDown}
          onMouseUp={this.handleMouseUp}
          onMouseLeave={this.handleMouseOut}
          cellClickHandlers={clickHandlers}
        />
      </StyledBodyRow>
    )
  }
}

export default BodyRow
