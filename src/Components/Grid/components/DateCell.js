import React, { Component } from "react"

class DateCell extends Component {
  render() {
    const { date } = this.props

    const options = {
      year: "numeric",
      month: "short",
      day: "numeric"
    }

    return (
      <div className="thead-cell date">
        <div className="date-inner">
          {date.toLocaleString(
            "ru",
            options
          )}
        </div>
      </div>
    )
  }
}

export default DateCell
