import React, { Component } from "react"
import Emoji from "react-emoji-render"
import classNames from "classnames"
import formatDate from "../helpers/formatDate"
import getCurrentTime from "../helpers/getCurrentTime"

import changeDate from "../helpers/changeDate"

const icons = {
  Наличные: ":dollar:",
  Карта: ":credit_card:",
  "Безналичный расчет": ":bank:",
  "Примечание клиента": ":pushpin:",
  "Поздний выезд": ":alarm_clock:"
}

class BodyCell extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps !== this.props || nextState !== this.state
  }

  render() {
    const {
      roomID,
      isReserved,
      date,
      today,
      reservationData,
      cellData,
      clickHandler
    } = this.props

    // Use the properties of an empty cell
    // if it is not included in any of the reservations
    if (!isReserved) {
      const {
        startSelecting,
        stopSelecting,
        selectCell,
        cellID,
        selectionData
      } = this.props

      // Minimum check-in date
      const date14H = new Date(date)
      date14H.setHours(14)

      // Checking the possibility of making a reservation with this cell at the moment
      const dateRule =
        +new Date(date) >=
        +new Date(changeDate(formatDate(getCurrentTime()), -1))

      // Check if the cell is in the selected range
      let rule = false
      const { start, end } = selectionData
      if (start && end) {
        if (start > end) {
          rule = cellID <= start && cellID >= end
        } else if (start < end) {
          rule = cellID >= start && cellID <= end
        } else {
          rule = cellID === start
        }
      }

      const emptyCellClass = classNames({
        "tbody-cell": true,
        today: date === today,
        selected: rule && dateRule
      })

      /*
      * Allows to interact with the selection of a cell
      * only if it falls within the required time interval
      */
      return (
        <div
          className={emptyCellClass}
          onMouseDown={dateRule ? e => startSelecting(e, cellID, date) : null}
          onMouseUp={dateRule ? stopSelecting : null}
          onMouseEnter={dateRule ? () => selectCell(cellID, date) : null}
        />
      )
    }

    // Use the properties of an reserved cell
    const { clientNote, isCheckOutLate, start, end } = reservationData
    const { migratedFrom, migratedTo, paymentType } = cellData

    const isReservationOneCell =
      !isCheckOutLate && start === changeDate(end, -1)

    const className = classNames({
      "tbody-cell": true,
      combined: !isReservationOneCell,
      payed: reservationData.isPayed && !reservationData.isArchived,
      "not-payed":
        !reservationData.isPayed && !reservationData.isArchived && isReserved,
      archived: reservationData.isArchived,
      today: date === today && !isReserved,
      "has-refund": reservationData.hasRefund,
      start: !isReservationOneCell && date === start,
      end:
        !isReservationOneCell &&
        (isCheckOutLate ? date === end : date === changeDate(end, -1)),
      "m-to": !!migratedTo,
      "m-from": !!migratedFrom
    })

    /*
    * If there was a transition from or to another room,
    * assign the appropriate classes depending on the direction of migration
    */
    const leftMigratedClassNames = classNames({
      migrated: true,
      lt: migratedFrom && +migratedFrom < +roomID,
      lb: migratedFrom && +migratedFrom > +roomID
    })

    const rightMigratedClassNames = classNames({
      migrated: true,
      rt: migratedTo && +migratedTo < +roomID,
      rb: migratedTo && +migratedTo > +roomID
    })

    const tooltipData = {
      bookingNumber: reservationData.bookingNumber,
      clientName: reservationData.clientName,
      clientPhone: reservationData.clientPhone,
      clientNote: clientNote
    }

    return (
      <div
        className={className}
        onClick={clickHandler}
        data-for="grid-tooltip"
        data-tip={JSON.stringify(tooltipData)}
      >
        {/* Payment type icon */}
        {paymentType ? (
          <Emoji text={icons[paymentType]} className="grid-payment-icon" />
        ) : null}

        {/* Show when there is a client note. */}
        {clientNote ? (
          <div className="client-note">
            <Emoji
              text={icons["Примечание клиента"]}
              className="grid-additional-icon"
            />
          </div>
        ) : null}

        {/* Show when there is a late check-out. */}
        {isCheckOutLate ? (
          <div className="late-check-out">
            <Emoji
              text={icons["Поздний выезд"]}
              className="grid-additional-icon"
            />
          </div>
        ) : null}

        {/* 
         * Show if there is a transition from this room to another,
         * or from another to this one.
         */}
        {migratedFrom && <div className={leftMigratedClassNames} />}
        {migratedTo && <div className={rightMigratedClassNames} />}
      </div>
    )
  }
}

export default BodyCell
