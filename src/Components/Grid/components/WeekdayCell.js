import React, { Component } from "react"

class WeekdayCell extends Component {
  render() {
    const { date } = this.props

    const options = {
      weekday: "short"
    }

    return (
      <div className="thead-cell weekday">
        {date.toLocaleString(
          "ru",
          options
        )}
      </div>
    )
  }
}

export default WeekdayCell
