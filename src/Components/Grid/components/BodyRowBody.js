import React, { Component } from "react"
import BodyCell from "./BodyCell"
import changeDate from "../helpers/changeDate"

class BodyRowBody extends Component {
  constructor(props) {
    super(props)

    this.state = {
      mousePressed: false,
      startCell: null,
      endCell: null,
      startDay: null
    }
  }

  // Used at the beginning of holding the mouse button on an empty cell
  startSelecting = (e, cellID, date) => {
    // Check that the mouse button is left
    if (e.button === 0) {
      this.setState(prev => ({
        ...prev,
        mousePressed: true,
        startCell: cellID,
        endCell: cellID,
        startDay: date
      }))
    }
  }

  // Used when releasing the mouse button while selecting cells
  // or when the mouse moves from the row
  stopSelecting = () => {
    const { cellClickHandlers, name } = this.props
    const { startDay, startCell, endCell, mousePressed } = this.state
    if (mousePressed) {
      const { id } = this.props

      this.setState(prev => ({
        ...prev,
        mousePressed: false,
        startCell: null,
        endCell: null,
        startDay: null
      }))

      let daysCount =
        (endCell > startCell ? endCell - startCell : startCell - endCell) + 1

      cellClickHandlers.empty(id, name, startDay, daysCount)
    }
  }

  // Cell selection when mouse over it
  selectCell = (cellID, date) => {
    this.setState(prev => {
      if (prev.mousePressed) {
        const oDay = +new Date(prev.firstDay)
        const nDay = +new Date(date)

        return {
          ...prev,
          endCell: cellID,
          firstDay: nDay < oDay ? date : prev.firstDay
        }
      }
    })
  }

  render() {
    const {
      id,
      days,
      reservations,
      firstDate,
      daysCount,
      today,
      cellClickHandlers
    } = this.props
    const cells = []

    let reservation, day

    // Render cells for each day in a row
    for (let i = 1; i <= daysCount; i++) {
      const date = changeDate(firstDate, i - 1)
      day = days ? days[date] : null
      if (day) {
        reservation = reservations[day.reservation]
        let _day = day.reservation

        // The cell is one of the days of the reservation
        cells.push(
          <BodyCell
            key={i}
            roomID={id}
            isReserved={true}
            date={date}
            today={today}
            reservationData={reservation}
            cellData={day}
            clickHandler={() => cellClickHandlers.filled(_day)}
          />
        )
      } else {
        /*
         * Cell is empty
         * It includes selection management functions
         * (since the user can only allocate empty cells)
         */
        cells.push(
          <BodyCell
            key={i}
            cellID={i}
            selectionData={{
              start: this.state.startCell,
              end: this.state.endCell
            }}
            isReserved={false}
            date={date}
            today={today}
            startSelecting={this.startSelecting}
            stopSelecting={this.stopSelecting}
            escapeSelect={this.escapeSelect}
            selectCell={this.selectCell}
          />
        )
      }
    }

    return (
      <div className="tbody-body" onMouseLeave={this.stopSelecting}>
        {cells}
      </div>
    )
  }
}

export default BodyRowBody
