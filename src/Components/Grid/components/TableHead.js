import React, { Component } from "react"
import FirstHeadRow from "./FirstHeadRow"
import SecondHeadRow from "./SecondHeadRow"
import styled from "styled-components"

const THead = styled.div`
  position: sticky;
  z-index: 1100;
  top: 0;

  min-width: 100%;
  width: ${({ daysCount, multiplier }) => 237 + daysCount * multiplier}px
  margin-left: -1px;
  padding-left: 168px;

  transform: translateZ(1px);

  color: #ffffff;
  border-bottom: 2px solid #483d8b;
  background-color: slateblue;
`

const SizeControlBlock = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 168px;
  padding: 0 25px;
  height: 157px;
  display: flex;
`

const SizeControlInner = styled.div`
  margin: auto;
  text-align: left;
`

const SizeControlTitle = styled.h2`
  margin: 0 0 10px 0;
  font-size: 16px;
  color: #fff;
  display: block;
`

const SizeControlItem = styled.span`
  line-height: 1.2;
  display: inline-block;
  margin-right: 10px;
  margin-bottom: 10px;
  font-size: 14px;
  padding: 6px 8px 4px;
  background-color: ${({ isActive }) => (isActive ? "#483d8b" : "#fff")};
  color: ${({ isActive }) => (isActive ? "#fff" : "#333")};
  cursor: ${({ isActive }) => (!isActive ? "pointer" : "auto")};

  &:last-child {
    margin-right: 0;
    margin-bottom: 0;
  }
`

class TableHead extends Component {
  render() {
    const { firstDate, daysCount, size, multiplier, SizeControl } = this.props

    return (
      <THead daysCount={daysCount} multiplier={multiplier}>
        <SizeControlBlock>
          <SizeControlInner>
            <SizeControlTitle>Размер таблицы</SizeControlTitle>
            <SizeControlItem
              isActive={size === 100}
              onClick={SizeControl.set100Size}
            >
              100%
            </SizeControlItem>
            <SizeControlItem
              isActive={size === 80}
              onClick={SizeControl.set80Size}
            >
              80%
            </SizeControlItem>
            <SizeControlItem
              isActive={size === 60}
              onClick={SizeControl.set60Size}
            >
              60%
            </SizeControlItem>
          </SizeControlInner>
        </SizeControlBlock>
        <FirstHeadRow firstDate={firstDate} daysCount={daysCount} />
        <SecondHeadRow firstDate={firstDate} daysCount={daysCount} />
      </THead>
    )
  }
}

export default TableHead
