import React, { Component } from "react"
import uuid from "uuid"
import DateCell from "./DateCell"

class SecondHeadRow extends Component {
  render() {
    const { firstDate, daysCount } = this.props
    const row = []

    let date = new Date(firstDate)
    for (let i = 0; i < daysCount; i++) {
      row.push(<DateCell date={new Date(date)} key={uuid()} />)
      date.setDate(date.getDate() + 1)
    }

    return <div className="thead-row">{row}</div>
  }
}

export default SecondHeadRow
