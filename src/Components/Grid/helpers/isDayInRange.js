// Check if the day is in range or not

export default (day, firstDay, lastDay) => {
  const dayTimestamp = +new Date(day)
  const firstDayTimestamp = +new Date(firstDay)
  const lastDayTimestamp = +new Date(lastDay)
  return dayTimestamp >= firstDayTimestamp && dayTimestamp <= lastDayTimestamp
}
