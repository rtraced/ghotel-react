// Get the current date in Moscow time

export default () => {
  const date = new Date()
  const utc = 3
  date.setHours(
    date.getHours() + utc,
    date.getMinutes() + date.getTimezoneOffset()
  )
  return date
}
