import formatDate from "./formatDate"

// Move date by one or more days

export default (date, offset) => {
  const newDate = new Date(date)
  newDate.setDate(newDate.getDate() + offset)
  return formatDate(newDate)
}
