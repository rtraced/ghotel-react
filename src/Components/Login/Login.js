import React, { Component } from "react"
import { Form, Button } from "react-bootstrap"
import { Redirect } from "react-router-dom"
import Select from "../Common/Select"
import TextField from "../Common/Textfield"
import { getAdminsCall } from "../../Modules/api"
import LoadingModal from "../Common/LoadingModal"

const LoginBlock = props => (
  <div style={{ display: "flex", height: "100%" }}>{props.children}</div>
)

const LoginContent = props => (
  <div style={{ margin: "auto", width: 250 }}>{props.children}</div>
)

const LoginHead = props => (
  <h2
    style={{
      fontSize: 18,
      fontWeight: 700,
      margin: 0,
      padding: "20px 25px",
      backgroundColor: "#3a3a3a",
      color: "#fff",
      letterSpacing: 0.5,
      borderTopLeftRadius: 5,
      borderTopRightRadius: 5
    }}
  >
    {props.children}
  </h2>
)

const LoginBody = props => (
  <div
    style={{
      padding: 25,
      backgroundColor: "#e2e2e2",
      borderBottomLeftRadius: 5,
      borderBottomRightRadius: 5
    }}
  >
    {props.children}
  </div>
)

const LoginError = props => (
  <div
    style={{
      padding: "20px 10px 0",
      color: "crimson"
    }}
  >
    {props.children}
  </div>
)

class Login extends Component {
  state = {
    admins: [],
    isAdminsLoading: false,
    login: "",
    password: ""
  }

  componentDidMount() {
    this.setState({ isAdminsLoading: true })
    getAdminsCall().then(admins => {
      this.setState({ admins, isAdminsLoading: false })
    })
  }

  handleLoginChange = login => this.setState({ login })
  handlePasswordChange = password => this.setState({ password })

  handleSubmit = event => {
    event.preventDefault()
    const { admins, login, password } = this.state
    const getAdminByName = name => admins.find(a => a.name === name)

    if (!login || !password) {
      this.props.setSessionError("Заполните поля")
      return
    }

    const admin = getAdminByName(login)

    this.props.loginAdmin(admin.pk, password, login, admin.uid)
  }

  render() {
    if (this.props.isAuthorized) {
      return <Redirect to="/" />
    }

    return (
      <LoginBlock>
        <LoginContent>
          <LoginHead>Представьтесь</LoginHead>
          <LoginBody>
            <Form onSubmit={this.handleSubmit}>
              <Select
                name="Логин"
                value={this.state.login}
                values={this.state.admins.map(admin => ({
                  text: admin.name,
                  value: admin.name
                }))}
                onChange={this.handleLoginChange}
                style={{ marginBottom: 15 }}
              />
              <TextField
                name="Пароль"
                type="password"
                onChange={this.handlePasswordChange}
                style={{ marginBottom: 20 }}
              />
              <Button type="submit" bsStyle="success" style={{ width: "100%" }}>
                Войти
              </Button>
              {this.props.error ? (
                <LoginError>{this.props.error}</LoginError>
              ) : null}
            </Form>
          </LoginBody>
        </LoginContent>
        <LoadingModal isOpen={this.state.isAdminsLoading} />
      </LoginBlock>
    )
  }
}

export default Login
