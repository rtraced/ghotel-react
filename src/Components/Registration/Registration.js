import React, { Component } from "react"
import { Form, Button, Row, Col } from "react-bootstrap"
import { Redirect } from "react-router-dom"
import TextField from "../Common/Textfield"
import { registerAdminCall } from "../../Modules/api"

const RegistrationBlock = props => (
  <div style={{ display: "flex", height: "100%" }}>{props.children}</div>
)

const RegistrationContent = props => (
  <div style={{ margin: "auto", width: 500 }}>{props.children}</div>
)

const RegistrationHead = props => (
  <h2
    style={{
      fontSize: 18,
      fontWeight: 700,
      margin: 0,
      padding: "20px 25px",
      backgroundColor: "#3a3a3a",
      color: "#fff",
      letterSpacing: 0.5,
      borderTopLeftRadius: 5,
      borderTopRightRadius: 5
    }}
  >
    {props.children}
  </h2>
)

const RegistrationBody = props => (
  <div
    style={{
      padding: 25,
      backgroundColor: "#e2e2e2",
      borderBottomLeftRadius: 5,
      borderBottomRightRadius: 5
    }}
  >
    {props.children}
  </div>
)

const RegistrationWarning = props => (
  <div
    style={{
      marginTop: -5,
      marginBottom: 15,
      padding: "0 6px",
      color: "#555",
      fontSize: 12
    }}
  >
    Имя должно обязательно совпадать с вашим именем в графике смен (в т.ч.
    точки). Если вас нет в списке администраторов в графике смен —{" "}
    <b>сообщите руководству</b> перед созданием логина.
  </div>
)

const RegistrationError = props => (
  <div
    style={{
      padding: "20px 10px 0",
      color: "crimson",
      textAlign: "center"
    }}
  >
    {props.children}
  </div>
)

class Registration extends Component {
  state = {
    name: "",
    password: "",
    passwordConfirm: "",
    error: ""
  }

  setError = error => this.setState({ error })
  clearError = () => this.setState({ error: "" })

  handleNameChange = name => this.setState({ name })
  handlePasswordChange = password => this.setState({ password })
  handlePConfirmChange = passwordConfirm => this.setState({ passwordConfirm })

  handleSubmit = event => {
    event.preventDefault()
    const { name, password, passwordConfirm } = this.state

    if (!name || !password || !passwordConfirm) {
      this.setError("Все поля должны быть заполнены")
      return
    }

    if (password !== passwordConfirm) {
      this.setError("Пароли не совпадают")
      return
    }

    registerAdminCall(name, password).then(
      res => {
        this.props.loginAdminAfterRegistration(res.name, res.password)
        this.clearError()
      },
      err => this.setError("Bad server response")
    )
  }

  render() {
    if (this.props.isAuthorized) {
      return <Redirect to="/" />
    }

    return (
      <RegistrationBlock>
        <RegistrationContent>
          <RegistrationHead>Создать пользователя</RegistrationHead>
          <RegistrationBody>
            <Form onSubmit={this.handleSubmit}>
              <Row style={{ marginBottom: 10 }}>
                <Col xs={6}>
                  <TextField
                    name="Имя"
                    type="text"
                    onChange={this.handleNameChange}
                    style={{ marginBottom: 15 }}
                  />
                  <RegistrationWarning />
                </Col>
                <Col xs={6}>
                  <TextField
                    name="Пароль"
                    type="password"
                    onChange={this.handlePasswordChange}
                    style={{ marginBottom: 15 }}
                  />
                  <TextField
                    name="Подтверждение пароля"
                    type="password"
                    onChange={this.handlePConfirmChange}
                    style={{ marginBottom: 20 }}
                  />
                </Col>
              </Row>
              <Button type="submit" bsStyle="success" style={{ width: "100%" }}>
                Зарегистрироваться
              </Button>
              {this.state.error ? (
                <RegistrationError>{this.state.error}</RegistrationError>
              ) : null}
            </Form>
          </RegistrationBody>
        </RegistrationContent>
      </RegistrationBlock>
    )
  }
}

export default Registration
