import React, { Component } from "react"
import * as Sentry from "@sentry/browser"

class SentryWrapper extends Component {
  state = {
    error: null
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ error })
    Sentry.withScope(scope => {
      Object.keys(errorInfo).forEach(key => {
        scope.setExtra(key, errorInfo[key])
      })
      Sentry.captureException(error)
    })
  }

  render() {
    if (this.state.error) {
      return <a onClick={() => Sentry.showReportDialog()}>Report feedback</a>
    } else {
      return this.props.children
    }
  }
}

export default SentryWrapper
