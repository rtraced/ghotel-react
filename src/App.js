import React, { Component } from "react"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import { Switch, Route } from "react-router-dom"
import Modal from "react-modal"

import { setSettings } from "./Modules/settings/SettingsModule"

import LoginContainer from "./Containers/LoginContainer"
import EditReservationContainer from "./Containers/EditReservationContainer"
import NewReservationContainer from "./Containers/NewReservationContainer"
import PrivateRoute from "./Containers/PrivateRoute"
import NavbarContainer from "./Containers/NavbarContainer"
import RegistrationContainer from "./Containers/RegistrationContainer"
import WubookList from "./Components/WubookList/WubookList"
import Search from "./Components/Search/Search"
import SearchByDate from "./Components/SearchByDate/SearchByDate"
import SearchByStart from "./Components/SearchByStart/SearchByStart"
import NoShowSearch from "./Components/NoShowSearch/NoShowSearch"
import CheckPrinterCP from "./Components/CheckPrinterCP"
import SettingsContainer from "./Containers/SettingsContainer"
import MainPageContainer from "./Containers/MainPageContainer"

export const modalDefaultStyles = {
  overlay: {
    position: "fixed",
    zIndex: 1000000000,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "rgba(0, 0, 0, 0.75)",
    display: "flex"
  },
  content: {
    margin: "auto",
    backgroundColor: "#fff",
    borderRadius: 5,
    outline: "none",
    minWidth: 540,
    maxWidth: 640
  }
}

Modal.setAppElement("#root")
Modal.defaultStyles = modalDefaultStyles

class App extends Component {
  componentDidMount() {
    this.props.setSettings()
  }

  render() {
    return (
      <div style={{ paddingTop: 90, height: "100%" }} className="global-page">
        <NavbarContainer />
        <Switch>
          <Route path="/login" component={LoginContainer} />
          <Route path="/register" component={RegistrationContainer} />
          <PrivateRoute path="/" exact component={MainPageContainer} />
          <PrivateRoute
            path="/reservation"
            component={EditReservationContainer}
          />
          <PrivateRoute
            path="/new-reservation"
            component={NewReservationContainer}
          />
          <PrivateRoute path="/wubook" component={WubookList} />
          <PrivateRoute path="/search-by-name" component={Search} />
          <PrivateRoute path="/search-by-date" component={SearchByDate} />
          <PrivateRoute path="/search-by-start" component={SearchByStart} />
          <PrivateRoute path="/no-show-search" component={NoShowSearch} />
          <PrivateRoute path="/check-printer-cp" component={CheckPrinterCP} />
          <PrivateRoute path="/settings" component={SettingsContainer} />
        </Switch>
      </div>
    )
  }
}

const mapDispatchToProps = {
  setSettings
}

export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(App)
)
