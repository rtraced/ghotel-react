import React from "react"
import ReactDOM from "react-dom"
import * as Sentry from "@sentry/browser"
import { Router } from "react-router-dom"
import { Provider } from "react-redux"
import { createStore, applyMiddleware } from "redux"
import history from "./Modules/helpers/history"
import thunk from "redux-thunk"
import reducers from "./Modules/index"
import "./index.css"
import "./css/style.css"
import "./App.css"

// Polyfills
import "mdn-polyfills/Array.prototype.find"
import "mdn-polyfills/Array.prototype.filter"
import "mdn-polyfills/Array.prototype.includes"
import "mdn-polyfills/Array.prototype.some"

import App from "./App"
import SentryWrapper from "./SentryWrapper"

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk)
)

Sentry.init({
  dsn: "https://237d66748315489d92ee206a84646245@sentry.io/1310630"
})

ReactDOM.render(
  <SentryWrapper>
    <Router history={history}>
      <Provider store={store}>
        <App />
      </Provider>
    </Router>
  </SentryWrapper>,
  document.getElementById("root")
)
